USE [master]
GO
/****** Object:  Database [HCNS.CMSAdminDB]    Script Date: 18/02/2022 8:52:56 SA ******/
CREATE DATABASE [HCNS.CMSAdminDB]
 CONTAINMENT = NONE
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HCNS.CMSAdminDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET RECOVERY FULL 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET  MULTI_USER 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'HCNS.CMSAdminDB', N'ON'
GO
USE [HCNS.CMSAdminDB]
GO
/****** Object:  User [HCNS.CMSAdminDB]    Script Date: 18/02/2022 8:52:57 SA ******/
CREATE USER [HCNS.CMSAdminDB] FOR LOGIN [HCNS.CMSAdminDB] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [hanhtm]    Script Date: 18/02/2022 8:52:57 SA ******/
CREATE USER [hanhtm] FOR LOGIN [hanhtm] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [hanhtm]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_DateToInt]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- ==============================================================================
-- Author	: anhtd
-- Create date	: 
-- Description	: Convert Date to Int
-- ==============================================================================

CREATE FUNCTION [dbo].[FN_DateToInt] (@_DateInput DateTime)
RETURNS INT
AS 

BEGIN
	DECLARE @_Result INT
	Set @_Result = CONVERT(VARCHAR(10), @_DateInput, 112)
	return @_Result

END





GO
/****** Object:  UserDefinedFunction [dbo].[FN_IntToDate]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- ==============================================================================
-- Author	: anhtd
-- Create date	: 
-- Description	: Convert Int sang Date
-- ==============================================================================

CREATE FUNCTION [dbo].[FN_IntToDate] (@_DateInt NVARCHAR(10))
	RETURNS DATETIME
AS
BEGIN
	DECLARE @_Year NVARCHAR(4), @_Month NVARCHAR(2), @_Day NVARCHAR(2);
	DECLARE @_FirstYear NVARCHAR(1), @_LastYear NVARCHAR(1), @_strDate NVARCHAR(23)
	DECLARE  @_DateOut DATETIME;
	
	SET @_Year = LEFT(@_DateInt,4)
	SET @_LastYear = SUBSTRING(@_DateInt,2,1)
	
	SET @_Month = SUBSTRING(@_DateInt,5,2)
	SET @_Day = SUBSTRING(@_DateInt,7,2)
	
	IF (CONVERT(INT,@_Month) > 12) OR  (CONVERT(INT,@_Day) > 31 ) BEGIN
		SET @_DateOut = '1900-01-01 10:10:001'
	END
	ELSE BEGIN
		SET @_strDate = @_Year + '-' + @_Month + '-' + @_Day + ' 00:00:00.000'
		IF ISDATE(@_strDate) = 1 BEGIN
			SET @_DateOut = @_strDate
		END
		ELSE BEGIN
			SET @_DateOut = '1900-01-01 01:01:001' 	
		END		
	END

	RETURN @_DateOut
END



GO
/****** Object:  UserDefinedFunction [dbo].[FN_Parameters_Split]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author		:  anhtd
-- ALTER date	: 
-- Description	: Tach chuoi
-- ==============================================================================
CREATE FUNCTION [dbo].[FN_Parameters_Split]
    (@String NVARCHAR(MAX)
    ,@Delimiter NVARCHAR(10))
    
RETURNS @ValueTable TABLE ([Index] INT IDENTITY(1,1), [Value] NVARCHAR(1000))
BEGIN
    DECLARE @NextString NVARCHAR(2000)
    DECLARE @Pos INT
    DECLARE @NextPos INT
    DECLARE @CommaCheck NVARCHAR(1)
 
	--Initialize
    SET @NextString = ''
    SET @CommaCheck = RIGHT(@String,1) 
 
	--Check for trailing Comma, if not exists, INSERT
	--if (@CommaCheck <> @Delimiter )
    SET @String = @String + @Delimiter
 
	--Get position of first Comma
    SET @Pos = CHARINDEX(@Delimiter,@String)
    SET @NextPos = 1
 
	--Loop while there is still a comma in the String of levels
    WHILE (@pos <> 0) 
        BEGIN
            SET @NextString = SUBSTRING(@String,1,@Pos - 1)
 
            INSERT  INTO @ValueTable
                    ([Value])
            VALUES  (LTRIM(RTRIM(@NextString)))
 
            SET @String = SUBSTRING(@String,@pos + 1,LEN(@String))
  
            SET @NextPos = @Pos
            SET @pos = CHARINDEX(@Delimiter,@String)
        END
 
    RETURN
END






GO
/****** Object:  UserDefinedFunction [dbo].[FN_Parameters_SplitTable]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author		:  anhtd
-- ALTER date	: 
-- Description	: Tach chuoi sang bang
-- ==============================================================================

 
CREATE FUNCTION [dbo].[FN_Parameters_SplitTable]
    (@String NVARCHAR(MAX)
    ,@RowDelimiter NVARCHAR(10)
    ,@ColumnDelimiter NVARCHAR(10))
RETURNS @Table TABLE ([Index] INT IDENTITY, [Value1] NVARCHAR(200), [Value2] NVARCHAR(200), [Value3] NVARCHAR(200), [ValueN] NVARCHAR(200))
BEGIN
    
    DECLARE @Value NVARCHAR(1000);
    DECLARE CUR_SPLIT CURSOR FOR
    SELECT Value FROM dbo.FN_Parameters_Split(@String,@RowDelimiter)
	WHERE ISNULL([Value],'')<> '';
    
    OPEN CUR_SPLIT
	FETCH NEXT FROM CUR_SPLIT INTO @Value
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE @Index INT = 0;
		DECLARE @ValueIn NVARCHAR(1000)='';
		DECLARE	@ColumnCount INT = 1;
		
		IF(ISNULL(@Value,'') <> '')
		BEGIN		
			WHILE (@ColumnCount <= (SELECT COUNT(*) FROM dbo.FN_Parameters_Split(@Value,@ColumnDelimiter)))
			BEGIN
				SELECT TOP 1 @ValueIn=Value FROM dbo.FN_Parameters_Split(@Value,@ColumnDelimiter) WHERE [Index] = @ColumnCount;
				
				IF(@ColumnCount = 1)
				BEGIN
					INSERT INTO @Table(Value1)VALUES(@ValueIn);
					SET @Index = @@IDENTITY;
				END
				ELSE IF (@ColumnCount = 1)
				BEGIN
					UPDATE @Table SET Value1 = @ValueIn WHERE [Index] = @Index;
				END
				ELSE IF (@ColumnCount = 2)
				BEGIN
					UPDATE @Table SET Value2 = @ValueIn WHERE [Index] = @Index;
				END
				ELSE IF (@ColumnCount = 3)
				BEGIN
					UPDATE @Table SET Value3 = @ValueIn WHERE [Index] = @Index;
				END
				ELSE BEGIN
					UPDATE @Table SET ValueN = ISNULL(ValueN,'') + CASE WHEN ISNULL(ValueN,'') <> '' THEN ';' ELSE '' END + @ValueIn
					WHERE [Index] = @Index;
				END
				
				SET @ColumnCount  = @ColumnCount + 1;
			END 
		END
		
		FETCH NEXT FROM CUR_SPLIT INTO @Value
	END
	
	CLOSE CUR_SPLIT;
	DEALLOCATE CUR_SPLIT;
	
	RETURN;
END





GO
/****** Object:  UserDefinedFunction [dbo].[FN_PasswordEncrypt]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- ==============================================================================
-- Author	: anhtd
-- Create date	: 
-- Description	: Ma hoa Pass
-- ==============================================================================

CREATE FUNCTION [dbo].[FN_PasswordEncrypt] (@UserName varchar(50),
											@Password varchar(64) )
RETURNS VARCHAR(64)
AS 

BEGIN
	DECLARE @_Result VARCHAR(64) =''
	
	SET @_Result = LOWER(@UserName +'145$s@&%'+@Password)
	SELECT  @_Result = LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', @_Result), 2))
	return @_Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_UNIX_TIMESTAMP]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================================================================
-- Author		:  anhtd
-- ALTER date	: 
-- Description	:Convert Datetime sang Unix
-- ==============================================================================

CREATE FUNCTION [dbo].[FN_UNIX_TIMESTAMP] (
 @_InputDatetime datetime
)
RETURNS BIGINT
AS
BEGIN
DECLARE @diff BIGINT 
    IF @_InputDatetime >= {d '2068-01-19'}
    BEGIN 
        SET @diff = CONVERT(BIGINT, DATEDIFF(S, {d '2000-01-01'}, {d '2068-01-19'})) 
            + CONVERT(BIGINT, DATEDIFF(S, {d '2068-01-19'}, @_InputDatetime)) 
    END 
    ELSE 
        SET @diff = DATEDIFF(S, {d '2000-01-01'}, @_InputDatetime) 
    RETURN @diff
END



GO
/****** Object:  UserDefinedFunction [dbo].[FN_UnixTime2DateTime]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author		:  anhtd
-- ALTER date	: 
-- Description	:Convert Unix sang Datetime 
-- ==============================================================================
CREATE FUNCTION [dbo].[FN_UnixTime2DateTime] (@Datetime BIGINT)
RETURNS DATETIME
AS
BEGIN
    DECLARE @LocalTimeOffset BIGINT
           ,@AdjustedLocalDatetime BIGINT;
    SET @LocalTimeOffset = DATEDIFF(second,GETDATE(),GETDATE())
    SET @AdjustedLocalDatetime = @Datetime - @LocalTimeOffset
    RETURN (SELECT DATEADD(second,@Datetime, CAST('2000-01-01 00:00:00' AS datetime)))
END;


GO
/****** Object:  Table [dbo].[CMS_Functions]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_Functions](
	[FunctionID] [int] IDENTITY(1,1) NOT NULL,
	[FunctionName] [nvarchar](150) NOT NULL,
	[LinkHttp] [nvarchar](150) NOT NULL,
	[LinkFunction] [nvarchar](150) NULL,
	[IsDisplay] [smallint] NULL,
	[IsActive] [smallint] NULL,
	[ParentID] [int] NULL,
	[CssIcon] [nvarchar](50) NULL,
	[ActionName] [nvarchar](200) NULL,
	[ByOrder] [smallint] NULL,
	[CreateUser] [varchar](50) NULL,
	[CreatedTime] [datetime] NULL,
 CONSTRAINT [PK_Functions] PRIMARY KEY CLUSTERED 
(
	[FunctionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMS_Media]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_Media](
	[MediaID] [int] IDENTITY(1,1) NOT NULL,
	[MediaType] [smallint] NULL,
	[MediaName] [varchar](150) NULL,
	[Path] [nvarchar](200) NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
	[Alt] [varchar](50) NULL,
	[TitleShare] [nvarchar](500) NULL,
	[Extend] [varchar](10) NULL,
	[CreateUser] [varchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[DateInt] [int] NULL,
 CONSTRAINT [PK_CMS_Media] PRIMARY KEY CLUSTERED 
(
	[MediaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMS_User]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[UserType] [smallint] NULL,
	[Password] [varchar](64) NOT NULL,
	[FullName] [nvarchar](150) NULL,
	[Email] [varchar](150) NULL,
	[Mobile] [varchar](50) NULL,
	[DateOfBirth] [date] NULL,
	[NickName] [nvarchar](50) NULL,
	[User_Img] [nvarchar](200) NULL,
	[User_Info] [nvarchar](500) NULL,
	[Status] [smallint] NULL,
	[CreatedUser] [varchar](50) NULL,
	[CreatedTime] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMS_UserFunction]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_UserFunction](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[FunctionID] [int] NOT NULL,
	[IsGrant] [bit] NOT NULL,
	[IsInsert] [bit] NOT NULL,
	[IsUpdate] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[CreatedUser] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserFunction] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[FunctionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMS_UserFunction_Log]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_UserFunction_Log](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[FunctionID] [int] NOT NULL,
	[IsGrant] [bit] NOT NULL,
	[IsInsert] [bit] NOT NULL,
	[IsUpdate] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[CreatedUser] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CMS_UserFunction_Log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMS_UserLogs]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_UserLogs](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[FunctionID] [int] NOT NULL,
	[UserName] [varchar](50) NULL,
	[FullName] [nvarchar](100) NULL,
	[FunctionName] [nvarchar](100) NULL,
	[Description] [nvarchar](2000) NULL,
	[LogType] [int] NULL,
	[ClientIP] [varchar](64) NULL,
	[CreatedTime] [datetime] NOT NULL,
	[DateInt] [int] NULL,
 CONSTRAINT [PK_UserLogs] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMS_UserType]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_UserType](
	[UserType] [smallint] IDENTITY(1,1) NOT NULL,
	[UserTypeName] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedUser] [varchar](50) NULL,
	[CreatedTime] [datetime] NULL,
 CONSTRAINT [PK_CMS_UserType] PRIMARY KEY CLUSTERED 
(
	[UserType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMS_UserTypeFunction]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_UserTypeFunction](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[UserType] [int] NOT NULL,
	[FunctionID] [int] NOT NULL,
	[IsGrant] [bit] NOT NULL,
	[IsInsert] [bit] NOT NULL,
	[IsUpdate] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[CreatedUserType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserTypeFunction] PRIMARY KEY CLUSTERED 
(
	[UserType] ASC,
	[FunctionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMS_UserTypeFunction_Log]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_UserTypeFunction_Log](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[UserType] [int] NOT NULL,
	[FunctionID] [int] NOT NULL,
	[IsGrant] [bit] NOT NULL,
	[IsInsert] [bit] NOT NULL,
	[IsUpdate] [bit] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[CreatedUser] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CMS_UserTypeFunction_Log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ErrorLog]    Script Date: 18/02/2022 8:52:57 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
	[UserName] [sysname] NOT NULL,
	[HostName] [nvarchar](200) NULL,
	[ErrorNumber] [int] NOT NULL,
	[ErrorCode] [int] NULL,
	[ErrorSeverity] [int] NULL,
	[ErrorState] [int] NULL,
	[ErrorProcedure] [nvarchar](126) NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[Parameters] [nvarchar](1000) NULL,
 CONSTRAINT [PK_ErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED 
(
	[ErrorLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CMS_Functions] ON 

INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (1, N'Hệ thống', N'', N'', 1, 1, 0, N'', N'', 0, N'Admin', CAST(N'2021-06-11T14:07:53.743' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (2, N'Nhóm người dùng', N'', N'/Manage/UserTypeList', 1, 1, 1, N'users', N'', 0, N'Admin', CAST(N'2021-06-16T09:23:33.630' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (3, N'Quản trị người dùng', N'', N'/Manage/UserList', 1, 1, 1, N'user', N'', 1, N'Admin', CAST(N'2021-06-16T09:23:48.403' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (4, N'Quản trị chức năng', N'', N'/Manage/FunctionList', 1, 1, 1, N'circle', N'', 2, N'Admin', CAST(N'2021-06-16T10:06:42.633' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (5, N'Quản trị phân quyền', N'', N'', 0, 1, 1, N'', N'', 3, N'Admin', CAST(N'2021-06-11T14:26:39.153' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (6, N'Quản trị log người dùng', N'', N'', 0, 1, 1, N'', N'Log', 0, N'Admin', CAST(N'2021-06-11T14:33:12.773' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (7, N'Quản lý nhân sự', N'', N'', 1, 1, 0, N'circle', N'', 3, N'thangtv', CAST(N'2021-12-21T14:30:31.907' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (8, N'Quản lý tiền phạt', N'', N'/AccountManagement/AccountTicketList', 1, 1, 7, N'circle', N'', 0, N'thangtv', CAST(N'2021-12-21T14:30:55.473' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (9, N'Báo cáo nạp', N'', N'/Report/ReportTopup', 0, 0, 7, N'circle', N'', 2, N'thangtv', CAST(N'2021-12-21T14:28:40.487' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (10, N'Báo cáo tiêu', N'', N'/Report/ReportOutput', 0, 0, 7, N'circle', N'', 1, N'thangtv', CAST(N'2021-12-21T14:28:37.530' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (11, N'GM Tool', N'', N'', 0, 0, 0, N'search', N'', 1, N'Admin', CAST(N'2021-06-28T09:33:44.710' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (12, N'Lịch sử đăng nhập', N'', N'/GMTool/HistoryAccountLogin', 1, 1, 11, N'circle', N'', 2, N'thangtv', CAST(N'2021-06-22T14:39:53.047' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (13, N'Lịch sử biến động', N'', N'/GMTool/HistoryTicket', 1, 1, 11, N'circle', N'', 3, N'thangtv', CAST(N'2021-06-22T14:39:59.270' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (14, N'Báo cáo đối soát', N'', N'/Report/HistoryTopup', 0, 0, 7, N'circle', N'', 3, N'thangtv', CAST(N'2021-12-21T14:28:42.800' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (15, N'Gạch thẻ', N'', N'', 0, 0, 0, N'credit-card', N'', 1, N'thangtv', CAST(N'2021-12-21T10:38:27.213' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (16, N'Báo cáo gạch thẻ', N'', N'/Card/ReportCard', 1, 1, 15, N'circle', N'', 4, N'thangtv', CAST(N'2021-06-17T13:30:06.600' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (17, N'Lịch sử gạch thẻ', N'', N'/Card/HistoryCard', 1, 1, 15, N'circle', N'', 3, N'thangtv', CAST(N'2021-06-17T15:43:12.550' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (18, N'Danh sách đối tác', N'', N'/Card/PartnerList', 1, 1, 15, N'circle', N'', 1, N'Admin', CAST(N'2021-06-18T14:25:07.943' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (19, N'Dịch vụ đối tác', N'', N'/Card/PartnerServiceList', 1, 1, 15, N'circle', N'', 2, N'Admin', CAST(N'2021-06-18T16:35:00.160' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (20, N'Lịch sử FeedBack', N'', N'/GMTool/HistoryFeedBack', 1, 1, 11, N'circle', N'', 4, N'thangtv', CAST(N'2021-06-22T14:40:05.597' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (21, N'Danh sách loại thẻ', N'', N'/Card/CardTypeList', 1, 1, 15, N'circle', N'', 0, N'thangtv', CAST(N'2021-06-22T11:13:58.733' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (22, N'Danh sách Game', N'', N'/GMTool/GamesList', 1, 1, 11, N'circle', N'', 0, N'thangtv', CAST(N'2021-06-22T15:06:43.877' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (23, N'Danh sách Story', N'', N'/GMTool/StoryList', 1, 1, 11, N'circle', N'', 1, N'thangtv', CAST(N'2021-06-23T10:29:46.457' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (24, N'FPay Billing Order', N'', N'', 0, 0, 0, N'', N'', 2, N'thangtv', CAST(N'2021-12-21T10:38:30.997' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (25, N'Danh sách APP', N'', N'/BillingOrder/AppList', 1, 1, 24, N'circle', N'', 2, N'thangtv', CAST(N'2021-07-02T11:47:36.510' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (26, N'Cổng thanh toán', N'', N'/BillingOrder/GatewayList', 1, 1, 24, N'circle', N'', 5, N'thangtv', CAST(N'2021-07-02T11:45:52.837' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (27, N'Cổng thanh toán của App', N'', N'/BillingOrder/GatewayAppsList', 1, 1, 24, N'circle', N'', 4, N'Admin', CAST(N'2021-07-05T13:37:17.610' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (28, N'Tỉ giá chuyển đổi', N'', N'/BillingOrder/ExchangeRateList', 0, 0, 24, N'circle', N'', 4, N'thangtv', CAST(N'2021-11-03T10:20:54.980' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (29, N' Danh sách đơn hàng', N'', N'/BillingOrder/OrderList', 1, 1, 24, N'circle', N'', 0, N'thangtv', CAST(N'2021-07-07T09:52:40.203' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (30, N'Báo cáo', N'', N'/BillingOrder/ReportOrder', 1, 1, 24, N'circle', N'', 1, N'thangtv', CAST(N'2021-07-07T09:52:28.233' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (31, N'Danh sách IAP', N'', N'/BillingOrder/IAPList', 1, 1, 24, N'circle', N'', 3, N'thangtv', CAST(N'2021-11-03T10:27:31.697' AS DateTime))
INSERT [dbo].[CMS_Functions] ([FunctionID], [FunctionName], [LinkHttp], [LinkFunction], [IsDisplay], [IsActive], [ParentID], [CssIcon], [ActionName], [ByOrder], [CreateUser], [CreatedTime]) VALUES (32, N'Tài khoản bị khóa', N'', N'/BillingOrder/FIDBlockList', 0, 0, 24, N'circle', N'', 6, N'thangtv', CAST(N'2021-11-03T10:20:58.513' AS DateTime))
SET IDENTITY_INSERT [dbo].[CMS_Functions] OFF
GO
SET IDENTITY_INSERT [dbo].[CMS_User] ON 

INSERT [dbo].[CMS_User] ([UserID], [UserName], [UserType], [Password], [FullName], [Email], [Mobile], [DateOfBirth], [NickName], [User_Img], [User_Info], [Status], [CreatedUser], [CreatedTime]) VALUES (1, N'Admin', 1, N'300dbb8e8dbca583283123d01a0867fc', N'Admin', N'ad@ftech.ai', N'0353265987', CAST(N'2021-01-01' AS Date), N'Admin', N'', N'', 1, N'thangtv', CAST(N'2021-12-27T16:34:00.703' AS DateTime))
INSERT [dbo].[CMS_User] ([UserID], [UserName], [UserType], [Password], [FullName], [Email], [Mobile], [DateOfBirth], [NickName], [User_Img], [User_Info], [Status], [CreatedUser], [CreatedTime]) VALUES (2, N'thangtv', 1, N'f103e741d8400a5ccc4c0a236a4b53ec', N'Trần Viết Thắng', N'thangtv@ftech.ai', N'0353265452', CAST(N'2021-06-16' AS Date), N'', N'', N'', 1, N'Admin', CAST(N'2021-12-10T13:34:19.560' AS DateTime))
INSERT [dbo].[CMS_User] ([UserID], [UserName], [UserType], [Password], [FullName], [Email], [Mobile], [DateOfBirth], [NickName], [User_Img], [User_Info], [Status], [CreatedUser], [CreatedTime]) VALUES (3, N'platform', 3, N'67d8a1c5b01ae6a1a0a4b003ea4a6664', N'Phòng Platform', N'platform@ftech.ai', N'0999999999', CAST(N'2021-07-05' AS Date), N'', N'', N'', 1, N'Admin', CAST(N'2021-12-28T09:39:18.263' AS DateTime))
INSERT [dbo].[CMS_User] ([UserID], [UserName], [UserType], [Password], [FullName], [Email], [Mobile], [DateOfBirth], [NickName], [User_Img], [User_Info], [Status], [CreatedUser], [CreatedTime]) VALUES (4, N'tuanpm', 1, N'b05eba79f13d233d38900e09a77da970', N'tuanpm', N'tuanpm@ftech.ai', N'123123123', CAST(N'2021-08-18' AS Date), N'', N'', N'', 1, N'thangtv', CAST(N'2021-08-18T14:26:57.700' AS DateTime))
INSERT [dbo].[CMS_User] ([UserID], [UserName], [UserType], [Password], [FullName], [Email], [Mobile], [DateOfBirth], [NickName], [User_Img], [User_Info], [Status], [CreatedUser], [CreatedTime]) VALUES (5, N'truongtx', 1, N'e67aabdf29d350b9345b990be34ef360', N'Trường', N'truongtx@ftech.ai', N'091332456', CAST(N'2021-12-27' AS Date), N'', N'', N'', 1, N'tuanpm', CAST(N'2021-12-27T22:38:29.253' AS DateTime))
SET IDENTITY_INSERT [dbo].[CMS_User] OFF
GO
SET IDENTITY_INSERT [dbo].[CMS_UserFunction] ON 

INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1, 1, 1, 1, 1, 1, 1, CAST(N'2021-06-02T08:57:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1158, 1, 2, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1159, 1, 3, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1160, 1, 4, 1, 1, 1, 0, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (5, 1, 5, 1, 1, 1, 1, CAST(N'2021-06-02T08:57:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (6, 1, 6, 1, 1, 1, 1, CAST(N'2021-06-02T08:57:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (722, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (724, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (723, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (719, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (720, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (725, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1121, 1, 16, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1120, 1, 17, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1118, 1, 18, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1119, 1, 19, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (721, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1117, 1, 21, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1122, 1, 25, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1124, 1, 26, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1125, 1, 27, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1110, 1, 28, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1126, 1, 29, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1127, 1, 30, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1123, 1, 31, 1, 1, 1, 1, CAST(N'2021-11-05T14:12:14.167' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1112, 1, 32, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (7, 2, 1, 1, 1, 1, 1, CAST(N'2021-06-16T13:06:49.560' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1161, 2, 2, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1162, 2, 3, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1163, 2, 4, 1, 1, 1, 0, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (13, 2, 5, 1, 1, 1, 1, CAST(N'2021-06-16T13:06:49.560' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (14, 2, 6, 1, 1, 1, 1, CAST(N'2021-06-16T13:06:49.560' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (789, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (791, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (790, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (786, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (787, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (792, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (977, 2, 16, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (976, 2, 17, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (974, 2, 18, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (975, 2, 19, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (788, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (973, 2, 21, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (784, 2, 22, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (785, 2, 23, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (978, 2, 25, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (980, 2, 26, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (981, 2, 27, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (982, 2, 28, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (983, 2, 29, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (985, 2, 30, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (979, 2, 31, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (984, 2, 32, 1, 1, 1, 1, CAST(N'2021-10-05T17:35:22.093' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1171, 3, 2, 0, 0, 0, 0, CAST(N'2021-12-28T09:39:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1172, 3, 3, 0, 0, 0, 0, CAST(N'2021-12-28T09:39:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1173, 3, 4, 0, 0, 0, 0, CAST(N'2021-12-28T09:39:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1174, 3, 8, 0, 0, 0, 0, CAST(N'2021-12-28T09:39:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1175, 3, 9, 0, 0, 0, 0, CAST(N'2021-12-28T09:39:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1176, 3, 10, 0, 0, 0, 0, CAST(N'2021-12-28T09:39:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1177, 3, 14, 0, 0, 0, 0, CAST(N'2021-12-28T09:39:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (916, 4, 1, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1168, 4, 2, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1169, 4, 3, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1170, 4, 4, 1, 1, 1, 0, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (924, 4, 5, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (925, 4, 6, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (917, 4, 7, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1147, 4, 8, 1, 1, 1, 1, CAST(N'2021-12-27T16:49:00.210' AS DateTime), N'tuanpm')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (918, 4, 11, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (919, 4, 15, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1009, 4, 16, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1008, 4, 17, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1006, 4, 18, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1007, 4, 19, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1005, 4, 21, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (920, 4, 24, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1010, 4, 25, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1012, 4, 26, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1013, 4, 27, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1014, 4, 28, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1015, 4, 29, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1017, 4, 30, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1011, 4, 31, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1016, 4, 32, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1154, 5, 8, 1, 1, 1, 1, CAST(N'2021-12-27T22:38:40.787' AS DateTime), N'tuanpm')
SET IDENTITY_INSERT [dbo].[CMS_UserFunction] OFF
GO
SET IDENTITY_INSERT [dbo].[CMS_UserFunction_Log] ON 

INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-16T13:06:49.560' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (2, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-16T13:06:49.560' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (3, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-16T13:06:49.560' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (4, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (5, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (6, 2, 4, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (7, 2, 8, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (8, 2, 9, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (9, 2, 10, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (10, 2, 12, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (11, 2, 13, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (12, 2, 14, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (13, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (14, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (15, 2, 18, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (16, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (17, 2, 20, 0, 0, 0, 0, CAST(N'2021-06-21T14:24:53.850' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (18, 2, 2, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (19, 2, 3, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (20, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (21, 2, 8, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (22, 2, 9, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (23, 2, 10, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (24, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (25, 2, 13, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (26, 2, 14, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (27, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (28, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (29, 2, 18, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (30, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (31, 2, 20, 0, 0, 0, 0, CAST(N'2021-06-21T14:25:37.973' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (32, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-02T08:57:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (33, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-02T08:57:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (34, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-02T08:57:18.263' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (35, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (36, 2, 3, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (37, 2, 4, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (38, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (39, 2, 9, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (40, 2, 10, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (41, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (42, 2, 13, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (43, 2, 14, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (44, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (45, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (46, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (47, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (48, 2, 20, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:04.473' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (49, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (50, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (51, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (52, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (53, 2, 9, 0, 0, 0, 0, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (54, 2, 10, 0, 0, 0, 0, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (55, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (56, 2, 13, 0, 0, 0, 0, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (57, 2, 14, 0, 0, 0, 0, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (58, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (59, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (60, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (61, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (62, 2, 20, 0, 0, 0, 0, CAST(N'2021-06-21T14:39:00.433' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (63, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (64, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (65, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (66, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (67, 2, 9, 0, 0, 0, 0, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (68, 2, 10, 0, 0, 0, 0, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (69, 2, 12, 0, 0, 0, 0, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (70, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (71, 2, 14, 0, 0, 0, 0, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (72, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (73, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (74, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (75, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (76, 2, 20, 0, 0, 0, 0, CAST(N'2021-06-21T14:41:49.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (77, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (78, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (79, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (80, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (81, 2, 9, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (82, 2, 10, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (83, 2, 12, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (84, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (85, 2, 14, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (86, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (87, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (88, 2, 18, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (89, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (90, 2, 20, 0, 0, 0, 0, CAST(N'2021-06-21T15:22:45.613' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (91, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (92, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (93, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (94, 1, 8, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (95, 1, 9, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (96, 1, 10, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (97, 1, 12, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (98, 1, 13, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (99, 1, 14, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (100, 1, 16, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (101, 1, 17, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (102, 1, 18, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (103, 1, 19, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (104, 1, 20, 0, 0, 0, 0, CAST(N'2021-06-21T14:38:41.130' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (105, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (106, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (107, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (108, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (109, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (110, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (111, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (112, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (113, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (114, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (115, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (116, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (117, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (118, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T15:27:42.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (119, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (120, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (121, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (122, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (123, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (124, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (125, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (126, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (127, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (128, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (129, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (130, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (131, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (132, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T17:19:53.603' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (133, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (134, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (135, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (136, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (137, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (138, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (139, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (140, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (141, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (142, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (143, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (144, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (145, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (146, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T17:28:52.723' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (147, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (148, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (149, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (150, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (151, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (152, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (153, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (154, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (155, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (156, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (157, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (158, 2, 18, 0, 0, 0, 0, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (159, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (160, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:29.343' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (161, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (162, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (163, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (164, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (165, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (166, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (167, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (168, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (169, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (170, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (171, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (172, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (173, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (174, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T17:33:44.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (175, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (176, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (177, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (178, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (179, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (180, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (181, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (182, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (183, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (184, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (185, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (186, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (187, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (188, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:15.560' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (189, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (190, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (191, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (192, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (193, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (194, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (195, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (196, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (197, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (198, 2, 16, 0, 0, 0, 0, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (199, 2, 17, 0, 0, 0, 0, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (200, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (201, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (202, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:16.067' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (203, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (204, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (205, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (206, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (207, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (208, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (209, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (210, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (211, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (212, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (213, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (214, 2, 18, 0, 0, 0, 0, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (215, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (216, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:02:31.640' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (217, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (218, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (219, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (220, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (221, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (222, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (223, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (224, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (225, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (226, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (227, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (228, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (229, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (230, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:20.073' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (231, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (232, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (233, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (234, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (235, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (236, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (237, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (238, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (239, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (240, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (241, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (242, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (243, 2, 19, 1, 0, 0, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (244, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:03:49.080' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (245, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (246, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (247, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (248, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (249, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (250, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (251, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (252, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (253, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (254, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (255, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (256, 2, 18, 1, 0, 0, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (257, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (258, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:08:36.007' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (259, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (260, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (261, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (262, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (263, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (264, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (265, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (266, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (267, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (268, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (269, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (270, 2, 18, 0, 0, 0, 0, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (271, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (272, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:26.217' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (273, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (274, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (275, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (276, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (277, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (278, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (279, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (280, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (281, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (282, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (283, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (284, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (285, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (286, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:12:46.943' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (287, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (288, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (289, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (290, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (291, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (292, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (293, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (294, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (295, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (296, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (297, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (298, 2, 18, 1, 0, 0, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (299, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (300, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:13:04.853' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (301, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (302, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (303, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (304, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (305, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (306, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (307, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (308, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (309, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (310, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (311, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (312, 2, 18, 1, 0, 0, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (313, 2, 19, 0, 0, 0, 0, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (314, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:16:24.340' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (315, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (316, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (317, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (318, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (319, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (320, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (321, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (322, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (323, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (324, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (325, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (326, 2, 18, 1, 0, 0, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (327, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (328, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:29:41.083' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (329, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (330, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (331, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (332, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (333, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (334, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (335, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (336, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (337, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (338, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (339, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (340, 2, 18, 1, 0, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (341, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (342, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:30:04.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (343, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (344, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (345, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (346, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (347, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (348, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (349, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (350, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (351, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (352, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (353, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (354, 2, 18, 1, 1, 0, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (355, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (356, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:33:48.123' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (357, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (358, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (359, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (360, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (361, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (362, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (363, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (364, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (365, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (366, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (367, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (368, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (369, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (370, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:34:15.610' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (371, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (372, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (373, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (374, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (375, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (376, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (377, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (378, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (379, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (380, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (381, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (382, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (383, 2, 19, 1, 0, 0, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (384, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:04.740' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (385, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (386, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (387, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (388, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (389, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (390, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (391, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (392, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (393, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (394, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (395, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (396, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (397, 2, 19, 1, 0, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (398, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:20.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (399, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (400, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (401, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (402, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (403, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (404, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (405, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (406, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (407, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (408, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (409, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (410, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (411, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (412, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-21T18:37:35.157' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (413, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (414, 2, 3, 0, 0, 0, 0, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (415, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (416, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (417, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (418, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (419, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (420, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (421, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (422, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (423, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (424, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (425, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (426, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:25:57.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (427, 1, 2, 0, 0, 0, 0, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (428, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (429, 1, 4, 0, 0, 0, 0, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (430, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (431, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (432, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (433, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (434, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (435, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (436, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (437, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (438, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (439, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (440, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-21T15:28:53.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (441, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (442, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (443, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (444, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (445, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (446, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (447, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (448, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (449, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (450, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (451, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (452, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (453, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (454, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:22.773' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (455, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (456, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (457, 2, 4, 1, 0, 0, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (458, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (459, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (460, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (461, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (462, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (463, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (464, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (465, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (466, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (467, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (468, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:30:36.003' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (469, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (470, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (471, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (472, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (473, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (474, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (475, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (476, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (477, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (478, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (479, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (480, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (481, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (482, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:26:33.707' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (483, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (484, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (485, 1, 4, 1, 1, 0, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (486, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (487, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (488, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (489, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (490, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (491, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (492, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (493, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (494, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (495, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (496, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:34:57.660' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (497, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (498, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (499, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (500, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (501, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (502, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (503, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (504, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (505, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (506, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (507, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (508, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (509, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (510, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:35:30.177' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (511, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (512, 1, 3, 1, 0, 0, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (513, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (514, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (515, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (516, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (517, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (518, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (519, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (520, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (521, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (522, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (523, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (524, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:37:10.870' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (525, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (526, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (527, 1, 4, 1, 0, 0, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (528, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (529, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (530, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (531, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (532, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (533, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (534, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (535, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (536, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (537, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (538, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:38:21.627' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (539, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (540, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (541, 1, 4, 1, 0, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (542, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (543, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (544, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (545, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (546, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (547, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (548, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (549, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (550, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (551, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (552, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:39.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (553, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (554, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (555, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (556, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (557, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (558, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (559, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (560, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (561, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (562, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (563, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (564, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (565, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (566, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:39:58.247' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (567, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (568, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (569, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (570, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (571, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (572, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (573, 1, 12, 0, 0, 0, 0, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (574, 1, 13, 0, 0, 0, 0, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (575, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (576, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (577, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (578, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (579, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (580, 1, 20, 0, 0, 0, 0, CAST(N'2021-06-22T09:43:43.927' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (581, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (582, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (583, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (584, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (585, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (586, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (587, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (588, 1, 13, 0, 0, 0, 0, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (589, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (590, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (591, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (592, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (593, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (594, 1, 20, 0, 0, 0, 0, CAST(N'2021-06-22T09:44:10.103' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (595, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (596, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (597, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (598, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (599, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (600, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (601, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (602, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (603, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (604, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (605, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (606, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (607, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (608, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:44:29.023' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (609, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (610, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (611, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (612, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (613, 1, 9, 0, 0, 0, 0, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (614, 1, 10, 0, 0, 0, 0, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (615, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (616, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (617, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (618, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (619, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (620, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (621, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (622, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:12.287' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (623, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (624, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (625, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (626, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (627, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (628, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (629, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (630, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (631, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (632, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (633, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (634, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (635, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (636, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:45:31.500' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (637, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (638, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (639, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (640, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (641, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (642, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (643, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (644, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (645, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (646, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (647, 1, 17, 1, 0, 0, 0, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (648, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (649, 1, 19, 1, 0, 0, 0, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (650, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:47:40.333' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (651, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (652, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (653, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (654, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (655, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (656, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (657, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (658, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (659, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (660, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (661, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (662, 1, 18, 1, 0, 0, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (663, 1, 19, 1, 0, 0, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (664, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:12.873' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (665, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (666, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (667, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (668, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (669, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (670, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (671, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (672, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (673, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (674, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (675, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (676, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (677, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (678, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:31:13.583' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (679, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (680, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (681, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (682, 1, 8, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (683, 1, 9, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (684, 1, 10, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (685, 1, 12, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (686, 1, 13, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (687, 1, 14, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (688, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (689, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (690, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (691, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (692, 1, 20, 1, 1, 1, 1, CAST(N'2021-06-22T09:48:29.633' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (693, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (694, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (695, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (696, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (697, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (698, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (699, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (700, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (701, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (702, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (703, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (704, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (705, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (706, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (707, 2, 21, 1, 1, 1, 1, CAST(N'2021-06-22T11:14:26.907' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (708, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (709, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (710, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (711, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (712, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (713, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (714, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (715, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (716, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (717, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (718, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (719, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (720, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (721, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (722, 2, 21, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (723, 2, 22, 1, 1, 1, 1, CAST(N'2021-06-22T15:07:11.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (724, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (725, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (726, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (727, 2, 8, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (728, 2, 9, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (729, 2, 10, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (730, 2, 12, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (731, 2, 13, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (732, 2, 14, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (733, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (734, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (735, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (736, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (737, 2, 20, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (738, 2, 21, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (739, 2, 22, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (740, 2, 23, 1, 1, 1, 1, CAST(N'2021-06-23T10:30:00.190' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (741, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (742, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (743, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (744, 2, 8, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (745, 2, 9, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (746, 2, 10, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (747, 2, 12, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (748, 2, 13, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (749, 2, 14, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (750, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (751, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (752, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (753, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (754, 2, 20, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (755, 2, 21, 1, 1, 1, 1, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (756, 2, 22, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (757, 2, 23, 0, 0, 0, 0, CAST(N'2021-06-24T10:41:21.423' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (758, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (759, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (760, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (761, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (762, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (763, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (764, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (765, 1, 21, 1, 1, 1, 1, CAST(N'2021-06-22T13:23:27.550' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (766, 1, 2, 0, 0, 0, 0, CAST(N'2021-06-28T13:43:19.367' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (767, 1, 3, 0, 0, 0, 0, CAST(N'2021-06-28T13:43:19.367' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (768, 1, 4, 0, 0, 0, 0, CAST(N'2021-06-28T13:43:19.367' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (769, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-28T13:43:19.367' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (770, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-28T13:43:19.367' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (771, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-28T13:43:19.367' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (772, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-28T13:43:19.367' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (773, 1, 21, 1, 1, 1, 1, CAST(N'2021-06-28T13:43:19.367' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (774, 1, 2, 1, 1, 1, 1, CAST(N'2021-06-28T13:44:37.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (775, 1, 3, 1, 1, 1, 1, CAST(N'2021-06-28T13:44:37.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (776, 1, 4, 1, 1, 1, 1, CAST(N'2021-06-28T13:44:37.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (777, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-28T13:44:37.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (778, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-28T13:44:37.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (779, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-28T13:44:37.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (780, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-28T13:44:37.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (781, 1, 21, 1, 1, 1, 1, CAST(N'2021-06-28T13:44:37.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (782, 2, 2, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (783, 2, 3, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (784, 2, 4, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (785, 2, 16, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (786, 2, 17, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (787, 2, 18, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (788, 2, 19, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (789, 2, 21, 1, 1, 1, 1, CAST(N'2021-06-24T10:42:39.087' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (790, 1, 2, 0, 0, 0, 0, CAST(N'2021-06-28T13:48:42.623' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (791, 1, 3, 0, 0, 0, 0, CAST(N'2021-06-28T13:48:42.623' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (792, 1, 4, 0, 0, 0, 0, CAST(N'2021-06-28T13:48:42.623' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (793, 1, 16, 1, 1, 1, 1, CAST(N'2021-06-28T13:48:42.623' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (794, 1, 17, 1, 1, 1, 1, CAST(N'2021-06-28T13:48:42.623' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (795, 1, 18, 1, 1, 1, 1, CAST(N'2021-06-28T13:48:42.623' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (796, 1, 19, 1, 1, 1, 1, CAST(N'2021-06-28T13:48:42.623' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (797, 1, 21, 1, 1, 1, 1, CAST(N'2021-06-28T13:48:42.623' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (798, 1, 2, 0, 0, 0, 0, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (799, 1, 3, 0, 0, 0, 0, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (800, 1, 4, 0, 0, 0, 0, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (801, 1, 16, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (802, 1, 17, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (803, 1, 18, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (804, 1, 19, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (805, 1, 21, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (806, 1, 25, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (807, 1, 26, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (808, 1, 27, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (809, 1, 28, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (810, 1, 29, 1, 1, 1, 1, CAST(N'2021-07-05T13:20:42.867' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (811, 2, 2, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (812, 2, 3, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (813, 2, 4, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (814, 2, 16, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (815, 2, 17, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (816, 2, 18, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (817, 2, 19, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (818, 2, 21, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (819, 2, 25, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (820, 2, 26, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (821, 2, 27, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (822, 2, 28, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (823, 2, 29, 1, 1, 1, 1, CAST(N'2021-07-02T11:44:43.913' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (824, 1, 2, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (825, 1, 3, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (826, 1, 4, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (827, 1, 16, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (828, 1, 17, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (829, 1, 18, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (830, 1, 19, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (831, 1, 21, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (832, 1, 25, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (833, 1, 26, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (834, 1, 27, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (835, 1, 28, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (836, 1, 29, 1, 1, 1, 1, CAST(N'2021-07-05T13:21:45.707' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (837, 3, 2, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (838, 3, 3, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (839, 3, 4, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (840, 3, 16, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (841, 3, 17, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (842, 3, 18, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (843, 3, 19, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (844, 3, 21, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (845, 3, 25, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (846, 3, 26, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (847, 3, 27, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (848, 3, 28, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (849, 3, 29, 1, 1, 1, 1, CAST(N'2021-07-05T13:31:53.440' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (850, 4, 2, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (851, 4, 3, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (852, 4, 4, 1, 1, 1, 1, CAST(N'2021-08-18T14:26:57.700' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (853, 2, 2, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (854, 2, 3, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (855, 2, 4, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (856, 2, 16, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (857, 2, 17, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (858, 2, 18, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (859, 2, 19, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (860, 2, 21, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (861, 2, 25, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (862, 2, 26, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (863, 2, 27, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (864, 2, 28, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (865, 2, 29, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (866, 2, 30, 1, 1, 1, 1, CAST(N'2021-07-07T09:52:53.240' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (867, 3, 2, 0, 0, 0, 0, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (868, 3, 3, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (869, 3, 4, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (870, 3, 16, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (871, 3, 17, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (872, 3, 18, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (873, 3, 19, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (874, 3, 21, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (875, 3, 25, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (876, 3, 26, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (877, 3, 27, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (878, 3, 28, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (879, 3, 29, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (880, 3, 30, 1, 1, 1, 1, CAST(N'2021-07-07T16:10:27.963' AS DateTime), N'platform')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (881, 2, 2, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (882, 2, 3, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (883, 2, 4, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (884, 2, 16, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (885, 2, 17, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (886, 2, 18, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (887, 2, 19, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (888, 2, 21, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (889, 2, 25, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (890, 2, 26, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (891, 2, 27, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (892, 2, 28, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (893, 2, 29, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (894, 2, 30, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (895, 2, 31, 1, 1, 1, 1, CAST(N'2021-08-23T16:12:21.430' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (896, 3, 2, 0, 0, 0, 0, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (897, 3, 3, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (898, 3, 4, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (899, 3, 16, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (900, 3, 17, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (901, 3, 18, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (902, 3, 19, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (903, 3, 21, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (904, 3, 25, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (905, 3, 26, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (906, 3, 27, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (907, 3, 28, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (908, 3, 29, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (909, 3, 30, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (910, 3, 31, 1, 1, 1, 1, CAST(N'2021-08-23T16:49:36.600' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (911, 4, 2, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (912, 4, 3, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (913, 4, 4, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (914, 4, 16, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (915, 4, 17, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (916, 4, 18, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (917, 4, 19, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (918, 4, 21, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (919, 4, 25, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (920, 4, 26, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (921, 4, 27, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (922, 4, 28, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (923, 4, 29, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (924, 4, 30, 1, 1, 1, 1, CAST(N'2021-08-18T14:27:28.410' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (925, 1, 2, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (926, 1, 3, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (927, 1, 4, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (928, 1, 16, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (929, 1, 17, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (930, 1, 18, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (931, 1, 19, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (932, 1, 21, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (933, 1, 25, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (934, 1, 26, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (935, 1, 27, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (936, 1, 28, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (937, 1, 29, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (938, 1, 30, 1, 1, 1, 1, CAST(N'2021-07-07T09:53:07.100' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (939, 1, 2, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (940, 1, 3, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (941, 1, 4, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (942, 1, 16, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (943, 1, 17, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (944, 1, 18, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (945, 1, 19, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (946, 1, 21, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (947, 1, 25, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (948, 1, 26, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (949, 1, 27, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (950, 1, 28, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (951, 1, 29, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (952, 1, 30, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (953, 1, 31, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (954, 1, 32, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:13.897' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (955, 1, 2, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (956, 1, 3, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (957, 1, 4, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (958, 1, 16, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (959, 1, 17, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (960, 1, 18, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (961, 1, 19, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (962, 1, 21, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (963, 1, 25, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (964, 1, 26, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (965, 1, 27, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (966, 1, 28, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (967, 1, 29, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (968, 1, 30, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (969, 1, 31, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (970, 1, 32, 1, 1, 1, 0, CAST(N'2021-10-14T13:14:10.373' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (971, 1, 2, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (972, 1, 3, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (973, 1, 4, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (974, 1, 16, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (975, 1, 17, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (976, 1, 18, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (977, 1, 19, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (978, 1, 21, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (979, 1, 25, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (980, 1, 26, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (981, 1, 27, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (982, 1, 28, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (983, 1, 29, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (984, 1, 30, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (985, 1, 31, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (986, 1, 32, 1, 1, 1, 1, CAST(N'2021-10-14T13:14:23.450' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (987, 1, 2, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (988, 1, 3, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (989, 1, 4, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (990, 1, 16, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (991, 1, 17, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (992, 1, 18, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (993, 1, 19, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (994, 1, 21, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (995, 1, 25, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (996, 1, 26, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (997, 1, 27, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (998, 1, 28, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (999, 1, 29, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
GO
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1000, 1, 30, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1001, 1, 31, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1002, 1, 32, 0, 0, 0, 0, CAST(N'2021-10-14T13:14:47.780' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1003, 1, 2, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1004, 1, 3, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1005, 1, 4, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1006, 1, 16, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1007, 1, 17, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1008, 1, 18, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1009, 1, 19, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1010, 1, 21, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1011, 1, 25, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1012, 1, 26, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1013, 1, 27, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1014, 1, 28, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1015, 1, 29, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1016, 1, 30, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1017, 1, 31, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1018, 1, 32, 1, 0, 0, 0, CAST(N'2021-10-14T13:15:14.420' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1019, 1, 2, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1020, 1, 3, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1021, 1, 4, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1022, 1, 16, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1023, 1, 17, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1024, 1, 18, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1025, 1, 19, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1026, 1, 21, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1027, 1, 25, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1028, 1, 26, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1029, 1, 27, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1030, 1, 29, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1031, 1, 30, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1032, 1, 31, 1, 1, 1, 1, CAST(N'2021-10-14T13:15:58.447' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1033, 3, 2, 0, 0, 0, 0, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1034, 3, 3, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1035, 3, 4, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1036, 3, 16, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1037, 3, 17, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1038, 3, 18, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1039, 3, 19, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1040, 3, 21, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1041, 3, 25, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1042, 3, 26, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1043, 3, 27, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1044, 3, 29, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1045, 3, 30, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1046, 3, 31, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1047, 3, 2, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1048, 3, 3, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1049, 4, 2, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1050, 4, 3, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1051, 4, 4, 1, 1, 1, 1, CAST(N'2021-10-06T10:08:08.210' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1052, 5, 2, 1, 1, 1, 1, CAST(N'2021-12-27T22:38:29.253' AS DateTime), N'tuanpm')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1053, 5, 3, 1, 1, 1, 1, CAST(N'2021-12-27T22:38:29.253' AS DateTime), N'tuanpm')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1054, 5, 4, 1, 1, 1, 1, CAST(N'2021-12-27T22:38:29.253' AS DateTime), N'tuanpm')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1055, 3, 2, 0, 0, 0, 0, CAST(N'2021-12-21T10:45:26.517' AS DateTime), N'admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1056, 3, 3, 0, 0, 0, 1, CAST(N'2021-12-21T10:45:26.517' AS DateTime), N'admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1057, 3, 4, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1058, 3, 16, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1059, 3, 17, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1060, 3, 18, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1061, 3, 19, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1062, 3, 21, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1063, 3, 25, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1064, 3, 26, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1065, 3, 27, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1066, 3, 28, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1067, 3, 29, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1068, 3, 30, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1069, 3, 31, 1, 1, 1, 1, CAST(N'2021-11-05T14:23:04.680' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1070, 3, 32, 1, 1, 1, 1, CAST(N'2021-10-06T10:07:58.317' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1071, 3, 2, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1072, 3, 3, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1073, 3, 4, 1, 1, 1, 0, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserFunction_Log] ([LogID], [UserID], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1074, 3, 5, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
SET IDENTITY_INSERT [dbo].[CMS_UserFunction_Log] OFF
GO
SET IDENTITY_INSERT [dbo].[CMS_UserType] ON 

INSERT [dbo].[CMS_UserType] ([UserType], [UserTypeName], [Description], [CreatedUser], [CreatedTime]) VALUES (1, N'Admin', N'Full quyền hệ thống', N'thangtv', CAST(N'2021-06-23T16:56:33.513' AS DateTime))
INSERT [dbo].[CMS_UserType] ([UserType], [UserTypeName], [Description], [CreatedUser], [CreatedTime]) VALUES (2, N'QTV FPay', N'Quản trị viên FPay', N'Admin', CAST(N'2021-07-05T13:30:04.133' AS DateTime))
INSERT [dbo].[CMS_UserType] ([UserType], [UserTypeName], [Description], [CreatedUser], [CreatedTime]) VALUES (3, N'QTV Platform', N'Phòng Platform', N'Admin', CAST(N'2021-07-05T13:30:26.747' AS DateTime))
SET IDENTITY_INSERT [dbo].[CMS_UserType] OFF
GO
SET IDENTITY_INSERT [dbo].[CMS_UserTypeFunction] ON 

INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (18, 1, 2, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (19, 1, 3, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (20, 1, 4, 1, 1, 1, 0, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (21, 1, 5, 1, 1, 1, 1, CAST(N'2021-12-28T09:37:59.410' AS DateTime), N'Admin')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (8, 3, 2, 0, 0, 0, 0, CAST(N'2021-12-21T11:37:55.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (9, 3, 3, 0, 0, 0, 0, CAST(N'2021-12-21T11:37:55.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (10, 3, 4, 0, 0, 0, 0, CAST(N'2021-12-21T11:37:55.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (11, 3, 8, 0, 0, 0, 0, CAST(N'2021-12-21T11:37:55.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (13, 3, 9, 0, 0, 0, 0, CAST(N'2021-12-21T11:37:55.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (12, 3, 10, 0, 0, 0, 0, CAST(N'2021-12-21T11:37:55.077' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUserType]) VALUES (14, 3, 14, 0, 0, 0, 0, CAST(N'2021-12-21T11:37:55.077' AS DateTime), N'thangtv')
SET IDENTITY_INSERT [dbo].[CMS_UserTypeFunction] OFF
GO
SET IDENTITY_INSERT [dbo].[CMS_UserTypeFunction_Log] ON 

INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (1, 3, 2, 0, 0, 0, 0, CAST(N'2021-12-21T11:36:50.680' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (2, 3, 3, 0, 0, 0, 0, CAST(N'2021-12-21T11:36:50.680' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (3, 3, 4, 0, 0, 0, 0, CAST(N'2021-12-21T11:36:50.680' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (4, 3, 8, 1, 1, 1, 1, CAST(N'2021-12-21T11:36:50.680' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (5, 3, 9, 1, 1, 1, 1, CAST(N'2021-12-21T11:36:50.680' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (6, 3, 10, 1, 1, 1, 1, CAST(N'2021-12-21T11:36:50.680' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (7, 3, 14, 1, 1, 1, 1, CAST(N'2021-12-21T11:36:50.680' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (8, 1, 2, 1, 1, 1, 1, CAST(N'2021-12-21T11:38:24.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (9, 1, 3, 1, 1, 1, 1, CAST(N'2021-12-21T11:38:24.860' AS DateTime), N'thangtv')
INSERT [dbo].[CMS_UserTypeFunction_Log] ([LogID], [UserType], [FunctionID], [IsGrant], [IsInsert], [IsUpdate], [IsDelete], [CreatedTime], [CreatedUser]) VALUES (10, 1, 4, 1, 1, 1, 1, CAST(N'2021-12-21T11:38:24.860' AS DateTime), N'thangtv')
SET IDENTITY_INSERT [dbo].[CMS_UserTypeFunction_Log] OFF
GO
SET IDENTITY_INSERT [dbo].[ErrorLog] ON 

INSERT [dbo].[ErrorLog] ([ErrorLogID], [ErrorTime], [UserName], [HostName], [ErrorNumber], [ErrorCode], [ErrorSeverity], [ErrorState], [ErrorProcedure], [ErrorLine], [ErrorMessage], [Parameters]) VALUES (1, CAST(N'2021-12-28T09:37:50.910' AS DateTime), N'dbo', N'ADMIN', 205, 0, 16, 1, NULL, 1, N'All queries combined using a UNION, INTERSECT or EXCEPT operator must have an equal number of expressions in their target lists.', N'')
SET IDENTITY_INSERT [dbo].[ErrorLog] OFF
GO
/****** Object:  Index [Idx_ParentID]    Script Date: 18/02/2022 8:53:00 SA ******/
CREATE NONCLUSTERED INDEX [Idx_ParentID] ON [dbo].[CMS_Functions]
(
	[ParentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [Idx_FullName_Email_Mobile]    Script Date: 18/02/2022 8:53:00 SA ******/
CREATE NONCLUSTERED INDEX [Idx_FullName_Email_Mobile] ON [dbo].[CMS_User]
(
	[FullName] ASC,
	[Email] ASC,
	[Mobile] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [Idx_Username]    Script Date: 18/02/2022 8:53:00 SA ******/
CREATE NONCLUSTERED INDEX [Idx_Username] ON [dbo].[CMS_User]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [Idx_UserPass]    Script Date: 18/02/2022 8:53:00 SA ******/
CREATE NONCLUSTERED INDEX [Idx_UserPass] ON [dbo].[CMS_User]
(
	[UserName] ASC,
	[Password] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [Idx_UserName]    Script Date: 18/02/2022 8:53:00 SA ******/
CREATE NONCLUSTERED INDEX [Idx_UserName] ON [dbo].[CMS_UserLogs]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMS_Functions] ADD  CONSTRAINT [DF_CMS_Functions_IsDisplay]  DEFAULT ((1)) FOR [IsDisplay]
GO
ALTER TABLE [dbo].[CMS_Functions] ADD  CONSTRAINT [DF_Functions_CreatedTime]  DEFAULT (getdate()) FOR [CreatedTime]
GO
ALTER TABLE [dbo].[CMS_Media] ADD  CONSTRAINT [DF_CMS_Media_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[CMS_User] ADD  CONSTRAINT [DF_User_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[CMS_User] ADD  CONSTRAINT [DF_User_CreatedDate]  DEFAULT (getdate()) FOR [CreatedTime]
GO
ALTER TABLE [dbo].[CMS_UserFunction] ADD  CONSTRAINT [DF_CMS_UserFunction_CreatedTime]  DEFAULT (getdate()) FOR [CreatedTime]
GO
ALTER TABLE [dbo].[CMS_UserLogs] ADD  CONSTRAINT [DF_UserLogs_LogType]  DEFAULT ((0)) FOR [LogType]
GO
ALTER TABLE [dbo].[CMS_UserLogs] ADD  CONSTRAINT [DF_UserLogs_LogTime]  DEFAULT (getdate()) FOR [CreatedTime]
GO
ALTER TABLE [dbo].[CMS_UserType] ADD  CONSTRAINT [DF_CMS_UserType_CreateTime]  DEFAULT (getdate()) FOR [CreatedTime]
GO
ALTER TABLE [dbo].[CMS_UserTypeFunction] ADD  CONSTRAINT [DF_CMS_UserTypeFunction_CreatedTime]  DEFAULT (getdate()) FOR [CreatedTime]
GO
ALTER TABLE [dbo].[ErrorLog] ADD  CONSTRAINT [DF_ErrorLog_ErrorTime]  DEFAULT (getdate()) FOR [ErrorTime]
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_Function_GetList]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_Function_GetList]
	@FunctionID int= 0,
	@FunctionName nvarchar(150)= '',
	@IsDisplay smallint= -1, -- -1: all; 1: yes; 0:no
	@IsActive smallint= -1,-- -1: all; 1: yes; 0:no
	@ParentID int= -1,-- -1: lay tat ca;  <>-1: lay tat ca Funct co cha la @ParentID
	@ActionName nvarchar(200)= '',
	@Page int = 1,
	@PageSize int = 100,
	@TotalRow int = 0 OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Function TABLE (	[STT] [int] IDENTITY,
								[FunctionID] [int],
								[FunctionName] [nvarchar](150),
								[LinkHttp] [nvarchar](150),
								[LinkFunction] [nvarchar](150),
								[IsDisplay] [smallint],
								[IsActive] [smallint],
								[ParentID] [int],
								[ParentName] [nvarchar](150),
								[CssIcon] [nvarchar](50),
								[ActionName] [nvarchar](200),
								[ByOrder] [smallint],
								[CreateUser] [varchar](50),
								[CreatedTime] [datetime])
	SELECT	@FunctionName = CASE WHEN LEN(@FunctionName)>1 THEN '%' +@FunctionName+'%' ELSE '' END,
			@Page= ISNULL(@Page,1),
			@PageSize = ISNULL(@PageSize,100)
	IF(ISNULL(@FunctionID,0)>0)
	BEGIN
		INSERT INTO @Function
		SELECT F1.[FunctionID]
			  ,F1.[FunctionName]
			  ,F1.[LinkHttp]
			  ,F1.[LinkFunction]
			  ,F1.[IsDisplay]
			  ,F1.[IsActive]
			  ,F1.[ParentID]
			  ,ISNULL(F2.[FunctionName],'Root')
			  ,F1.[CssIcon]
			  ,F1.[ActionName]
			  ,F1.[ByOrder]
			  ,F1.[CreateUser]
			  ,F1.[CreatedTime]
		FROM [dbo].[CMS_Functions] F1
			LEFT JOIN [dbo].[CMS_Functions] F2 ON F1.[ParentID] = F2. [FunctionID]
		WHERE F1.FunctionID = @FunctionID

		SET @TotalRow = @@ROWCOUNT
	END 
	ELSE BEGIN
		INSERT INTO @Function
		SELECT F1.[FunctionID]
			  ,F1.[FunctionName]
			  ,F1.[LinkHttp]
			  ,F1.[LinkFunction]
			  ,F1.[IsDisplay]
			  ,F1.[IsActive]
			  ,F1.[ParentID]
			  ,ISNULL(F2.[FunctionName],'Root')
			  ,F1.[CssIcon]
			  ,F1.[ActionName]
			  ,F1.[ByOrder]
			  ,F1.[CreateUser]
			  ,F1.[CreatedTime]
		FROM [dbo].[CMS_Functions] F1
			LEFT JOIN [dbo].[CMS_Functions] F2 ON F1.[ParentID] = F2. [FunctionID]
		WHERE (ISNULL(@FunctionName,'')='' OR F1.FunctionName  lIKE @FunctionName)
			AND (ISNULL(@IsActive,-1)=-1 OR F1.IsActive = @IsActive)
			AND (ISNULL(@IsDisplay,-1) =-1 OR F1.IsDisplay = @IsDisplay)
			AND (ISNULL(@ParentID,-1)=-1 OR F1.ParentID = @ParentID)
			AND (ISNULL(@ActionName,'')='' OR F1.ActionName = @ActionName)
		ORDER BY F1.FunctionID ASC

		SET @TotalRow = @@ROWCOUNT
	END

	SELECT *
	FROM @Function
	WHERE STT BETWEEN (@Page-1)*@PageSize+1 AND @Page*@PageSize
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_Function_INUP]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_Function_INUP]
	@FunctionID int= NULL,
	@FunctionName nvarchar(150)= NULL,
	@LinkHttp nvarchar(150)= NULL,
	@LinkFunction nvarchar(150)= NULL,
	@IsDisplay smallint= NULL,
	@IsActive smallint= NULL,
	@ParentID int= NULL,
	@CssIcon nvarchar(50)= NULL,
	@ActionName nvarchar(200)= NULL,
	@ByOrder smallint = NULL,-- Thu tu hien thi
	@CreateUser varchar(50)= NULL,
	@ResponseStatus int = 0 OUTPUT
	-- >0: Thanh cong
	-- -60: input co truong de trong
	-- -99: Loi ko xac dinh
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE()
	
	IF(ISNULL(@CreateUser,'')=''
		OR (ISNULL(@FunctionID,0)=0
			AND (ISNULL(@FunctionName,'')='' )))
	BEGIN
		SET @ResponseStatus = -60
		RETURN
	END

	BEGIN TRY
		IF(ISNULL(@FunctionID,0) =0)
		BEGIN	
			SELECT @ByOrder = ISNULL(MAX([ByOrder]),0)+1
			FROM [dbo].[CMS_Functions]
			WHERE ParentID = @ParentID

			INSERT INTO [dbo].[CMS_Functions]
				([FunctionName]
				  ,[LinkHttp]
				  ,[LinkFunction]
				  ,[IsDisplay]
				  ,[IsActive]
				  ,[ParentID]
				  ,[CssIcon]
				  ,[ActionName]
				  ,[ByOrder]
				  ,[CreateUser]
				  ,[CreatedTime])
			VALUES(
					ISNULL(@FunctionName,''),
					ISNULL(@LinkHttp,''),
					ISNULL(@LinkFunction,''),
					ISNULL(@IsDisplay,0),
					ISNULL(@IsActive,0),
					ISNULL(@ParentID,-1),
					ISNULL(@CssIcon,''),
					ISNULL(@ActionName,''),
					ISNULL(@ByOrder,1),
					@CreateUser,
					@Now)

			SET @ResponseStatus = @@ROWCOUNT
		END
		ELSE BEGIN
			UPDATE TOP(1) [dbo].[CMS_Functions]
			SET	FunctionName = ISNULL(@FunctionName,FunctionName),
				LinkHttp = ISNULL(@LinkHttp,LinkHttp),
				LinkFunction = ISNULL(@LinkFunction,LinkFunction),
				IsDisplay = ISNULL(@IsDisplay,IsDisplay),
				IsActive = ISNULL(@IsActive,IsActive),
				ParentID = ISNULL(@ParentID,ParentID),
				CssIcon = ISNULL(@CssIcon,CssIcon),
				ActionName = ISNULL(@ActionName,ActionName),
				ByOrder = ISNULL(@ByOrder,ByOrder),
				CreateUser = @CreateUser,
				CreatedTime = @Now
			WHERE FunctionID = @FunctionID

			SET @ResponseStatus = @@ROWCOUNT
		END
	END TRY
	BEGIN CATCH
		SET @ResponseStatus = -99
		EXEC SP_LogError
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_Function_UpdateOrder]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_Function_UpdateOrder]
	@FunctionOrder varchar(500), -- Sap xep: ParentID,FunctionID,ByOrder;ParentID,FunctionID,ByOrder;
	@CreateUser varchar(50),
	@ResponseStatus int = 0 OUTPUT
	-- >0: Thanh cong
	-- -60: input co truong de trong
	-- -99: Loi ko xac dinh
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE()
	
	IF(ISNULL(@CreateUser,'')=''
		OR (ISNULL(@FunctionOrder,'')='' ))
	BEGIN
		SET @ResponseStatus = -60
		RETURN
	END

	BEGIN TRY
		UPDATE [dbo].[CMS_Functions]
		SET [ParentID] = S.Value1,
			[ByOrder] = S.Value3
		FROM [dbo].[CMS_Functions] F
			JOIN [dbo].[FN_Parameters_SplitTable](@FunctionOrder,';',',') S
				ON F.FunctionID = S.Value2
					AND S.Value2 IS NOT NULL
		SET @ResponseStatus = @@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @ResponseStatus = -99
		EXEC SP_LogError
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_Media_GetList]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_Media_GetList]
	@MediaID int= 0,
	@MediaType smallint= 0,-- 0: all;1:Image; 2: Video; 3:File
	@MediaName varchar(150)= '',
	@CreateUser varchar(50)= '',
	@FromDate date = NULL,
	@ToDate date = NULL,
	@Page int =1,
	@PageSize int =100,
	@TotalRow int = 0 OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE(),
			@From int =0,
			@To int = 0
	
	DECLARE @Media TABLE (	[STT] [int] IDENTITY(1,1),
							[MediaID] [int],
							[MediaType] [smallint],
							[MediaName] [varchar](150),
							[Path] [nvarchar](200),
							[Width] [int],
							[Height] [int],
							[Alt] [varchar](50),
							[TitleShare] [nvarchar](500),
							[Extend] [varchar](10),
							[CreateUser] [varchar](50),
							[CreateTime] [datetime],
							[DateInt] [int])
	
	SELECT	@Page = ISNULL(@Page,1),
			@PageSize = ISNULL(@PageSize,100),
			@MediaName = CASE WHEN LEN(@MediaName)>1 THEN '%'+@MediaName +'%' ELSE ''END,
			@CreateUser = CASE WHEN LEN(@CreateUser)>1 THEN '%'+@CreateUser +'%' ELSE ''END,
			@FromDate = ISNULL(@FromDate, DATEADD(D,-10,@Now)),
			@ToDate = ISNULL(@ToDate,@Now)
	SELECT @From = [dbo].[FN_DateToInt](@FromDate),
			@To = [dbo].[FN_DateToInt](@ToDate)
	
	IF(ISNULL(@MediaID,0)>0)
	BEGIN
		INSERT INTO @Media
		SELECT TOP (1000) [MediaID]
			  ,[MediaType]
			  ,[MediaName]
			  ,[Path]
			  ,[Width]
			  ,[Height]
			  ,[Alt]
			  ,[TitleShare]
			  ,[Extend]
			  ,[CreateUser]
			  ,[CreateTime]
			  ,[DateInt]
		FROM [dbo].[CMS_Media]
		WHERE MediaID = @MediaID
		SET @TotalRow = @@ROWCOUNT
	END	
	ELSE BEGIN
		INSERT INTO @Media
		SELECT TOP (1000) [MediaID]
			  ,[MediaType]
			  ,[MediaName]
			  ,[Path]
			  ,[Width]
			  ,[Height]
			  ,[Alt]
			  ,[TitleShare]
			  ,[Extend]
			  ,[CreateUser]
			  ,[CreateTime]
			  ,[DateInt]
		FROM [dbo].[CMS_Media]
		WHERE	(ISNULL(@MediaName,'')='' OR MediaName LIKE @MediaName)
			AND (ISNULL(@CreateUser,'')='' OR CreateUser LIKE @CreateUser)
			AND (ISNULL(@MediaType,0)= 0 OR MediaType = @MediaType)
			AND DateInt BETWEEN @From AND @To

		SET @TotalRow = @@ROWCOUNT
	END

	SELECT *
	FROM @Media
	WHERE STT BETWEEN (@Page-1)*@PageSize +1 AND @Page*@PageSize
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_Media_INUP]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_Media_INUP]
	@MediaID int= 0,
	@MediaType smallint= NULL,--1:Image; 2: Video; 3:File
	@MediaName varchar(150)= NULL,
	@Path nvarchar(200)= NULL,
	@Width int= NULL,
	@Height int= NULL,
	@Alt varchar(50)= NULL,
	@TitleShare nvarchar(500)= NULL,
	@Extend varchar(10)= NULL,
	@CreateUser varchar(50)= NULL,
	@ResponseStatus int = 0 OUTPUT
	-- >0: Thanh cong
	-- -60: input co truogn de trong
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE(),
			@DateInt int = [dbo].[FN_DateToInt](GETDATE())

    IF(ISNULL(@CreateUser,'')='' 
		OR (ISNULL(@MediaID,0)=0 
			AND ( ISNULL(@MediaType,0)=0
					OR ISNULL(@MediaName,'')=''
					OR ISNULL(@Path,'')='')))
	BEGIN
		SET @ResponseStatus = -60
		RETURN
	END

	BEGIN TRY
		IF(ISNULL(@MediaID,0)=0)
		BEGIN
			INSERT INTO [dbo].[CMS_Media]
			   ([MediaType]
			   ,[MediaName]
			   ,[Path]
			   ,[Width]
			   ,[Height]
			   ,[Alt]
			   ,[TitleShare]
			   ,[Extend]
			   ,[CreateUser]
			   ,[CreateTime]
			   ,[DateInt])
			VALUES( ISNULL(@MediaType,0),
					ISNULL(@MediaName,''),
					ISNULL(@Path,''),
					ISNULL(@Width,0),
					ISNULL(@Height,0),
					ISNULL(@Alt,''),
					ISNULL(@TitleShare,''),
					ISNULL(@Extend,''),
					@CreateUser,
					@Now,
					@DateInt)
			SET @ResponseStatus = @@ROWCOUNT
		END
		ELSE BEGIN
			UPDATE TOP(1) [dbo].[CMS_Media]
			SET MediaType = ISNULL(@MediaType,MediaType),
				MediaName = ISNULL(@MediaName,MediaName),
				Path = ISNULL(@Path,Path),
				Width = ISNULL(@Width,Width),
				Height = ISNULL(@Height,Height),
				Alt = ISNULL(@Alt,Alt),
				TitleShare = ISNULL(@TitleShare,TitleShare),
				Extend = ISNULL(@Extend,Extend),
				CreateUser = @CreateUser,
				CreateTime = @Now,
				DateInt = @DateInt
			WHERE MediaID = @MediaID

			SET @ResponseStatus = @@ROWCOUNT
		END 
	END TRY
	BEGIN CATCH
		SET @ResponseStatus = -99
		EXEC SP_LogError
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_User_Authen]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_User_Authen]
	@UserName varchar(50),-- '': all
	@Password varchar(64),
	@ResponseStatus int =0 output
	-- >0: dang nhap thanh cong
	-- -50: Tai khoan ko ton tai
	-- -54: Tai khoan da bi khoa
	-- -55: Mat khau khong hop le
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE(),
			@PassCurr varchar(64),
			@StatusCurr smallint 

	SELECT TOP(1) @PassCurr = [Password],@StatusCurr = [Status], @ResponseStatus = [UserID]
	FROM [dbo].[CMS_User]
	WHERE UserName = @UserName

	IF(ISNULL(@PassCurr,'')='')
	BEGIN
		SET @ResponseStatus = -50
		RETURN
	END

	IF(ISNULL(@StatusCurr,0)<>1)
	BEGIN
		SET @ResponseStatus = -54
		RETURN
	END
	
	SELECT @Password = [dbo].[FN_PasswordEncrypt](@UserName,@Password)
	--SELECT @Password
	IF(ISNULL(@Password,'') <> @PassCurr)
	BEGIN
		SET @ResponseStatus = -55
		RETURN
	END
	DECLARE @User TABLE (	[STT] [int] IDENTITY(1,1),
							[UserID] [int],
							[UserName] [varchar](50),
							[UserType] [smallint],
							[UserTypeName] [nvarchar](50),
							[FullName] [nvarchar](150),
							[Email] [varchar](150),
							[Mobile] [varchar](50),
							[DateOfBirth] [date],
							[NickName] [nvarchar](50),
							[User_Img] [nvarchar](200),
							[User_Info] [nvarchar](500),
							[Status] [smallint],
							[CreatedUser] [varchar](50),
							[CreatedTime] [datetime])
	
	INSERT INTO @User
	SELECT     [UserID]
			,U.[UserName]
			,U.[UserType]
			,UT.[UserTypeName]
			,U.[FullName]
			,U.[Email]
			,U.[Mobile]
			,U.[DateOfBirth]
			,U.[NickName]
			,U.[User_Img]
			,U.[User_Info]
			,U.[Status]
			,U.[CreatedUser]
			,U.[CreatedTime]
	FROM [dbo].[CMS_User] U 
		LEFT JOIN [dbo].[CMS_UserType] UT ON U.UserType = UT.UserType
	WHERE UserName = @UserName

	
	SELECT * 
	FROM @User
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_User_ChangePass]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_User_ChangePass]
	@UserName varchar(50),-- '': all
	@PasswordOld varchar(64),
	@PasswordNew varchar(64),
	@ResponseStatus int =0 output
	-- >0: dang nhap thanh cong
	-- -50: Tai khoan ko ton tai
	-- -54: Tai khoan da bi khoa
	-- -55: Mat khau cu khong chinh xac
	-- -60: Input khong hop le
	-- -99: Loi ko xac dinh

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE(),
			@PassCurr varchar(64),
			@StatusCurr smallint 
	IF(ISNULL(@PasswordNew,'')='')
	BEGIN
		SET @ResponseStatus =-60
		RETURN
	END

	SELECT TOP(1) @PassCurr = [Password],@StatusCurr = [Status]
	FROM [dbo].[CMS_User]
	WHERE UserName = @UserName

	IF(ISNULL(@PassCurr,'')='')
	BEGIN
		SET @ResponseStatus = -50
		RETURN
	END

	IF(ISNULL(@StatusCurr,0)<>1)
	BEGIN
		SET @ResponseStatus = -54
		RETURN
	END

	SELECT @PasswordOld = [dbo].[FN_PasswordEncrypt](@UserName,@PasswordOld)

	IF(ISNULL(@PasswordOld,'') <> @PassCurr)
	BEGIN
		SET @ResponseStatus = -55
		RETURN
	END

	BEGIN TRY
		UPDATE TOP(1) [dbo].[CMS_User]
		SET Password = [dbo].[FN_PasswordEncrypt](@UserName,@PasswordNew)
		WHERE UserName = @UserName
		
		SET @ResponseStatus = @@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @ResponseStatus = -99
		EXEC SP_LogError
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_User_GetList]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_User_GetList]
	@UserID int= 0,-- 0:all
	@UserName varchar(50)= '',-- '': all
	@UserType smallint= -1,-- -1: all
	@SearchText nvarchar(50)='',
	@Status smallint= -1, -- -1:all; 1: active; 0: disable
	@Page int = 1,
	@PageSize int =100,
	@TotalRow int =0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE()
	SELECT	@UserName = CASE WHEN  LEN(@UserName)>1 THEN '%' +@UserName +'%' ELSE '' END,
			@SearchText = CASE WHEN  LEN(@SearchText)>1 THEN '%' +@SearchText +'%' ELSE '' END,
			@Page = ISNULL(@Page,1),
			@PageSize = ISNULL(@PageSize,100)
	DECLARE @User TABLE (	[STT] [int] IDENTITY(1,1),
							[UserID] [int],
							[UserName] [varchar](50),
							[UserType] [smallint],
							[UserTypeName] [nvarchar](50),
							[FullName] [nvarchar](150),
							[Email] [varchar](150),
							[Mobile] [varchar](50),
							[DateOfBirth] [date],
							[NickName] [nvarchar](50),
							[User_Img] [nvarchar](200),
							[User_Info] [nvarchar](500),
							[Status] [smallint],
							[CreatedUser] [varchar](50),
							[CreatedTime] [datetime])
	IF(ISNULL(@UserID,0)>0)							
	BEGIN
		INSERT INTO @User
		SELECT TOP (1) [UserID]
			  ,U.[UserName]
			  ,U.[UserType]
			  ,UT.[UserTypeName]
			  ,U.[FullName]
			  ,U.[Email]
			  ,U.[Mobile]
			  ,U.[DateOfBirth]
			  ,U.[NickName]
			  ,U.[User_Img]
			  ,U.[User_Info]
			  ,U.[Status]
			  ,U.[CreatedUser]
			  ,U.[CreatedTime]
		FROM [dbo].[CMS_User] U 
			LEFT JOIN [dbo].[CMS_UserType] UT ON U.UserType = UT.UserType
		WHERE UserID = @UserID

		SET @TotalRow = @@ROWCOUNT
	END
	ELSE BEGIN
	INSERT INTO @User
		SELECT TOP (1000) [UserID]
			  ,U.[UserName]
			  ,U.[UserType]
			  ,UT.[UserTypeName]
			  ,U.[FullName]
			  ,U.[Email]
			  ,U.[Mobile]
			  ,U.[DateOfBirth]
			  ,U.[NickName]
			  ,U.[User_Img]
			  ,U.[User_Info]
			  ,U.[Status]
			  ,U.[CreatedUser]
			  ,U.[CreatedTime]
		FROM [dbo].[CMS_User] U 
			LEFT JOIN [dbo].[CMS_UserType] UT ON U.UserType = UT.UserType
		WHERE (ISNULL(@Status,-1) =-1 OR U.Status = @Status)
			AND (ISNULL(@UserName,'')='' OR UserName LIKE @UserName)
			AND (ISNULL(@UserType,-1)=-1 OR U.UserType = @UserType)
			AND (ISNULL(@SearchText,'')='' OR U.FullName LIKE @SearchText
											OR U.Email LIKE @SearchText
											OR U.Mobile LIKE @SearchText
											OR U.NickName LIKE @SearchText)

		SET @TotalRow = @@ROWCOUNT
	END
	SELECT *
	FROM @User
	WHERE STT BETWEEN (@Page-1)*@PageSize +1 AND @Page*@PageSize
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_User_INUP]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_User_INUP]
	@UserID int= 0,-- 0:insert; <>0: update
	@UserName varchar(50)= NULL,-- *:bat buoc khi them moi
	@UserType smallint= NULL,
	@Password varchar(64)= NULL,-- *
	@FullName nvarchar(150)= NULL,-- *
	@Email varchar(150)= NULL,-- *
	@Mobile varchar(50)= NULL,-- *
	@DateOfBirth date= NULL,
	@NickName nvarchar(50)= NULL,
	@User_Img nvarchar(200)= NULL,
	@User_Info nvarchar(500)= NULL,
	@Status smallint= NULL, -- 1: active; 0: disable
	@CreatedUser varchar(50),
	@ResponseStatus int = 0 output
	-- >0: thanh cong;
	-- -50: Da ton tai UserName nay
	-- -60: input co truong de trong
	-- -99: Loi ko xac dinh
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE(),
			@FunctionDetail varchar(1000),
			@UserType_Cur int =0

	IF(ISNULL(@CreatedUser,'')=''
		OR (ISNULL(@UserID,0)= 0 
			AND (ISNULL(@UserName,'')=''
				OR ISNULL(@Password,'')=''
				OR ISNULL(@FullName,'')=''
				OR ISNULL(@Email,'')=''
				OR ISNULL(@Mobile,'')='')))
	BEGIN
		SET @ResponseStatus = -60
		RETURN
	END
	IF(ISNULL(@UserID,0)=0 
		AND EXISTS(SELECT TOP(1)1 FROM[dbo].[CMS_User]
					WHERE UserName = @UserName ))
	BEGIN
		SET @ResponseStatus = -50
		RETURN
	END

	BEGIN TRY
		IF(ISNULL(@UserID,0)=0)
		BEGIN
			INSERT INTO [dbo].[CMS_User]
			   ([UserName]
			   ,[UserType]
			   ,[Password]
			   ,[FullName]
			   ,[Email]
			   ,[Mobile]
			   ,[DateOfBirth]
			   ,[NickName]
			   ,[User_Img]
			   ,[User_Info]
			   ,[Status]
			   ,[CreatedUser]
			   ,[CreatedTime])
			SELECT @UserName,
					ISNULL(@UserType,0),
					[dbo].[FN_PasswordEncrypt](@UserName,@Password),
					ISNULL(@FullName,''),
					ISNULL(@Email,''),
					ISNULL(@Mobile,''),
					ISNULL(@DateOfBirth,GETDATE()),
					ISNULL(@NickName,''),
					ISNULL(@User_Img,''),
					ISNULL(@User_Info,''),
					ISNULL(@Status,1),
					@CreatedUser,
					@Now
			SET @ResponseStatus =@@IDENTITY

			-- Lay quyen theo nhom
			IF(@UserType >0)
			BEGIN
				INSERT INTO [dbo].[CMS_UserFunction]
					   ([UserID]
					   ,[FunctionID]
					   ,[IsGrant]
					   ,[IsInsert]
					   ,[IsUpdate]
					   ,[IsDelete]
					   ,[CreatedTime]
					   ,[CreatedUser])
				SELECT  @ResponseStatus [UserID]
					   ,[FunctionID]
					   ,[IsGrant]
					   ,[IsInsert]
					   ,[IsUpdate]
					   ,[IsDelete]
					   ,@Now [CreatedTime]
					   ,@CreatedUser [CreatedUser]
				FROM [dbo].[CMS_UserTypeFunction] WITH(NOLOCK)
				WHERE [UserType] = @UserType
			END
		END
		ELSE BEGIN
			SELECT TOP(1)  @UserType_Cur = [UserType]
			FROM [dbo].[CMS_User]
			WHERE UserID = @UserID 

			UPDATE TOP(1) [dbo].[CMS_User]
			SET UserType = ISNULL(@UserType,UserType),
				Password = CASE WHEN ISNULL(@Password,[Password])<>[Password] THEN [dbo].[FN_PasswordEncrypt](@UserName,@Password) ELSE [Password] END   ,
				FullName = ISNULL(@FullName,FullName),
				Email = ISNULL(@Email,Email),
				Mobile = ISNULL(@Mobile,Mobile),
				DateOfBirth = ISNULL(@DateOfBirth,DateOfBirth),
				NickName = ISNULL(@NickName,NickName),
				User_Img = ISNULL(@User_Img,User_Img),
				User_Info = ISNULL(@User_Info,User_Info),
				Status = ISNULL(@Status,Status),
				CreatedUser = @CreatedUser,
				CreatedTime = @Now
			WHERE UserID = @UserID 

			SET @ResponseStatus =@@ROWCOUNT

			IF @ResponseStatus >0 AND ISNULL(@UserType_Cur,0) <> ISNULL(@UserType,0)
			BEGIN
				INSERT INTO [dbo].[CMS_UserFunction_Log]
				SELECT [UserID]
					  ,[FunctionID]
					  ,[IsGrant]
					  ,[IsInsert]
					  ,[IsUpdate]
					  ,[IsDelete]
					  ,[CreatedTime]
					  ,[CreatedUser]
				FROM [dbo].[CMS_UserFunction]
				WHERE UserID = @UserID

				DELETE FROM [dbo].[CMS_UserFunction] 
				WHERE UserID = @UserID 

				INSERT INTO [dbo].[CMS_UserFunction]
					   ([UserID]
					   ,[FunctionID]
					   ,[IsGrant]
					   ,[IsInsert]
					   ,[IsUpdate]
					   ,[IsDelete]
					   ,[CreatedTime]
					   ,[CreatedUser])
				SELECT  @UserID [UserID]
					   ,[FunctionID]
					   ,[IsGrant]
					   ,[IsInsert]
					   ,[IsUpdate]
					   ,[IsDelete]
					   ,@Now [CreatedTime]
					   ,@CreatedUser [CreatedUser]
				FROM [dbo].[CMS_UserTypeFunction] WITH(NOLOCK)
				WHERE [UserType] = @UserType
			END
		END
	END TRY
	BEGIN CATCH
		SET @ResponseStatus =-99
		EXEC SP_LogError
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_UserFunction_GetbyUser]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_UserFunction_GetbyUser]
	@UserID int,
	@FunctionID int=0,
	@Page int =1,
	@PageSize int = 100,
	@TotalRow int = 0 OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE()
	DECLARE @Function TABLE (	[STT] [int] IDENTITY,
								[UserID] [int],
								[FunctionID] [int],
								[FunctionName] [nvarchar](150),
								[ParenID] [int],
								[ParentName] [nvarchar](150),
								[LinkFunction] [nvarchar](150),
								[ActionName] [nvarchar](200),
								[IsGrant] [bit],
								[IsInsert] [bit],
								[IsUpdate] [bit],
								[IsDelete] [bit],
								[CreatedTime] [datetime],
								[CreatedUser] [nvarchar](50))
	SELECT	@Page = ISNULL(@Page,1),
			@PageSize = ISNULL(@PageSize,100),
			@TotalRow =0
	IF(EXISTS(SELECT TOP(1)1 FROM [dbo].[CMS_User] WHERE UserID = @UserID AND Status = 1))
	BEGIN
		INSERT INTO @Function
		SELECT UF.[UserID]
			  ,UF.[FunctionID]
			  ,F1.FunctionName
			  ,F1.ParentID
			  ,F2.FunctionName
			  ,F1.[LinkFunction]
			  ,F1.[ActionName]
			  ,UF.[IsGrant]
			  ,UF.[IsInsert]
			  ,UF.[IsUpdate]
			  ,UF.[IsDelete]
			  ,UF.[CreatedTime]
			  ,UF.[CreatedUser]
		FROM [dbo].[CMS_UserFunction] UF WITH(NOLOCK)
			LEFT JOIN [dbo].[CMS_Functions] F1 ON UF.FunctionID = F1.FunctionID
			LEFT JOIN [dbo].[CMS_Functions] F2 ON F1.ParentID = F2.FunctionID
		WHERE UserID = @UserID
			AND (ISNULL(@FunctionID,0)=0 OR UF.FunctionID = @FunctionID)
			AND (CONVERT(TINYINT,UF.[IsGrant]) +CONVERT(TINYINT,UF.[IsInsert]) +CONVERT(TINYINT,UF.[IsUpdate])+CONVERT(TINYINT,UF.[IsDelete])) >0
		ORDER BY F1.ParentID ASC, F1.ByOrder ASC

		SET @TotalRow = @@ROWCOUNT

		INSERT INTO @Function
		SELECT @UserID [UserID]
			  ,F1.[FunctionID]
			  ,F1.FunctionName
			  ,F1.ParentID
			  ,F2.FunctionName
			  ,F1.[LinkFunction]
			  ,F1.[ActionName]
			  ,1 [IsGrant]
			  ,1 [IsInsert]
			  ,1 [IsUpdate]
			  ,1 [IsDelete]
			  ,@Now [CreatedTime]
			  ,''[CreatedUser]
		FROM [dbo].[CMS_Functions] F1  
			LEFT JOIN [dbo].[CMS_Functions] F2 ON F1.ParentID = F2.FunctionID
		WHERE F1.[FunctionID] IN (SELECT [ParenID] FROM @Function)
			--AND F1.[FunctionID] NOT IN(SELECT [FunctionID] FROM @Function)
	
		SET @TotalRow = ISNULL(@TotalRow,0)+@@ROWCOUNT
	END

	SELECT *
	FROM @Function
	WHERE STT BETWEEN (@Page-1)*@PageSize +1 AND @Page*@PageSize

END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_UserFunction_INUP]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_UserFunction_INUP]
	@UserID int,
	@FunctionDetail nvarchar(MAX),-- FunctionID,IsGrant,IsInsert,Isupdate,IsDelete;..
	@CreatedUser nvarchar(50),
	@ResponseStatus int = 0 OUTPUT
	-- >0: thanh cong
	-- -60: chuoi input ko dung dinh dang
	-- -99: Loi ko xac dinh
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE()
	DECLARE @Function TABLE (	[STT] [int] IDENTITY,
								[FunctionID] [int],
								[IsGrant] [bit],
								[IsInsert] [bit],
								[Isupdate] [bit],
								[IsDelete] [bit])

	BEGIN TRY
		INSERT INTO @Function(FunctionID,IsGrant,IsInsert,Isupdate,IsDelete)
		EXECUTE [dbo].[SP_SplitToTableWithMultiColumn] 
		   @FunctionDetail 
		  ,';'
		  ,','

	END TRY
	BEGIN CATCH
		SET @ResponseStatus = -60
		EXEC SP_LogError
		RETURN
	END CATCH

	BEGIN TRY
		INSERT INTO [dbo].[CMS_UserFunction_Log]
		SELECT [UserID]
			  ,[FunctionID]
			  ,[IsGrant]
			  ,[IsInsert]
			  ,[IsUpdate]
			  ,[IsDelete]
			  ,[CreatedTime]
			  ,[CreatedUser]
		FROM [dbo].[CMS_UserFunction]
		WHERE UserID = @UserID
			AND FunctionID IN (SELECT FunctionID FROM @Function)
		
		DELETE FROM [dbo].[CMS_UserFunction]
		WHERE UserID = @UserID
			AND FunctionID IN (SELECT FunctionID FROM @Function)
		
		INSERT INTO [dbo].[CMS_UserFunction]
			(	[UserID]
			   ,[FunctionID]
			   ,[IsGrant]
			   ,[IsInsert]
			   ,[IsUpdate]
			   ,[IsDelete]
			   ,[CreatedTime]
			   ,[CreatedUser])
		SELECT  @UserID
			   ,[FunctionID]
			   ,[IsGrant]
			   ,[IsInsert]
			   ,[IsUpdate]
			   ,[IsDelete]
			   ,@Now
			   ,@CreatedUser
		FROM @Function

		SET @ResponseStatus =@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @ResponseStatus =-99
		EXEC SP_LogError
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_UserLog_GetList]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		
-- Create date: 
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_UserLog_GetList]
	 @UserName nvarchar(30) =''
	,@FunctionName nvarchar(100) = ''
	,@LogType int = 0 --0 : Log chuc nang, 1 : Log Login, 3: Log đôi mật khẩu
	,@FromDate date = NULL
	,@ToDate date = NULL
	,@Page int = 1
	,@PageSize int = 100
	,@TotalRow int =0 output
	
AS
BEGIN
	SET NOCOUNT, XACT_ABORT ON
	DECLARE @Date date = getdate(),
			@From int =0,
			@To int =0
	DECLARE @UserLog TABLE (	[STT] [int] IDENTITY,
								[LogID] [int],
								[FunctionID] [int],
								[UserName] [varchar](50),
								[FullName] [nvarchar](100),
								[FunctionName] [nvarchar](100),
								[Description] [nvarchar](2000),
								[LogType] [int],
								[ClientIP] [varchar](64),
								[CreatedTime] [datetime])
	SELECT	@Username = CASE WHEN LEN(@UserName)>1 THEN '%'+@UserName+'%' ELSE '' END,
			@FromDate = ISNULL(@FromDate, DATEADD(D,-7,@Date)),
			@ToDate = ISNULL(@ToDate,@Date),
			@Page = ISNULL(@Page,1),
			@PageSize = ISNULL(@PageSize,100)

	SELECT @From = [dbo].[FN_DateToInt](@FromDate),
			@To = [dbo].[FN_DateToInt](@ToDate)
	
	INSERT INTO @UserLog
	SELECT [LogID]
		  ,[FunctionID]
		  ,[UserName]
		  ,[Fullname]
		  ,[FunctionName]
		  ,[Description]
		  ,[LogType]
		  ,[ClientIP]
		  ,[CreatedTime]
	FROM [dbo].[CMS_UserLogs]
	WHERE (ISNULL(@UserName,'')='' OR Username LIKE @UserName)
		AND (ISNULL(@FunctionName,'')='' OR FunctionName LIKE @FunctionName
				OR  [Description] LIKE @FunctionName)
		AND (ISNULL(@LogType,-1) =-1 OR LogType = @LogType)
		AND DateInt BETWEEN @From AND @To
	ORDER BY LogID DESC

	SET @TotalRow = @@ROWCOUNT

	SELECT *
	FROM @UserLog
	WHERE STT BETWEEN (@Page-1)*@PageSize+1 AND @Page*@PageSize
	
END



GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_UserLog_Insert]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: 
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_UserLog_Insert]
	 @Username varchar(50) 
	,@FunctionID int
	,@FunctionName nvarchar(100) = ''
	,@Description nvarchar(1000) =''
	,@LogType int = 0 --0 : Log chuc nang, 1 : Log Login, 3: Log đôi mật khẩu
	,@ClientIP varchar(20) = null
	,@ResponseCode int =0 output
	-- >0: Thanh cong
	-- -99: Loi ko xac dinh
AS
BEGIN
	SET NOCOUNT, XACT_ABORT ON
	
	SET @LogType = ISNULL(@LogType,0)
	SET @ClientIP = ISNULL(@ClientIP,'')
	DECLARE @Now datetime = GETDATE()
	



	BEGIN TRY
		
		INSERT INTO [dbo].[CMS_UserLogs]
           ([FunctionID]
           ,[Username]
           ,[Fullname]
           ,[FunctionName]
           ,[Description]
           ,[LogType]
           ,[ClientIP]
           ,[CreatedTime]
           ,[DateInt])
		SELECT   @FunctionID
				,@Username
				,Fullname
				,@FunctionName
				,@Description
				,@LogType
				,@ClientIP
				,@Now
				,[dbo].[FN_DateToInt](@Now)
		FROM dbo.[CMS_User] 
		WHERE [Username] = @Username

		SET @ResponseCode = @@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @ResponseCode = -99;
		EXEC	[dbo].[sp_LogError]
				
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_UserType_GetList]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_UserType_GetList]
	@UserType smallint= 0,-- 0:all
	@UserTypeName nvarchar(50)= '',
	@TotalRow int = 0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE()
	SELECT @UserTypeName = CASE WHEN LEN(@UserTypeName)>1 THEN '%' +@UserTypeName +'%' ELSE '' END

	SELECT TOP (1000) [UserType]
      ,[UserTypeName]
      ,[Description]
      ,[CreatedUser]
      ,[CreatedTime]
	FROM [dbo].[CMS_UserType] 
	WHERE	(ISNULL(@UserType,0)= 0 OR UserType = @UserType) 
		AND (ISNULL(@UserTypeName,'')='' OR UserTypeName LIKE @UserTypeName)
	ORDER BY UserType ASC 

	SET @TotalRow = @@ROWCOUNT

END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_UserType_INUP]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_UserType_INUP]
	@UserType smallint= 0,-- 0:insert; <>0: update
	@UserTypeName nvarchar(50)= NULL,
	@Description nvarchar(500)= NULL,
	@CreatedUser varchar(50),
	@ResponseStatus int = 0 output
	-- >0: thanh cong;
	-- -60: input co truong de trong
	-- -99: Loi ko xac dinh
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE()

	IF(ISNULL(@CreatedUser,'')=''
		OR (ISNULL(@UserType,0)= 0 AND ISNULL(@UserTypeName,'')=''))
	BEGIN
		SET @ResponseStatus = -60
		RETURN
	END

	BEGIN TRY
		IF(ISNULL(@UserType,0)=0)
		BEGIN
			INSERT INTO [dbo].[CMS_UserType]
				   ([UserTypeName]
				   ,[Description]
				   ,[CreatedUser]
				   ,[CreatedTime])
			VALUES (ISNULL(@UserTypeName,''),
					ISNULL(@Description,''),
					@CreatedUser,
					@Now)

			SET @ResponseStatus =@@ROWCOUNT
		END
		ELSE BEGIN
			UPDATE TOP(1) [dbo].[CMS_UserType]
			SET UserTypeName = ISNULL(@UserTypeName,UserTypeName),
				Description = ISNULL(@Description,Description),
				CreatedUser = @CreatedUser,
				CreatedTime = @Now
			WHERE UserType = @UserType

			SET @ResponseStatus =@@ROWCOUNT
		END
	END TRY
	BEGIN CATCH
		SET @ResponseStatus =-99
		EXEC SP_LogError
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_UserTypeFunction_GetbyUserType]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_UserTypeFunction_GetbyUserType]
	@UserType int,
	@FunctionID int=0,
	@Page int =1,
	@PageSize int = 100,
	@TotalRow int = 0 OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE()
	DECLARE @Function TABLE (	[STT] [int] IDENTITY,
								[UserType] [int],
								[FunctionID] [int],
								[FunctionName] [nvarchar](150),
								[ParenID] [int],
								[ParentName] [nvarchar](150),
								[LinkFunction] [nvarchar](150),
								[ActionName] [nvarchar](200),
								[IsGrant] [bit],
								[IsInsert] [bit],
								[IsUpdate] [bit],
								[IsDelete] [bit],
								[CreatedTime] [datetime],
								[CreatedUserType] [nvarchar](50))
	SELECT	@Page = ISNULL(@Page,1),
			@PageSize = ISNULL(@PageSize,100),
			@TotalRow =0
	IF(EXISTS(SELECT TOP(1)1 FROM [dbo].[CMS_UserType] WHERE UserType = @UserType))
	BEGIN
		INSERT INTO @Function
		SELECT UF.[UserType]
			  ,UF.[FunctionID]
			  ,F1.FunctionName
			  ,F1.ParentID
			  ,F2.FunctionName
			  ,F1.[LinkFunction]
			  ,F1.[ActionName]
			  ,UF.[IsGrant]
			  ,UF.[IsInsert]
			  ,UF.[IsUpdate]
			  ,UF.[IsDelete]
			  ,UF.[CreatedTime]
			  ,UF.[CreatedUserType]
		FROM [dbo].[CMS_UserTypeFunction] UF WITH(NOLOCK)
			LEFT JOIN [dbo].[CMS_Functions] F1 ON UF.FunctionID = F1.FunctionID
			LEFT JOIN [dbo].[CMS_Functions] F2 ON F1.ParentID = F2.FunctionID
		WHERE UserType = @UserType
			AND (ISNULL(@FunctionID,0)=0 OR UF.FunctionID = @FunctionID)
			AND (CONVERT(TINYINT,UF.[IsGrant]) +CONVERT(TINYINT,UF.[IsInsert]) +CONVERT(TINYINT,UF.[IsUpdate])+CONVERT(TINYINT,UF.[IsDelete])) >0
		ORDER BY F1.ParentID ASC, F1.ByOrder ASC

		SET @TotalRow = @@ROWCOUNT

		INSERT INTO @Function
		SELECT @UserType [UserType]
			  ,F1.[FunctionID]
			  ,F1.FunctionName
			  ,F1.ParentID
			  ,F2.FunctionName
			  ,F1.[LinkFunction]
			  ,F1.[ActionName]
			  ,1 [IsGrant]
			  ,1 [IsInsert]
			  ,1 [IsUpdate]
			  ,1 [IsDelete]
			  ,@Now [CreatedTime]
			  ,''[CreatedUserType]
		FROM [dbo].[CMS_Functions] F1 
			LEFT JOIN [dbo].[CMS_Functions] F2 ON F1.ParentID = F2.FunctionID
		WHERE F1.[FunctionID] IN (SELECT [ParenID] FROM @Function)
			--AND F1.[FunctionID] NOT IN(SELECT [FunctionID] FROM @Function)
	
		SET @TotalRow = ISNULL(@TotalRow,0)+@@ROWCOUNT
	END

	SELECT *
	FROM @Function
	WHERE STT BETWEEN (@Page-1)*@PageSize +1 AND @Page*@PageSize

END
GO
/****** Object:  StoredProcedure [dbo].[SP_CMS_UserTypeFunction_INUP]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CMS_UserTypeFunction_INUP]
	@UserType int,
	@FunctionDetail nvarchar(MAX),-- FunctionID,IsGrant,IsInsert,Isupdate,IsDelete;..
	@CreatedUserType nvarchar(50),
	@ResponseStatus int = 0 OUTPUT
	-- >0: thanh cong
	-- -60: chuoi input ko dung dinh dang
	-- -99: Loi ko xac dinh
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Now datetime = GETDATE(),
			@i int =1,
			@TotalUser int = 0,
			@UserID int
	DECLARE @Function TABLE (	[STT] [int] IDENTITY,
								[FunctionID] [int],
								[IsGrant] [bit],
								[IsInsert] [bit],
								[Isupdate] [bit],
								[IsDelete] [bit])
	DECLARE @Function_Old TABLE (	[FunctionID] [int],
									[IsGrant] [bit],
									[IsInsert] [bit],
									[Isupdate] [bit],
									[IsDelete] [bit])
	DECLARE @User TABLE([STT] [int] IDENTITY,
						[UserID] [int])

	BEGIN TRY
		INSERT INTO @Function(FunctionID,IsGrant,IsInsert,Isupdate,IsDelete)
		EXECUTE [dbo].[SP_SplitToTableWithMultiColumn] 
		   @FunctionDetail 
		  ,';'
		  ,','

	END TRY
	BEGIN CATCH
		SET @ResponseStatus = -60
		EXEC SP_LogError
		RETURN
	END CATCH

	BEGIN TRY
		IF(EXISTS(SELECT TOP(1)1 FROM [dbo].[CMS_UserType] WHERE UserType = @UserType ))
		BEGIN 
			INSERT INTO @User
			SELECT UserID
			FROM [dbo].[CMS_User]
			WHERE UserType = @UserType

			SET @TotalUser = @@ROWCOUNT

			INSERT INTO @Function_Old
			SELECT [FunctionID]
				  ,[IsGrant]
				  ,[IsInsert]
				  ,[IsUpdate]
				  ,[IsDelete]
			FROM [dbo].[CMS_UserTypeFunction] WITH(NOLOCK)
			WHERE UserType = @UserType

			DELETE FROM [dbo].[CMS_UserFunction]
			WHERE UserID IN(SELECT UserID FROM @User)
				AND FunctionID IN(SELECT [FunctionID] FROM @Function_Old)
			
		END

		INSERT INTO [dbo].[CMS_UserTypeFunction_Log]
		SELECT [UserType]
			  ,[FunctionID]
			  ,[IsGrant]
			  ,[IsInsert]
			  ,[IsUpdate]
			  ,[IsDelete]
			  ,[CreatedTime]
			  ,[CreatedUserType]
		FROM [dbo].[CMS_UserTypeFunction] 
		WHERE UserType = @UserType
		
		DELETE FROM [dbo].[CMS_UserTypeFunction]
		WHERE UserType = @UserType
		

		INSERT INTO [dbo].[CMS_UserTypeFunction]
			(	[UserType]
			   ,[FunctionID]
			   ,[IsGrant]
			   ,[IsInsert]
			   ,[IsUpdate]
			   ,[IsDelete]
			   ,[CreatedTime]
			   ,[CreatedUserType])
		SELECT  @UserType
			   ,[FunctionID]
			   ,[IsGrant]
			   ,[IsInsert]
			   ,[IsUpdate]
			   ,[IsDelete]
			   ,@Now
			   ,@CreatedUserType
		FROM @Function

		SET @ResponseStatus =@@ROWCOUNT
		
		WHILE(@i <= @TotalUser)
			BEGIN
				SELECT @UserID = UserID 
				FROM @User
				WHERE STT = @i

				INSERT INTO [dbo].[CMS_UserFunction]
					   ([UserID]
					   ,[FunctionID]
					   ,[IsGrant]
					   ,[IsInsert]
					   ,[IsUpdate]
					   ,[IsDelete]
					   ,[CreatedTime]
					   ,[CreatedUser])
				SELECT  @UserID
					   ,[FunctionID]
					   ,[IsGrant]
					   ,[IsInsert]
					   ,[IsUpdate]
					   ,[IsDelete]
					   ,@Now [CreatedTime]
					   ,@CreatedUserType [CreatedUser]
				FROM @Function F
				WHERE NOT EXISTS (	SELECT TOP(1)1 FROM [dbo].[CMS_UserFunction] UF WITH(NOLOCK) 
									WHERE F.FunctionID = UF.FunctionID AND UF.UserID = @UserID) 
				
				SET @i = @i + 1
			END
	END TRY
	BEGIN CATCH
		SET @ResponseStatus =-99
		EXEC SP_LogError
	END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[SP_LogError]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO












-- ==============================================================================
-- Author		:  ngokhong
-- ALTER date	: 18/04/2020
-- Description	: Mô tả @_ErrorCode  	
--					1	: Cập nhật thành công
--					0	: Lỗi thực thi
--					-1	: ERROR with @@ROWCOUNT to validate the operation 			
--					-2	: Cannot UPDATE,INSERT duplicate key row unique 
--					-3 : value NULL into column  does not allow nulls.
--					-4 : Error converting data type varchar to numeric.
--					-5 : Conversion failed when converting datetime from character string.
--					-6 : The UPDATE,INSERT statement conflicted with the FOREIGN KEY constraint 
--					-7 : Cannot INSERT explicit value for identity column in table  when IDENTITY_INSERT is set to OFF. 
--					-8 : The conversion value overflowed an int column. Maximum VALUE exceeded.
--					-9 : Transaction was deadlocked on lock resources with another process and has been chosen as the deadlock victim.
--					
-- ==============================================================================
CREATE PROCEDURE [dbo].[SP_LogError]
	@_Parameters nvarchar(1000) = '', --Chuoi cac tham so dau vao phan tach boi dau '|'
    @_ErrorCode int = 0 output
--WITH ENCRYPTION
AS 
BEGIN
    SET NOCOUNT ON;
    SET @_ErrorCode = 0;

    BEGIN TRY

        IF (ERROR_NUMBER() IS NULL) RETURN;
        IF (XACT_STATE() = -1)
        BEGIN
			PRINT 'Cannot log error since the current transaction is in an uncommittable state. '+'Rollback the transaction before executing SP_LogError in order to successfully log error information.';
			RETURN;
        END
      
        SET @_ErrorCode = CASE WHEN ERROR_NUMBER()=2601 THEN -2 -- Cannot UPDATE,INSERT duplicate key row unique 
			WHEN ERROR_NUMBER()=515 THEN -3 -- value NULL into column  does not allow nulls.
			WHEN ERROR_NUMBER() IN (245,235,8114,293) THEN -4 -- Error converting data type varchar to (INT,BIGINT,Numeric,Money,SmallMoney).
			WHEN ERROR_NUMBER()=241 THEN -5 -- Conversion failed when converting datetime from character string.
			WHEN ERROR_NUMBER()=547 THEN -6 -- The UPDATE,INSERT statement conflicted with the FOREIGN KEY constraint 
			WHEN ERROR_NUMBER()=544 THEN -7 -- Cannot INSERT explicit value for identity column in table  when IDENTITY_INSERT is set to OFF. 
			WHEN ERROR_NUMBER()=248 THEN -8 -- The conversion value overflowed an int column. Maximum VALUE exceeded.
			WHEN ERROR_NUMBER()=1205 THEN -9 -- Transaction was deadlocked on lock resources with another process and has been chosen as the deadlock victim.
			ELSE 0
        END;  -- END CASE

        INSERT [dbo].[ErrorLog]
			([ErrorTime]
			,[UserName]
			,[HostName]
			,[ErrorNumber]
			,[ErrorCode]
			,[ErrorSeverity]
			,[ErrorState]
			,[ErrorProcedure]
			,[ErrorLine]
			,[ErrorMessage]
			,[Parameters]
			)
		VALUES
			(GETDATE()
			,CONVERT(SYSNAME,CURRENT_USER)
			,HOST_NAME()
			,ERROR_NUMBER()
			,@_ErrorCode
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,ERROR_PROCEDURE()
			,ERROR_LINE()
			,ERROR_MESSAGE()
			,@_Parameters
			);

    END TRY
    BEGIN CATCH
        PRINT 'An error occurred in stored procedure SP_LogError: ';
        EXECUTE [dbo].[SP_LogErrorPrint];
        RETURN -1;
    END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[SP_LogErrorPrint]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- SP_PrintError prints error information about the error that caused 
-- execution to jump to the CATCH block of a TRY...CATCH construct. 
-- Should be executed from within the scope of a CATCH block otherwise 
-- it will return without printing any error information.
CREATE PROCEDURE    [dbo].[SP_LogErrorPrint] 
AS
BEGIN
    SET NOCOUNT ON;

    -- Print error information. 
    PRINT 'Error ' + CONVERT(varchar(50), ERROR_NUMBER()) +
          ', Severity ' + CONVERT(varchar(5), ERROR_SEVERITY()) +
          ', State ' + CONVERT(varchar(5), ERROR_STATE()) + 
          ', Procedure ' + ISNULL(ERROR_PROCEDURE(), '-') + 
          ', Line ' + CONVERT(varchar(5), ERROR_LINE());
    PRINT ERROR_MESSAGE();
END;
GO
/****** Object:  StoredProcedure [dbo].[SP_Parameters_SplitTable_Multi]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author		:  anhtd
-- ALTER date	: 
-- Description	: Tach chuoi sang bang nhieu cot
-- ==============================================================================

CREATE PROC [dbo].[SP_Parameters_SplitTable_Multi] 
(
	@Split NVARCHAR(MAX),
	@Delimiter1 VARCHAR(1),
	@Delimiter2 VARCHAR(1)
)
as
SET @Split = RTRIM(LTRIM(@Split));

DECLARE @count INT
DECLARE @tblSplit TABLE(ID INT, Value NVARCHAR(500))
DECLARE @spl NVARCHAR(MAX) = '';

INSERT INTO @tblSplit ( ID, Value )
SELECT [Index], [Value] FROM [dbo].[FN_Parameters_Split](@Split, @Delimiter1)
WHERE ISNULL([Value],'')<>''

SET @count = @@ROWCOUNT

WHILE @count > 0
BEGIN
	DECLARE @Value NVARCHAR(500), @ID INT
	SELECT TOP 1 @ID = ID, @Value = Value FROM @tblSplit
	IF @count > 1
		SET @spl = @spl + 'SELECT ' + '''' + REPLACE(@Value, @Delimiter2, '''' + @Delimiter2 + 'N''') + '''' + ' UNION ALL '
	ELSE
		SET @spl = @spl + 'SELECT ' + '''' + REPLACE(@Value, @Delimiter2, '''' + @Delimiter2 + 'N''') + ''''
	DELETE FROM @tblSplit WHERE ID = @ID
	SET @count = @count - 1
END

--PRINT @spl
EXEC sp_executesql @spl
GO
/****** Object:  StoredProcedure [dbo].[SP_SplitToTableWithMultiColumn]    Script Date: 18/02/2022 8:53:00 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_SplitToTableWithMultiColumn] 
(
	@Split NVARCHAR(MAX),
	@Delimiter1 VARCHAR(1),
	@Delimiter2 VARCHAR(1)
)
as
SET @Split = RTRIM(LTRIM(@Split));

DECLARE @count INT
DECLARE @tblSplit TABLE(ID INT, Value NVARCHAR(500))
DECLARE @spl NVARCHAR(MAX) = '';

INSERT INTO @tblSplit ( ID, Value )
SELECT [Index], [Value] FROM [dbo].[FN_Parameters_Split](@Split, @Delimiter1)
WHERE ISNULL([Value],'')<>''

SET @count = @@ROWCOUNT

WHILE @count > 0
BEGIN
	DECLARE @Value NVARCHAR(500), @ID INT
	SELECT TOP 1 @ID = ID, @Value = Value FROM @tblSplit
	IF @count > 1
		SET @spl = @spl + 'SELECT ' + '''' + REPLACE(@Value, @Delimiter2, '''' + @Delimiter2 + 'N''') + '''' + ' UNION ALL '
	ELSE
		SET @spl = @spl + 'SELECT ' + '''' + REPLACE(@Value, @Delimiter2, '''' + @Delimiter2 + 'N''') + ''''
	DELETE FROM @tblSplit WHERE ID = @ID
	SET @count = @count - 1
END

PRINT @spl
EXEC sp_executesql @spl








GO
USE [master]
GO
ALTER DATABASE [HCNS.CMSAdminDB] SET  READ_WRITE 
GO
