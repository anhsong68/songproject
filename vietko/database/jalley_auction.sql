-- --------------------------------------------------------
-- Host:                         192.168.1.254
-- Server version:               10.7.3-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for jalley_auction
DROP DATABASE IF EXISTS `jalley_auction`;
CREATE DATABASE IF NOT EXISTS `jalley_auction` /*!40100 DEFAULT CHARACTER SET utf8mb3 */;
USE `jalley_auction`;

-- Dumping structure for table jalley_auction.auction
DROP TABLE IF EXISTS `auction`;
CREATE TABLE IF NOT EXISTS `auction` (
  `auction_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '경매아이디',
  `auction_type` varchar(45) NOT NULL COMMENT '경매종류',
  `auction_title` varchar(45) NOT NULL COMMENT '경매제목',
  `auction_contents` longtext DEFAULT NULL COMMENT '경매내용',
  `auction_unit_price` bigint(20) NOT NULL COMMENT '경매단위가격',
  `auction_start_price` bigint(20) NOT NULL COMMENT '경매시작가',
  `auction_end_price` bigint(20) DEFAULT NULL COMMENT '경매종료가',
  `auction_start_date` datetime NOT NULL COMMENT '경매시작일',
  `auction_end_date` datetime NOT NULL COMMENT '경매종료일',
  `rent_start_date` datetime DEFAULT NULL COMMENT '대여시작일',
  `rent_end_date` datetime DEFAULT NULL COMMENT '대여종료일',
  `auction_status` varchar(45) NOT NULL COMMENT '경매상태',
  `main_yn` varchar(1) NOT NULL COMMENT '메인표시여부',
  `display_yn` varchar(1) NOT NULL COMMENT '상품노출여부',
  `use_yn` varchar(1) NOT NULL COMMENT '사용여부',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '등록일',
  `create_id` varchar(32) NOT NULL COMMENT '등록자',
  PRIMARY KEY (`auction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='경매';

-- Dumping data for table jalley_auction.auction: ~0 rows (approximately)
DELETE FROM `auction`;
/*!40000 ALTER TABLE `auction` DISABLE KEYS */;
INSERT INTO `auction` (`auction_id`, `auction_type`, `auction_title`, `auction_contents`, `auction_unit_price`, `auction_start_price`, `auction_end_price`, `auction_start_date`, `auction_end_date`, `rent_start_date`, `rent_end_date`, `auction_status`, `main_yn`, `display_yn`, `use_yn`, `create_date`, `create_id`) VALUES
	(1, 'signature_auction', 'AUCTION 1', '<p><strong>당신의 직감을 가지고 가십시오</strong></p><p>당신의 외모는 하룻밤 사이에 시그니처 스타일이 되지는 않을 것입니다. 그것은 당신이 실제로 좋아하지 않는 것이라면 훨씬 덜 가능성이 훨씬 적은 몇 번의 외출이 필요합니다. 맥도날드는 &ldquo;대부분 실험에 관한 것이지만 당신의 직감은 종종 옳습니다. 방에 들어갈 때 뭔가 불편하다면 그건 당신을 위한 것이 아닙니다.&rdquo;</p><p>종종 이러한 불편함은 외부가 내부와 일치하지 않을 때 발생합니다. &quot;[당신의 옷]은 당신이 누구인지를 반영하고 당신이 말을 하기도 전에 당신에 대해 무언가를 표현합니다.&quot;라고 맥도날드는 말합니다. 그것은 당신이 세상을 바라보는 방식을 반영하고 그것이 당신을 보고 싶어하는 모습을 선택하는 것을 의미합니다. 당신의 성격에 어울리지 않는 옷을 입으면 그것이 드러날 것입니다.<br />위대한 사람에게서 영감을 얻다</p><p>이전에 시도한 적이 없는 스타일 동작이 거의 없습니다. 즉, 리더를 따라 플레이하는 것처럼 간단하게 자신만의 스타일을 찾을 수 있습니다. 맥도날드는 &ldquo;당신과 닮은 남자를 보고 그것에 기초를 두십시오.</p><p>특히 키가 크고 마른 체형이라면 Jeff Goldblum의 개성 중심의 맞춤 재단 방식이 청사진이 될 수 있습니다. 확실히 그렇지 않다면 영감을 얻기 위해 Jonah Hill의 스트리트웨어가 많은 옷장을 살펴보십시오. &quot;시행착오가 있을 수 있지만, 다른 남자를 흉내내는 것이 실수를 많이 줄이는 좋은 방법입니다.&quot;<br />당신을 위해 작동하는 것을 알고</p><p>시그니처 스타일의 가장 좋은 점 중 하나는 몇 벌의 옷이 어떻게 맞아야 하는지 알아내면 된다는 것입니다. 그것들을 절대적으로 완벽하게 만드는 데 충분한 시간을 할애하십시오. 맥도날드는 &ldquo;시그니처 스타일은 당신에게 적합해야 하고 신체적으로 당신의 체형에 맞아야 합니다.</p><p>가장 좋은 두 가지 방법은 전문적으로 측정하고 가능한 한 많은 브랜드를 시험해 보고 같은 치수의 남성을 위한 옷을 만드는 브랜드를 찾는 것입니다. 데님에 올인하기로 결정했다면 폭넓게 샘플링하여 가장 잘 맞는 컷과 라벨을 찾으십시오.</p>', 2000, 50000, NULL, '2022-06-08 11:03:00', '2022-06-17 12:00:00', NULL, NULL, 'auction_now', 'Y', 'Y', 'Y', '2022-06-08 11:07:26', 'admin');
/*!40000 ALTER TABLE `auction` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.auction_backup
DROP TABLE IF EXISTS `auction_backup`;
CREATE TABLE IF NOT EXISTS `auction_backup` (
  `auction_backup_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '경매백업아이디',
  `work_type` varchar(45) NOT NULL COMMENT '작업구분',
  `auction_id` bigint(20) NOT NULL COMMENT '경매아이디',
  `auction_type` varchar(45) NOT NULL,
  `auction_title` varchar(45) NOT NULL COMMENT '경매제목',
  `auction_contents` varchar(2000) NOT NULL COMMENT '경매내용',
  `auction_unit_price` bigint(20) NOT NULL,
  `auction_start_price` bigint(20) NOT NULL COMMENT '경매시작가',
  `auction_end_price` bigint(20) DEFAULT NULL COMMENT '경매종료가',
  `auction_start_date` datetime NOT NULL COMMENT '경매시작일',
  `auction_end_date` datetime NOT NULL COMMENT '경매종료일',
  `rent_start_date` datetime DEFAULT NULL,
  `rent_end_date` datetime DEFAULT NULL,
  `auction_status` varchar(45) NOT NULL COMMENT '경매상태',
  `main_yn` varchar(45) NOT NULL COMMENT '메인표시여부',
  `display_yn` varchar(1) NOT NULL COMMENT '상품노출여부',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  `change_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '변경일',
  `change_id` varchar(32) NOT NULL COMMENT '변경아이디',
  PRIMARY KEY (`auction_backup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='경매로그';

-- Dumping data for table jalley_auction.auction_backup: ~0 rows (approximately)
DELETE FROM `auction_backup`;
/*!40000 ALTER TABLE `auction_backup` DISABLE KEYS */;
INSERT INTO `auction_backup` (`auction_backup_id`, `work_type`, `auction_id`, `auction_type`, `auction_title`, `auction_contents`, `auction_unit_price`, `auction_start_price`, `auction_end_price`, `auction_start_date`, `auction_end_date`, `rent_start_date`, `rent_end_date`, `auction_status`, `main_yn`, `display_yn`, `use_yn`, `change_date`, `change_id`) VALUES
	(1, 'insert', 1, 'signature_auction', 'AUCTION 1', '<p><strong>당신의 직감을 가지고 가십시오</strong></p><p>당신의 외모는 하룻밤 사이에 시그니처 스타일이 되지는 않을 것입니다. 그것은 당신이 실제로 좋아하지 않는 것이라면 훨씬 덜 가능성이 훨씬 적은 몇 번의 외출이 필요합니다. 맥도날드는 &ldquo;대부분 실험에 관한 것이지만 당신의 직감은 종종 옳습니다. 방에 들어갈 때 뭔가 불편하다면 그건 당신을 위한 것이 아닙니다.&rdquo;</p><p>종종 이러한 불편함은 외부가 내부와 일치하지 않을 때 발생합니다. &quot;[당신의 옷]은 당신이 누구인지를 반영하고 당신이 말을 하기도 전에 당신에 대해 무언가를 표현합니다.&quot;라고 맥도날드는 말합니다. 그것은 당신이 세상을 바라보는 방식을 반영하고 그것이 당신을 보고 싶어하는 모습을 선택하는 것을 의미합니다. 당신의 성격에 어울리지 않는 옷을 입으면 그것이 드러날 것입니다.<br />위대한 사람에게서 영감을 얻다</p><p>이전에 시도한 적이 없는 스타일 동작이 거의 없습니다. 즉, 리더를 따라 플레이하는 것처럼 간단하게 자신만의 스타일을 찾을 수 있습니다. 맥도날드는 &ldquo;당신과 닮은 남자를 보고 그것에 기초를 두십시오.</p><p>특히 키가 크고 마른 체형이라면 Jeff Goldblum의 개성 중심의 맞춤 재단 방식이 청사진이 될 수 있습니다. 확실히 그렇지 않다면 영감을 얻기 위해 Jonah Hill의 스트리트웨어가 많은 옷장을 살펴보십시오. &quot;시행착오가 있을 수 있지만, 다른 남자를 흉내내는 것이 실수를 많이 줄이는 좋은 방법입니다.&quot;<br />당신을 위해 작동하는 것을 알고</p><p>시그니처 스타일의 가장 좋은 점 중 하나는 몇 벌의 옷이 어떻게 맞아야 하는지 알아내면 된다는 것입니다. 그것들을 절대적으로 완벽하게 만드는 데 충분한 시간을 할애하십시오. 맥도날드는 &ldquo;시그니처 스타일은 당신에게 적합해야 하고 신체적으로 당신의 체형에 맞아야 합니다.</p><p>가장 좋은 두 가지 방법은 전문적으로 측정하고 가능한 한 많은 브랜드를 시험해 보고 같은 치수의 남성을 위한 옷을 만드는 브랜드를 찾는 것입니다. 데님에 올인하기로 결정했다면 폭넓게 샘플링하여 가장 잘 맞는 컷과 라벨을 찾으십시오.</p>', 2000, 50000, NULL, '2022-06-08 11:03:00', '2022-06-17 12:00:00', NULL, NULL, 'auction_now', 'Y', 'Y', 'Y', '2022-06-08 11:07:26', 'admin'),
	(2, 'update', 1, 'signature_auction', 'AUCTION 1', '<p><strong>당신의 직감을 가지고 가십시오</strong></p><p>당신의 외모는 하룻밤 사이에 시그니처 스타일이 되지는 않을 것입니다. 그것은 당신이 실제로 좋아하지 않는 것이라면 훨씬 덜 가능성이 훨씬 적은 몇 번의 외출이 필요합니다. 맥도날드는 &ldquo;대부분 실험에 관한 것이지만 당신의 직감은 종종 옳습니다. 방에 들어갈 때 뭔가 불편하다면 그건 당신을 위한 것이 아닙니다.&rdquo;</p><p>종종 이러한 불편함은 외부가 내부와 일치하지 않을 때 발생합니다. &quot;[당신의 옷]은 당신이 누구인지를 반영하고 당신이 말을 하기도 전에 당신에 대해 무언가를 표현합니다.&quot;라고 맥도날드는 말합니다. 그것은 당신이 세상을 바라보는 방식을 반영하고 그것이 당신을 보고 싶어하는 모습을 선택하는 것을 의미합니다. 당신의 성격에 어울리지 않는 옷을 입으면 그것이 드러날 것입니다.<br />위대한 사람에게서 영감을 얻다</p><p>이전에 시도한 적이 없는 스타일 동작이 거의 없습니다. 즉, 리더를 따라 플레이하는 것처럼 간단하게 자신만의 스타일을 찾을 수 있습니다. 맥도날드는 &ldquo;당신과 닮은 남자를 보고 그것에 기초를 두십시오.</p><p>특히 키가 크고 마른 체형이라면 Jeff Goldblum의 개성 중심의 맞춤 재단 방식이 청사진이 될 수 있습니다. 확실히 그렇지 않다면 영감을 얻기 위해 Jonah Hill의 스트리트웨어가 많은 옷장을 살펴보십시오. &quot;시행착오가 있을 수 있지만, 다른 남자를 흉내내는 것이 실수를 많이 줄이는 좋은 방법입니다.&quot;<br />당신을 위해 작동하는 것을 알고</p><p>시그니처 스타일의 가장 좋은 점 중 하나는 몇 벌의 옷이 어떻게 맞아야 하는지 알아내면 된다는 것입니다. 그것들을 절대적으로 완벽하게 만드는 데 충분한 시간을 할애하십시오. 맥도날드는 &ldquo;시그니처 스타일은 당신에게 적합해야 하고 신체적으로 당신의 체형에 맞아야 합니다.</p><p>가장 좋은 두 가지 방법은 전문적으로 측정하고 가능한 한 많은 브랜드를 시험해 보고 같은 치수의 남성을 위한 옷을 만드는 브랜드를 찾는 것입니다. 데님에 올인하기로 결정했다면 폭넓게 샘플링하여 가장 잘 맞는 컷과 라벨을 찾으십시오.</p>', 2000, 50000, NULL, '2022-06-08 11:03:00', '2022-06-17 12:00:00', NULL, NULL, 'auction_now', 'Y', 'Y', 'Y', '2022-06-10 11:01:41', 'admin');
/*!40000 ALTER TABLE `auction_backup` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.auction_items
DROP TABLE IF EXISTS `auction_items`;
CREATE TABLE IF NOT EXISTS `auction_items` (
  `auction_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '경매상품아이디',
  `auction_id` bigint(20) NOT NULL COMMENT '경매아이디',
  `product_id` bigint(20) NOT NULL COMMENT '상품아이디',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '등록일',
  `create_id` varchar(32) NOT NULL COMMENT '등록자',
  PRIMARY KEY (`auction_item_id`),
  KEY `FK_auction_items_auction_id_auction_auction_id` (`auction_id`),
  KEY `FK_auction_items_product_id_product_product_id` (`product_id`),
  CONSTRAINT `FK_auction_items_auction_id_auction_auction_id` FOREIGN KEY (`auction_id`) REFERENCES `auction` (`auction_id`),
  CONSTRAINT `FK_auction_items_product_id_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='경매상품';

-- Dumping data for table jalley_auction.auction_items: ~2 rows (approximately)
DELETE FROM `auction_items`;
/*!40000 ALTER TABLE `auction_items` DISABLE KEYS */;
INSERT INTO `auction_items` (`auction_item_id`, `auction_id`, `product_id`, `use_yn`, `create_date`, `create_id`) VALUES
	(1, 1, 203, 'Y', '2022-06-08 11:07:26', 'admin'),
	(2, 1, 202, 'Y', '2022-06-08 11:07:26', 'admin');
/*!40000 ALTER TABLE `auction_items` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.auction_items_backup
DROP TABLE IF EXISTS `auction_items_backup`;
CREATE TABLE IF NOT EXISTS `auction_items_backup` (
  `auction_items_backup_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '경매아이템백업아이디',
  `work_type` varchar(45) DEFAULT NULL COMMENT '작업구분',
  `auction_item_id` bigint(20) DEFAULT NULL COMMENT '경매상품아이디',
  `auction_id` bigint(20) DEFAULT NULL COMMENT '경매아이디',
  `product_id` bigint(20) DEFAULT NULL COMMENT '상품아이디',
  `use_yn` varchar(1) DEFAULT NULL COMMENT '사용유무',
  `change_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '등록일',
  `change_id` varchar(32) NOT NULL COMMENT '등록자',
  PRIMARY KEY (`auction_items_backup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='경매상품로그';

-- Dumping data for table jalley_auction.auction_items_backup: ~2 rows (approximately)
DELETE FROM `auction_items_backup`;
/*!40000 ALTER TABLE `auction_items_backup` DISABLE KEYS */;
INSERT INTO `auction_items_backup` (`auction_items_backup_id`, `work_type`, `auction_item_id`, `auction_id`, `product_id`, `use_yn`, `change_date`, `change_id`) VALUES
	(1, 'insert', 1, 1, 203, 'Y', '2022-06-08 11:07:26', 'admin'),
	(2, 'insert', 2, 1, 202, 'Y', '2022-06-08 11:07:26', 'admin');
/*!40000 ALTER TABLE `auction_items_backup` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.bid
DROP TABLE IF EXISTS `bid`;
CREATE TABLE IF NOT EXISTS `bid` (
  `bid_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '입찰아이디',
  `auction_id` bigint(20) DEFAULT NULL COMMENT '경매아이디',
  `bid_price` bigint(20) DEFAULT NULL COMMENT '입찰가격',
  `bidder_id` varchar(32) DEFAULT NULL COMMENT '입찰자아이디',
  `create_date` datetime DEFAULT current_timestamp() COMMENT '생성일',
  `create_id` varchar(32) DEFAULT NULL COMMENT '생성자',
  PRIMARY KEY (`bid_id`),
  KEY `FK_bid_auction_id_auction_auction_id` (`auction_id`),
  CONSTRAINT `FK_bid_auction_id_auction_auction_id` FOREIGN KEY (`auction_id`) REFERENCES `auction` (`auction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='입찰내역';

-- Dumping data for table jalley_auction.bid: ~2 rows (approximately)
DELETE FROM `bid`;
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
INSERT INTO `bid` (`bid_id`, `auction_id`, `bid_price`, `bidder_id`, `create_date`, `create_id`) VALUES
	(1, 1, 52000, 'emp10', '2022-06-08 11:10:00', 'emp10'),
	(2, 1, 54000, 'emp10', '2022-06-08 11:26:40', 'emp10');
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.board
DROP TABLE IF EXISTS `board`;
CREATE TABLE IF NOT EXISTS `board` (
  `board_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '게시판아이디',
  `board_type` varchar(45) DEFAULT NULL COMMENT '게시판종류',
  `board_kind` varchar(45) DEFAULT NULL COMMENT '게시판스킨',
  `user_id` varchar(32) DEFAULT NULL COMMENT '사용자아이디',
  `board_title` varchar(100) DEFAULT NULL COMMENT '제목',
  `board_contens` text DEFAULT NULL COMMENT '내용',
  `noti_yn` varchar(1) DEFAULT NULL COMMENT '공지유무',
  `noti_start_date` datetime DEFAULT NULL COMMENT '공지시작일',
  `noti_end_date` datetime DEFAULT NULL COMMENT '공지종료일',
  `popup_yn` varchar(1) DEFAULT NULL COMMENT '팝업유무',
  `popup_sort` int(11) DEFAULT NULL COMMENT '팝업정렬',
  `create_date` datetime DEFAULT NULL COMMENT '생성일',
  `create_id` varchar(32) DEFAULT NULL COMMENT '생성자',
  PRIMARY KEY (`board_id`),
  KEY `FK_board_board_type_board_type_board_type` (`board_type`),
  CONSTRAINT `FK_board_board_type_board_type_board_type` FOREIGN KEY (`board_type`) REFERENCES `board_type` (`board_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table jalley_auction.board: ~0 rows (approximately)
DELETE FROM `board`;
/*!40000 ALTER TABLE `board` DISABLE KEYS */;
/*!40000 ALTER TABLE `board` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.board_type
DROP TABLE IF EXISTS `board_type`;
CREATE TABLE IF NOT EXISTS `board_type` (
  `board_type` varchar(45) NOT NULL COMMENT '게시판종류',
  `board_name` varchar(45) DEFAULT NULL COMMENT '게시판명',
  `use_yn` varchar(45) DEFAULT NULL COMMENT '사용유무',
  `create_date` datetime DEFAULT NULL COMMENT '생성일',
  `create_id` varchar(32) DEFAULT NULL COMMENT '생성자',
  PRIMARY KEY (`board_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='게시판종류';

-- Dumping data for table jalley_auction.board_type: ~0 rows (approximately)
DELETE FROM `board_type`;
/*!40000 ALTER TABLE `board_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `board_type` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.cart
DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '장바구니아이디',
  `cart_type` varchar(45) NOT NULL COMMENT '장바구니종류',
  `user_id` varchar(32) NOT NULL COMMENT '사용자아이디',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  `create_date` datetime NOT NULL COMMENT '생성일',
  `create_id` varchar(32) NOT NULL COMMENT '생성자',
  PRIMARY KEY (`cart_id`),
  KEY `FK_cart_user_id_user_user_id` (`user_id`),
  CONSTRAINT `FK_cart_user_id_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='장바구니';

-- Dumping data for table jalley_auction.cart: ~0 rows (approximately)
DELETE FROM `cart`;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.cart_item
DROP TABLE IF EXISTS `cart_item`;
CREATE TABLE IF NOT EXISTS `cart_item` (
  `cart_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '장바구니아이템아이디',
  `cart_id` bigint(20) DEFAULT NULL COMMENT '장바구니아이디',
  `product_id` bigint(20) DEFAULT NULL COMMENT '상품아이디',
  `use_yn` varchar(45) DEFAULT NULL COMMENT '사용유무',
  `create_date` datetime DEFAULT NULL COMMENT '생성일',
  `create_id` varchar(32) DEFAULT NULL COMMENT '생성자',
  PRIMARY KEY (`cart_item_id`),
  KEY `FK_cate_item_cart_id_cart_cart_id` (`cart_id`),
  CONSTRAINT `FK_cate_item_cart_id_cart_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='장바구니아이템';

-- Dumping data for table jalley_auction.cart_item: ~0 rows (approximately)
DELETE FROM `cart_item`;
/*!40000 ALTER TABLE `cart_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_item` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.category
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `cate_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '카테고리아이디',
  `store_id` varchar(32) DEFAULT NULL COMMENT '상점아이디',
  `p_cate_id` bigint(20) DEFAULT NULL COMMENT '카테고리부모코드',
  `cate_code` varchar(10) NOT NULL COMMENT '카테고리코드',
  `cate_name` varchar(100) NOT NULL COMMENT '카테고리명',
  `cate_level` int(11) NOT NULL COMMENT '카테고리레벨',
  `sort` int(11) NOT NULL COMMENT '정렬',
  `create_id` varchar(32) NOT NULL COMMENT '생성자',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '생성일',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  PRIMARY KEY (`cate_id`),
  UNIQUE KEY `cate_code_UNIQUE` (`cate_code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COMMENT='카테고리';

-- Dumping data for table jalley_auction.category: ~2 rows (approximately)
DELETE FROM `category`;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`cate_id`, `store_id`, `p_cate_id`, `cate_code`, `cate_name`, `cate_level`, `sort`, `create_id`, `create_date`, `use_yn`) VALUES
	(1, 'admin', 0, 'signature', '시그니쳐', 0, 1, 'admin', '2022-01-01 00:00:00', 'Y'),
	(9, 'admin', 0, 'general', '일반 상품', 0, 2, 'admin', '2022-01-01 00:00:00', 'Y');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.cate_product
DROP TABLE IF EXISTS `cate_product`;
CREATE TABLE IF NOT EXISTS `cate_product` (
  `cp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cate_id` bigint(20) NOT NULL COMMENT '카테고리아이디',
  `product_id` bigint(20) NOT NULL COMMENT '상품아이디',
  `use_yn` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`cp_id`),
  KEY `FK_cate_product_product_id_product_product_id` (`product_id`),
  KEY `FK_cate_product_cate_id_category_cate_id` (`cate_id`),
  CONSTRAINT `FK_cate_product_cate_id_category_cate_id` FOREIGN KEY (`cate_id`) REFERENCES `category` (`cate_id`),
  CONSTRAINT `FK_cate_product_product_id_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table jalley_auction.cate_product: ~0 rows (approximately)
DELETE FROM `cate_product`;
/*!40000 ALTER TABLE `cate_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `cate_product` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.c_code
DROP TABLE IF EXISTS `c_code`;
CREATE TABLE IF NOT EXISTS `c_code` (
  `cc_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '공통코드순번',
  `lang_cd` varchar(3) DEFAULT NULL COMMENT '언어코드',
  `c_code` varchar(45) DEFAULT 'en' COMMENT '공통코드',
  `c_code_name` varchar(100) DEFAULT NULL COMMENT '공통코드명',
  `c_code_value` varchar(45) DEFAULT NULL COMMENT '공통코드값',
  `sort` int(11) DEFAULT NULL COMMENT '정렬',
  `use_yn` varchar(1) DEFAULT 'Y' COMMENT '사용유무',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '생성일',
  `create_id` varchar(32) DEFAULT NULL COMMENT '생성자',
  PRIMARY KEY (`cc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=299 DEFAULT CHARSET=utf8mb3 COMMENT='공통코드';

-- Dumping data for table jalley_auction.c_code: ~249 rows (approximately)
DELETE FROM `c_code`;
/*!40000 ALTER TABLE `c_code` DISABLE KEYS */;
INSERT INTO `c_code` (`cc_id`, `lang_cd`, `c_code`, `c_code_name`, `c_code_value`, `sort`, `use_yn`, `create_date`, `create_id`) VALUES
	(2, 'en', 'user_status', 'Active', '1', 0, 'Y', '0000-00-00 00:00:00', 'system'),
	(5, 'en', 'role_code', 'Admin', '1', 0, 'Y', '0000-00-00 00:00:00', 'system'),
	(7, 'en', 'user_type', 'Admin', '1', 0, 'Y', '2022-05-05 12:44:32', 'system'),
	(8, 'en', 'user_type', 'Main Company', '2', 1, 'Y', '2022-05-05 12:44:32', 'system'),
	(9, 'en', 'user_type', 'Other Company', '3', 2, 'Y', '2022-05-05 12:44:32', 'system'),
	(10, 'en', 'user_type', 'Company User', '4', 3, 'Y', '2022-05-05 12:44:32', 'system'),
	(11, 'en', 'user_type', 'General Company', '5', 4, 'Y', '2022-05-05 12:44:32', 'system'),
	(12, 'en', 'user_status', 'Blocked', '2', 1, 'Y', '2022-05-05 17:17:28', 'system'),
	(13, 'en', 'user_keyword', 'User ID', 'user_id', 0, 'Y', '2022-05-05 17:50:39', 'system'),
	(14, 'en', 'user_keyword', 'Phone Number', 'tel', 1, 'Y', '2022-05-05 17:50:39', 'system'),
	(15, 'en', 'user_keyword', 'Company Name', 'comp_name', 2, 'Y', '2022-05-05 17:50:39', 'system'),
	(16, 'en', 'user_keyword', 'Company Mgt Name', 'comp_mgt_name', 3, 'Y', '2022-05-05 17:50:39', 'system'),
	(17, 'en', 'user_keyword', 'Company Mgt Tel', 'comp_mgt_tel', 4, 'Y', '2022-05-05 17:50:39', 'system'),
	(18, 'en', 'user_keyword', 'Company Tel', 'comp_tel', 5, 'Y', '2022-05-05 18:05:15', 'system'),
	(19, 'en', 'category_keyword', 'Store ID', 'store_id', 0, 'Y', '2022-05-06 10:36:02', 'system'),
	(20, 'en', 'category_keyword', 'Category Code', 'cate_code', 1, 'Y', '2022-05-06 10:36:02', 'system'),
	(21, 'en', 'category_keyword', 'Category Name', 'cate_name', 2, 'Y', '2022-05-06 10:36:02', 'system'),
	(22, 'en', 'category_keyword', 'Create Id', 'create_id', 3, 'Y', '2022-05-06 10:36:02', 'system'),
	(23, 'en', 'user_blacklist_level', 'Good', '1', 0, 'Y', '2022-05-07 17:07:37', 'system'),
	(24, 'en', 'user_blacklist_level', 'Nomal', '2', 1, 'Y', '2022-05-07 17:07:37', 'system'),
	(25, 'en', 'user_blacklist_level', 'Bad', '3', 2, 'Y', '2022-05-07 17:07:37', 'system'),
	(26, 'en', 'user_yn', 'Yes', 'Y', 0, 'Y', '2022-05-07 17:07:37', 'system'),
	(27, 'en', 'user_yn', 'No', 'N', 1, 'Y', '2022-05-07 17:07:37', 'system'),
	(28, 'en', 'product_type', 'Signature', 'signature', 0, 'Y', '2022-05-09 08:55:52', 'system'),
	(29, 'en', 'product_type', 'General', 'general', 1, 'Y', '2022-05-09 08:55:52', 'system'),
	(30, 'en', 'product_status', 'Owned', '1', 0, 'Y', '2022-05-09 08:55:52', 'system'),
	(31, 'en', 'product_status', 'Waiting for auction', '2', 1, 'Y', '2022-05-09 08:55:52', 'system'),
	(32, 'en', 'product_keyword', 'Product Name', 'product_name', 0, 'Y', '2022-05-09 09:15:58', 'system'),
	(33, 'en', 'product_keyword', 'Serial Num', 'serial_num', 1, 'Y', '2022-05-09 09:15:58', 'system'),
	(34, 'en', 'display_yn', 'Yes', 'Y', 0, 'Y', '2022-05-09 13:31:33', 'system'),
	(35, 'en', 'display_yn', 'No', 'N', 1, 'Y', '2022-05-09 13:31:33', 'system'),
	(36, 'en', 'request_status', 'Request', 'request', 0, 'Y', '2022-05-11 09:37:47', 'system'),
	(37, 'en', 'request_status', 'Approve', 'approve', 1, 'Y', '2022-05-11 09:37:47', 'system'),
	(38, 'en', 'request_status', 'Register', 'register', 3, 'Y', '2022-05-11 09:37:47', 'system'),
	(39, 'en', 'product_request_keyword', 'Product Name', 'product_name', 0, 'Y', '2022-05-11 09:39:29', 'system'),
	(40, 'en', 'product_request_keyword', 'Serial Num', 'serial_num', 1, 'Y', '2022-05-11 09:39:29', 'system'),
	(41, 'en', 'product_request_keyword', 'Creator', 'create_id', 2, 'Y', '2022-05-11 09:39:29', 'system'),
	(42, 'en', 'request_status', 'Reject', 'reject', 2, 'Y', '2022-05-11 09:53:21', 'system'),
	(43, 'en', 'auction_type', 'Signature Auction', 'signature_auction', 0, 'Y', '2022-05-13 04:52:13', 'system'),
	(44, 'en', 'auction_type', 'Rent Auction', 'rent_auction', 1, 'Y', '2022-05-13 04:52:13', 'system'),
	(45, 'en', 'auction_status', 'Comming Up', 'comming_up', 0, 'Y', '2022-05-13 04:56:05', 'system'),
	(46, 'en', 'auction_status', 'Auction Now', 'auction_now', 1, 'Y', '2022-05-13 04:56:05', 'system'),
	(47, 'en', 'auction_status', 'Finished', 'finished', 2, 'Y', '2022-05-13 04:56:05', 'system'),
	(48, 'en', 'auction_main_yn', 'Yes', 'Y', 1, 'Y', '2022-05-13 05:03:13', 'system'),
	(49, 'en', 'auction_main_yn', 'No', 'N', 0, 'Y', '2022-05-13 05:03:13', 'system'),
	(50, 'en', 'auction_keyword', 'Auction ID', 'auction_id', 0, 'Y', '2022-05-13 15:00:00', 'system'),
	(51, 'en', 'auction_keyword', 'Auction Title', 'auction_title', 1, 'Y', '2022-05-13 15:00:00', 'system'),
	(52, 'en', 'auction_keyword', 'Create User', 'create_id', 2, 'Y', '2022-05-13 15:00:00', 'system'),
	(53, 'en', 'product_status', 'Processing for auction', '3', 2, 'Y', '2022-05-16 16:54:02', 'system'),
	(54, 'en', 'product_status', 'Đặt thuê / Book a rental / 렌탈 예약', '4', 3, 'Y', '2022-05-16 16:54:02', 'system'),
	(55, 'en', 'product_status', 'Đang sửa / Fixing / 고정', '5', 4, 'Y', '2022-05-16 16:54:02', 'system'),
	(56, 'en', 'product_status', 'Đang thuê / Hiring / 고용', '6', 5, 'Y', '2022-05-16 16:54:02', 'system'),
	(57, 'en', 'product_status', 'Đặt bán / Order to sell / 판매 주문', '7', 6, 'Y', '2022-05-16 16:54:02', 'system'),
	(58, 'en', 'product_status', 'Hoàn thành bán / Complete sale / 완전 판매', '8', 7, 'Y', '2022-05-16 16:54:02', 'system'),
	(59, 'en', 'auction_status', 'Pause', 'pause', 3, 'Y', '2022-05-18 06:17:26', 'system'),
	(60, 'en', 'rent_pay_status', 'Unpaid', '1', 0, 'Y', '2022-05-18 16:29:51', 'system'),
	(61, 'en', 'rent_pay_status', 'Paid', '2', 1, 'Y', '2022-05-18 16:29:51', 'system'),
	(62, 'en', 'rent_status', 'Rent Registration', '1', 0, 'Y', '2022-05-20 08:56:58', 'system'),
	(63, 'en', 'rent_status', 'Renting', '2', 1, 'Y', '2022-05-20 08:56:58', 'system'),
	(64, 'en', 'rent_status', 'Out Of Date', '3', 2, 'Y', '2022-05-20 08:56:58', 'system'),
	(65, 'en', 'rent_pay_status', 'Partial Payment', '3', 2, 'Y', '2022-05-20 08:56:58', 'system'),
	(66, 'en', 'rent_item_pay_status', 'Paid', '1', 0, 'Y', '2022-05-20 15:53:12', 'system'),
	(67, 'en', 'rent_item_pay_status', 'Unpaid', '2', 1, 'Y', '2022-05-20 15:53:12', 'system'),
	(68, 'en', 'rent_keyword', 'Rent Owner', 'user_id', 0, 'y', '2022-05-21 03:47:42', 'system'),
	(69, 'en', 'rent_keyword', 'Store Name', 'store_name', 1, 'y', '2022-05-21 03:47:42', 'system'),
	(70, 'en', 'rent_keyword', 'Tel Number', 'tel', 2, 'y', '2022-05-21 03:47:42', 'system'),
	(71, 'en', 'rent_status', 'Returned', '4', 3, 'y', '2022-05-21 04:21:50', 'system'),
	(73, 'en', 'rent_status', 'Extend', '5', 4, 'Y', '2022-05-26 15:19:19', 'system'),
	(74, 'en', 'order_type', 'Online', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(75, 'en', 'order_type', 'Offline', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(76, 'en', 'order_kind', 'Auction', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(77, 'en', 'order_kind', 'Rent Auction', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(78, 'en', 'order_kind', 'Rent', '3', 2, 'Y', '2022-05-28 21:18:12', 'system'),
	(79, 'en', 'order_kind', 'Sale', '4', 3, 'Y', '2022-05-28 21:18:12', 'system'),
	(80, 'en', 'order_kind', 'Fee', '5', 4, 'Y', '2022-05-28 21:18:12', 'system'),
	(81, 'en', 'pay_status', 'Pending', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(82, 'en', 'pay_status', 'Refund', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(83, 'en', 'pay_status', 'Cancel', '3', 2, 'Y', '2022-05-28 21:18:12', 'system'),
	(84, 'en', 'pay_status', 'Partial payment', '4', 3, 'Y', '2022-05-28 21:18:12', 'system'),
	(85, 'en', 'pay_status', 'Finished', '5', 4, 'y', '2022-05-30 11:33:59', 'system'),
	(86, 'en', 'order_keyword', 'User ID', 'user_id', 0, 'y', '2022-05-30 14:05:14', 'system'),
	(87, 'en', 'order_keyword', 'Create ID', 'create_id', 1, 'y', '2022-05-30 14:05:14', 'system'),
	(88, 'en', 'fee_rent_ofd', 'Fee Rent Out Of Date', '1000', 0, 'y', '2022-06-02 15:15:37', 'system'),
	(89, 'ko', 'user_status', '활성', '1', 0, 'Y', '0000-00-00 00:00:00', 'system'),
	(90, 'ko', 'role_code', '관리자', '1', 0, 'Y', '0000-00-00 00:00:00', 'system'),
	(91, 'ko', 'user_type', '관리자', '1', 0, 'Y', '2022-05-05 12:44:32', 'system'),
	(92, 'ko', 'user_type', '본사', '2', 1, 'Y', '2022-05-05 12:44:32', 'system'),
	(93, 'ko', 'user_type', '대리점', '3', 2, 'Y', '2022-05-05 12:44:32', 'system'),
	(94, 'ko', 'user_type', 'Company User', '4', 3, 'Y', '2022-05-05 12:44:32', 'system'),
	(95, 'ko', 'user_type', 'General Company', '5', 4, 'Y', '2022-05-05 12:44:32', 'system'),
	(96, 'ko', 'user_status', '차단', '2', 1, 'Y', '2022-05-05 17:17:28', 'system'),
	(97, 'ko', 'user_keyword', '사용자아이디', 'user_id', 0, 'Y', '2022-05-05 17:50:39', 'system'),
	(98, 'ko', 'user_keyword', '연락처', 'tel', 1, 'Y', '2022-05-05 17:50:39', 'system'),
	(99, 'ko', 'user_keyword', '회사명', 'comp_name', 2, 'Y', '2022-05-05 17:50:39', 'system'),
	(100, 'ko', 'user_keyword', '회사담당자명', 'comp_mgt_name', 3, 'Y', '2022-05-05 17:50:39', 'system'),
	(101, 'ko', 'user_keyword', '회사담당자연락처', 'comp_mgt_tel', 4, 'Y', '2022-05-05 17:50:39', 'system'),
	(102, 'ko', 'user_keyword', '회사연락처', 'comp_tel', 5, 'Y', '2022-05-05 18:05:15', 'system'),
	(103, 'ko', 'category_keyword', '상점아이디', 'store_id', 0, 'Y', '2022-05-06 10:36:02', 'system'),
	(104, 'ko', 'category_keyword', '카테고리코드', 'cate_code', 1, 'Y', '2022-05-06 10:36:02', 'system'),
	(105, 'ko', 'category_keyword', '카테고리명', 'cate_name', 2, 'Y', '2022-05-06 10:36:02', 'system'),
	(106, 'ko', 'category_keyword', '생성자', 'create_id', 3, 'Y', '2022-05-06 10:36:02', 'system'),
	(107, 'ko', 'user_blacklist_level', '좋음', '1', 0, 'Y', '2022-05-07 17:07:37', 'system'),
	(108, 'ko', 'user_blacklist_level', '일반', '2', 1, 'Y', '2022-05-07 17:07:37', 'system'),
	(109, 'ko', 'user_blacklist_level', '나쁨', '3', 2, 'Y', '2022-05-07 17:07:37', 'system'),
	(110, 'ko', 'user_yn', '유', 'Y', 0, 'Y', '2022-05-07 17:07:37', 'system'),
	(111, 'ko', 'user_yn', '무', 'N', 1, 'Y', '2022-05-07 17:07:37', 'system'),
	(112, 'ko', 'product_type', '시그니처', 'signature', 0, 'Y', '2022-05-09 08:55:52', 'system'),
	(113, 'ko', 'product_type', '일반', 'general', 1, 'Y', '2022-05-09 08:55:52', 'system'),
	(114, 'ko', 'product_status', '보유', '1', 0, 'Y', '2022-05-09 08:55:52', 'system'),
	(115, 'ko', 'product_status', '경매대기', '2', 1, 'Y', '2022-05-09 08:55:52', 'system'),
	(116, 'ko', 'product_keyword', '상품명', 'product_name', 0, 'Y', '2022-05-09 09:15:58', 'system'),
	(117, 'ko', 'product_keyword', '시리얼번호', 'serial_num', 1, 'Y', '2022-05-09 09:15:58', 'system'),
	(118, 'ko', 'display_yn', '네', 'Y', 0, 'Y', '2022-05-09 13:31:33', 'system'),
	(119, 'ko', 'display_yn', '아니오', 'N', 1, 'Y', '2022-05-09 13:31:33', 'system'),
	(120, 'ko', 'request_status', '요청', 'request', 0, 'Y', '2022-05-11 09:37:47', 'system'),
	(121, 'ko', 'request_status', '승인', 'approve', 1, 'Y', '2022-05-11 09:37:47', 'system'),
	(122, 'ko', 'request_status', '등록', 'register', 3, 'Y', '2022-05-11 09:37:47', 'system'),
	(123, 'ko', 'product_request_keyword', '상품명', 'product_name', 0, 'Y', '2022-05-11 09:39:29', 'system'),
	(124, 'ko', 'product_request_keyword', '시리얼번호', 'serial_num', 1, 'Y', '2022-05-11 09:39:29', 'system'),
	(125, 'ko', 'product_request_keyword', '생성자', 'create_id', 2, 'Y', '2022-05-11 09:39:29', 'system'),
	(126, 'ko', 'request_status', '거절', 'reject', 2, 'Y', '2022-05-11 09:53:21', 'system'),
	(127, 'ko', 'auction_type', '시그니처 경매', 'signature_auction', 0, 'Y', '2022-05-13 04:52:13', 'system'),
	(128, 'ko', 'auction_type', '대여 경매', 'rent_auction', 1, 'Y', '2022-05-13 04:52:13', 'system'),
	(129, 'ko', 'auction_status', '경매대기', 'comming_up', 0, 'Y', '2022-05-13 04:56:05', 'system'),
	(130, 'ko', 'auction_status', '경매시작', 'auction_now', 1, 'Y', '2022-05-13 04:56:05', 'system'),
	(131, 'ko', 'auction_status', '경매종료', 'finished', 2, 'Y', '2022-05-13 04:56:05', 'system'),
	(132, 'ko', 'auction_main_yn', '네', 'Y', 1, 'Y', '2022-05-13 05:03:13', 'system'),
	(133, 'ko', 'auction_main_yn', '아니오', 'N', 0, 'Y', '2022-05-13 05:03:13', 'system'),
	(134, 'ko', 'auction_keyword', '경매번호', 'auction_id', 0, 'Y', '2022-05-13 15:00:00', 'system'),
	(135, 'ko', 'auction_keyword', '경매제목', 'auction_title', 1, 'Y', '2022-05-13 15:00:00', 'system'),
	(136, 'ko', 'auction_keyword', '생성자', 'create_id', 2, 'Y', '2022-05-13 15:00:00', 'system'),
	(137, 'ko', 'product_status', '경매중', '3', 2, 'Y', '2022-05-16 16:54:02', 'system'),
	(138, 'ko', 'product_status', '대여예약', '4', 3, 'Y', '2022-05-16 16:54:02', 'system'),
	(139, 'ko', 'product_status', '수선중', '5', 4, 'Y', '2022-05-16 16:54:02', 'system'),
	(140, 'ko', 'product_status', '대여', '6', 5, 'Y', '2022-05-16 16:54:02', 'system'),
	(141, 'ko', 'product_status', '판매예약', '7', 6, 'Y', '2022-05-16 16:54:02', 'system'),
	(142, 'ko', 'product_status', '판매', '8', 7, 'Y', '2022-05-16 16:54:02', 'system'),
	(143, 'ko', 'auction_status', '경매중지', 'pause', 3, 'Y', '2022-05-18 06:17:26', 'system'),
	(144, 'ko', 'rent_pay_status', '미결제', '1', 0, 'Y', '2022-05-18 16:29:51', 'system'),
	(145, 'ko', 'rent_pay_status', '결제완료', '2', 1, 'Y', '2022-05-18 16:29:51', 'system'),
	(146, 'ko', 'rent_status', '대여예약', '1', 0, 'Y', '2022-05-20 08:56:58', 'system'),
	(147, 'ko', 'rent_status', '대여중', '2', 1, 'Y', '2022-05-20 08:56:58', 'system'),
	(148, 'ko', 'rent_status', '연체', '3', 2, 'Y', '2022-05-20 08:56:58', 'system'),
	(149, 'ko', 'rent_pay_status', '부분결제', '3', 2, 'Y', '2022-05-20 08:56:58', 'system'),
	(150, 'ko', 'rent_item_pay_status', '결제', '1', 0, 'Y', '2022-05-20 15:53:12', 'system'),
	(151, 'ko', 'rent_item_pay_status', '미결제', '2', 1, 'Y', '2022-05-20 15:53:12', 'system'),
	(152, 'ko', 'rent_keyword', '대여자아이디', 'user_id', 0, 'y', '2022-05-21 03:47:42', 'system'),
	(153, 'ko', 'rent_keyword', '상점명', 'store_name', 1, 'y', '2022-05-21 03:47:42', 'system'),
	(154, 'ko', 'rent_keyword', '연락처', 'tel', 2, 'y', '2022-05-21 03:47:42', 'system'),
	(155, 'ko', 'rent_status', '반납', '4', 3, 'y', '2022-05-21 04:21:50', 'system'),
	(156, 'ko', 'rent_status', '연장', '5', 4, 'Y', '2022-05-26 15:19:19', 'system'),
	(157, 'ko', 'order_type', '온라인', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(158, 'ko', 'order_type', '오프라인', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(159, 'ko', 'order_kind', '경매', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(160, 'ko', 'order_kind', '대여경매', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(161, 'ko', 'order_kind', '대여', '3', 2, 'Y', '2022-05-28 21:18:12', 'system'),
	(162, 'ko', 'order_kind', '판매', '4', 3, 'Y', '2022-05-28 21:18:12', 'system'),
	(163, 'ko', 'order_kind', '연체료', '5', 4, 'Y', '2022-05-28 21:18:12', 'system'),
	(164, 'ko', 'pay_status', '대기', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(165, 'ko', 'pay_status', '환불', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(166, 'ko', 'pay_status', '취소', '3', 2, 'Y', '2022-05-28 21:18:12', 'system'),
	(167, 'ko', 'pay_status', '부분결제', '4', 3, 'Y', '2022-05-28 21:18:12', 'system'),
	(168, 'ko', 'pay_status', '결제완료', '5', 4, 'y', '2022-05-30 11:33:59', 'system'),
	(169, 'ko', 'order_keyword', '사용자아이디', 'user_id', 0, 'y', '2022-05-30 14:05:14', 'system'),
	(170, 'ko', 'order_keyword', '생성자', 'create_id', 1, 'y', '2022-05-30 14:05:14', 'system'),
	(171, 'ko', 'fee_rent_ofd', '연체료', '1000', 0, 'y', '2022-06-02 15:15:37', 'system'),
	(216, 'vt', 'user_status', 'Đang hoạt động', '1', 0, 'Y', '0000-00-00 00:00:00', 'system'),
	(217, 'vt', 'role_code', 'Quản trị', '1', 0, 'Y', '0000-00-00 00:00:00', 'system'),
	(218, 'vt', 'user_type', 'Quản trị', '1', 0, 'Y', '2022-05-05 12:44:32', 'system'),
	(219, 'vt', 'user_type', 'Tổng công ty', '2', 1, 'Y', '2022-05-05 12:44:32', 'system'),
	(220, 'vt', 'user_type', 'Công ty khác', '3', 2, 'Y', '2022-05-05 12:44:32', 'system'),
	(221, 'vt', 'user_type', 'Company User', '4', 3, 'Y', '2022-05-05 12:44:32', 'system'),
	(222, 'vt', 'user_type', 'General Company', '5', 4, 'Y', '2022-05-05 12:44:32', 'system'),
	(223, 'vt', 'user_status', 'Đang khóa', '2', 1, 'Y', '2022-05-05 17:17:28', 'system'),
	(224, 'vt', 'user_keyword', 'Tên đăng nhập', 'user_id', 0, 'Y', '2022-05-05 17:50:39', 'system'),
	(225, 'vt', 'user_keyword', 'Số điện thoại', 'tel', 1, 'Y', '2022-05-05 17:50:39', 'system'),
	(226, 'vt', 'user_keyword', 'Tên công ty', 'comp_name', 2, 'Y', '2022-05-05 17:50:39', 'system'),
	(227, 'vt', 'user_keyword', 'Tên công ty quản lý', 'comp_mgt_name', 3, 'Y', '2022-05-05 17:50:39', 'system'),
	(228, 'vt', 'user_keyword', 'Điện thoại công ty quản lý', 'comp_mgt_tel', 4, 'Y', '2022-05-05 17:50:39', 'system'),
	(229, 'vt', 'user_keyword', 'Điện thoại công ty', 'comp_tel', 5, 'Y', '2022-05-05 18:05:15', 'system'),
	(230, 'vt', 'category_keyword', 'Tên cửa hàng', 'store_id', 0, 'Y', '2022-05-06 10:36:02', 'system'),
	(231, 'vt', 'category_keyword', 'Mã danh mục', 'cate_code', 1, 'Y', '2022-05-06 10:36:02', 'system'),
	(232, 'vt', 'category_keyword', 'Tên danh mục', 'cate_name', 2, 'Y', '2022-05-06 10:36:02', 'system'),
	(233, 'vt', 'category_keyword', 'Người tạo', 'create_id', 3, 'Y', '2022-05-06 10:36:02', 'system'),
	(234, 'vt', 'user_blacklist_level', 'Tốt', '1', 0, 'Y', '2022-05-07 17:07:37', 'system'),
	(235, 'vt', 'user_blacklist_level', 'Bình thường', '2', 1, 'Y', '2022-05-07 17:07:37', 'system'),
	(236, 'vt', 'user_blacklist_level', 'Không tốt', '3', 2, 'Y', '2022-05-07 17:07:37', 'system'),
	(237, 'vt', 'user_yn', 'Có', 'Y', 0, 'Y', '2022-05-07 17:07:37', 'system'),
	(238, 'vt', 'user_yn', 'Không', 'N', 1, 'Y', '2022-05-07 17:07:37', 'system'),
	(239, 'vt', 'product_type', 'Hàng cao cấp', 'signature', 0, 'Y', '2022-05-09 08:55:52', 'system'),
	(240, 'vt', 'product_type', 'Hàng bình thường', 'general', 1, 'Y', '2022-05-09 08:55:52', 'system'),
	(241, 'vt', 'product_status', 'Sở hữu', '1', 0, 'Y', '2022-05-09 08:55:52', 'system'),
	(242, 'vt', 'product_status', 'Chờ đầu giá', '2', 1, 'Y', '2022-05-09 08:55:52', 'system'),
	(243, 'vt', 'product_keyword', 'Tên sản phẩm', 'product_name', 0, 'Y', '2022-05-09 09:15:58', 'system'),
	(244, 'vt', 'product_keyword', 'Số serial', 'serial_num', 1, 'Y', '2022-05-09 09:15:58', 'system'),
	(245, 'vt', 'display_yn', 'Có', 'Y', 0, 'Y', '2022-05-09 13:31:33', 'system'),
	(246, 'vt', 'display_yn', 'Không', 'N', 1, 'Y', '2022-05-09 13:31:33', 'system'),
	(247, 'vt', 'request_status', 'Gửi yêu cầu', 'request', 0, 'Y', '2022-05-11 09:37:47', 'system'),
	(248, 'vt', 'request_status', 'Chấp thuật', 'approve', 1, 'Y', '2022-05-11 09:37:47', 'system'),
	(249, 'vt', 'request_status', 'Đăng ký', 'register', 3, 'Y', '2022-05-11 09:37:47', 'system'),
	(250, 'vt', 'product_request_keyword', 'Tên sản phẩm', 'product_name', 0, 'Y', '2022-05-11 09:39:29', 'system'),
	(251, 'vt', 'product_request_keyword', 'Số serial', 'serial_num', 1, 'Y', '2022-05-11 09:39:29', 'system'),
	(252, 'vt', 'product_request_keyword', 'Người tạo', 'create_id', 2, 'Y', '2022-05-11 09:39:29', 'system'),
	(253, 'vt', 'request_status', 'Từ chối', 'reject', 2, 'Y', '2022-05-11 09:53:21', 'system'),
	(254, 'vt', 'auction_type', 'Đấu giá sản phẩm cao cấp', 'signature_auction', 0, 'Y', '2022-05-13 04:52:13', 'system'),
	(255, 'vt', 'auction_type', 'Đấu giá cho thuê', 'rent_auction', 1, 'Y', '2022-05-13 04:52:13', 'system'),
	(256, 'vt', 'auction_status', 'Chuẩn bị đấu giá', 'comming_up', 0, 'Y', '2022-05-13 04:56:05', 'system'),
	(257, 'vt', 'auction_status', 'Đang đấu giá', 'auction_now', 1, 'Y', '2022-05-13 04:56:05', 'system'),
	(258, 'vt', 'auction_status', 'Kết thúc', 'finished', 2, 'Y', '2022-05-13 04:56:05', 'system'),
	(259, 'vt', 'auction_main_yn', 'Có', 'Y', 1, 'Y', '2022-05-13 05:03:13', 'system'),
	(260, 'vt', 'auction_main_yn', 'Không', 'N', 0, 'Y', '2022-05-13 05:03:13', 'system'),
	(261, 'vt', 'auction_keyword', 'Mã đấu giá', 'auction_id', 0, 'Y', '2022-05-13 15:00:00', 'system'),
	(262, 'vt', 'auction_keyword', 'Tiêu đề đấu giá', 'auction_title', 1, 'Y', '2022-05-13 15:00:00', 'system'),
	(263, 'vt', 'auction_keyword', 'Người tạo', 'create_id', 2, 'Y', '2022-05-13 15:00:00', 'system'),
	(264, 'vt', 'product_status', 'Đang xử lý đấu giá', '3', 2, 'Y', '2022-05-16 16:54:02', 'system'),
	(265, 'vt', 'product_status', 'Đặt thuê', '4', 3, 'Y', '2022-05-16 16:54:02', 'system'),
	(266, 'vt', 'product_status', 'Đang sửa', '5', 4, 'Y', '2022-05-16 16:54:02', 'system'),
	(267, 'vt', 'product_status', 'Đang thuê', '6', 5, 'Y', '2022-05-16 16:54:02', 'system'),
	(268, 'vt', 'product_status', 'Đặt bán', '7', 6, 'Y', '2022-05-16 16:54:02', 'system'),
	(269, 'vt', 'product_status', 'Hoàn thành bán', '8', 7, 'Y', '2022-05-16 16:54:02', 'system'),
	(270, 'vt', 'auction_status', 'Tạm dừng', 'pause', 3, 'Y', '2022-05-18 06:17:26', 'system'),
	(271, 'vt', 'rent_pay_status', 'Chưa thanh toán', '1', 0, 'Y', '2022-05-18 16:29:51', 'system'),
	(272, 'vt', 'rent_pay_status', 'Đã thanh toán', '2', 1, 'Y', '2022-05-18 16:29:51', 'system'),
	(273, 'vt', 'rent_status', 'Đăng ký thuê', '1', 0, 'Y', '2022-05-20 08:56:58', 'system'),
	(274, 'vt', 'rent_status', 'Đang thuê', '2', 1, 'Y', '2022-05-20 08:56:58', 'system'),
	(275, 'vt', 'rent_status', 'Quá hạn', '3', 2, 'Y', '2022-05-20 08:56:58', 'system'),
	(276, 'vt', 'rent_pay_status', 'Thanh toán một phần', '3', 2, 'Y', '2022-05-20 08:56:58', 'system'),
	(277, 'vt', 'rent_item_pay_status', 'Thanh toán', '1', 0, 'Y', '2022-05-20 15:53:12', 'system'),
	(278, 'vt', 'rent_item_pay_status', 'Chưa thanh toán', '2', 1, 'Y', '2022-05-20 15:53:12', 'system'),
	(279, 'vt', 'rent_keyword', 'Người thuê', 'user_id', 0, 'y', '2022-05-21 03:47:42', 'system'),
	(280, 'vt', 'rent_keyword', 'Tên cửa hàng', 'store_name', 1, 'y', '2022-05-21 03:47:42', 'system'),
	(281, 'vt', 'rent_keyword', 'Số điện thoại', 'tel', 2, 'y', '2022-05-21 03:47:42', 'system'),
	(282, 'vt', 'rent_status', 'Đã trả', '4', 3, 'y', '2022-05-21 04:21:50', 'system'),
	(283, 'vt', 'rent_status', 'Gia hạn', '5', 4, 'Y', '2022-05-26 15:19:19', 'system'),
	(284, 'vt', 'order_type', 'Trực tuyến', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(285, 'vt', 'order_type', 'Tại cửa hàng', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(286, 'vt', 'order_kind', 'Đấu giá', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(287, 'vt', 'order_kind', 'Đấu giá cho thuê', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(288, 'vt', 'order_kind', 'Cho thuê', '3', 2, 'Y', '2022-05-28 21:18:12', 'system'),
	(289, 'vt', 'order_kind', 'Bán', '4', 3, 'Y', '2022-05-28 21:18:12', 'system'),
	(290, 'vt', 'order_kind', 'Phí', '5', 4, 'Y', '2022-05-28 21:18:12', 'system'),
	(291, 'vt', 'pay_status', 'Chưa thanh toán', '1', 0, 'Y', '2022-05-28 21:18:12', 'system'),
	(292, 'vt', 'pay_status', 'Hoàn tiền', '2', 1, 'Y', '2022-05-28 21:18:12', 'system'),
	(293, 'vt', 'pay_status', 'Hủy', '3', 2, 'Y', '2022-05-28 21:18:12', 'system'),
	(294, 'vt', 'pay_status', 'Thanh toán một phần', '4', 3, 'Y', '2022-05-28 21:18:12', 'system'),
	(295, 'vt', 'pay_status', 'Đã thanh toán', '5', 4, 'y', '2022-05-30 11:33:59', 'system'),
	(296, 'vt', 'order_keyword', 'Tên người dùng', 'user_id', 0, 'y', '2022-05-30 14:05:14', 'system'),
	(297, 'vt', 'order_keyword', 'Người tạo', 'create_id', 1, 'y', '2022-05-30 14:05:14', 'system'),
	(298, 'vt', 'fee_rent_ofd', 'Phí thuê quá hạn', '1000', 0, 'y', '2022-06-02 15:15:37', 'system');
/*!40000 ALTER TABLE `c_code` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.delivery
DROP TABLE IF EXISTS `delivery`;
CREATE TABLE IF NOT EXISTS `delivery` (
  `delivery_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '배송지아이디',
  `user_id` varchar(32) DEFAULT NULL COMMENT '사용자아이디',
  `delivery_name` varchar(45) DEFAULT NULL COMMENT '배송지명',
  `delivery_tel` varchar(20) DEFAULT NULL COMMENT '배송지연락처',
  `delivery_addr` varchar(200) DEFAULT NULL COMMENT '배송지주소',
  `delivery_addr_detail` varchar(200) DEFAULT NULL COMMENT '배송지상세주소',
  `delivery_zipcode` varchar(10) DEFAULT NULL COMMENT '배송지우편번호',
  `use_yn` varchar(1) DEFAULT NULL COMMENT '사용유무',
  `create_date` datetime DEFAULT NULL COMMENT '생성일',
  `create_id` varchar(32) DEFAULT NULL COMMENT '생성자',
  PRIMARY KEY (`delivery_id`),
  KEY `FK_delivery_user_id_user_user_id` (`user_id`),
  CONSTRAINT `FK_delivery_user_id_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='배송지정보';

-- Dumping data for table jalley_auction.delivery: ~0 rows (approximately)
DELETE FROM `delivery`;
/*!40000 ALTER TABLE `delivery` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.deposit_withdraw_log
DROP TABLE IF EXISTS `deposit_withdraw_log`;
CREATE TABLE IF NOT EXISTS `deposit_withdraw_log` (
  `dw_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '입출금아이디',
  `work_type` varchar(45) DEFAULT NULL COMMENT '작업구분',
  `user_id` varchar(32) DEFAULT NULL COMMENT '사용자아이디',
  `price` bigint(20) DEFAULT NULL COMMENT '가격',
  `pay_status` varchar(45) DEFAULT NULL COMMENT '결제상태',
  `use_yn` varchar(1) DEFAULT NULL COMMENT '사용유무',
  `create_date` datetime DEFAULT NULL COMMENT '생성일',
  `create_id` varchar(32) DEFAULT NULL COMMENT '생성자',
  PRIMARY KEY (`dw_id`),
  KEY `FK_deposit_withdraw_log_user_id_user_user_id` (`user_id`),
  CONSTRAINT `FK_deposit_withdraw_log_user_id_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='입출금내역';

-- Dumping data for table jalley_auction.deposit_withdraw_log: ~0 rows (approximately)
DELETE FROM `deposit_withdraw_log`;
/*!40000 ALTER TABLE `deposit_withdraw_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `deposit_withdraw_log` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.fee_backup
DROP TABLE IF EXISTS `fee_backup`;
CREATE TABLE IF NOT EXISTS `fee_backup` (
  `fee_backup_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '연체료백업아이디',
  `work_type` varchar(45) NOT NULL COMMENT '작업구분',
  `fee_id` bigint(20) NOT NULL COMMENT '연체료아이디',
  `rent_id` bigint(20) NOT NULL COMMENT '대여아이디',
  `rent_item_id` bigint(20) NOT NULL COMMENT '대여상품아이디',
  `fee` bigint(20) NOT NULL COMMENT '연체료',
  `late_day` int(11) NOT NULL COMMENT '연체일',
  `pay_status` varchar(1) NOT NULL COMMENT '결제상태',
  `change_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '수정일',
  `change_id` varchar(32) NOT NULL COMMENT '수정자',
  PRIMARY KEY (`fee_backup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='연체료기록';

-- Dumping data for table jalley_auction.fee_backup: ~0 rows (approximately)
DELETE FROM `fee_backup`;
/*!40000 ALTER TABLE `fee_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_backup` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.fee_history
DROP TABLE IF EXISTS `fee_history`;
CREATE TABLE IF NOT EXISTS `fee_history` (
  `fee_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '연체료아이디',
  `rent_id` bigint(20) NOT NULL COMMENT '대여아이디',
  `rent_item_id` bigint(20) NOT NULL COMMENT '대여상품아이디',
  `fee` bigint(20) NOT NULL COMMENT '연체료',
  `late_day` int(11) NOT NULL COMMENT '연체일',
  `pay_status` varchar(1) NOT NULL COMMENT '결제상태',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '등록일',
  `create_id` varchar(32) NOT NULL COMMENT '등록자',
  PRIMARY KEY (`fee_id`),
  KEY `FK_fee_history_rent_idx` (`rent_id`),
  KEY `FK_fee_history_rent_item_idx` (`rent_item_id`),
  CONSTRAINT `FK_fee_history_rent` FOREIGN KEY (`rent_id`) REFERENCES `rent` (`rent_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_fee_history_rent_item` FOREIGN KEY (`rent_item_id`) REFERENCES `rent_items` (`rent_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='연체료기록';

-- Dumping data for table jalley_auction.fee_history: ~0 rows (approximately)
DELETE FROM `fee_history`;
/*!40000 ALTER TABLE `fee_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_history` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.file
DROP TABLE IF EXISTS `file`;
CREATE TABLE IF NOT EXISTS `file` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '파일아이디',
  `file_type` varchar(45) DEFAULT NULL COMMENT '파일타입',
  `file_size` bigint(20) DEFAULT NULL COMMENT '파일크기',
  `ext` varchar(5) DEFAULT NULL COMMENT '확장자',
  `file_name` varchar(100) DEFAULT NULL COMMENT '파일명',
  `file_hash_name` varchar(200) DEFAULT NULL COMMENT '파일해시명',
  `file_path` varchar(1000) DEFAULT NULL COMMENT '파일경로',
  `create_date` datetime DEFAULT current_timestamp() COMMENT '생성일',
  `create_id` varchar(32) DEFAULT NULL COMMENT '생성자',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8mb3 COMMENT='첨부파일';

-- Dumping data for table jalley_auction.file: ~203 rows (approximately)
DELETE FROM `file`;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` (`file_id`, `file_type`, `file_size`, `ext`, `file_name`, `file_hash_name`, `file_path`, `create_date`, `create_id`) VALUES
	(1, 'image/jpeg', 1018249, 'jpg', '/00000000001/product.jpg', '202202241516140.jpg', '2022/02/24/1/', '2022-02-24 15:16:14', 'jalley'),
	(2, 'image/jpeg', 1018249, 'jpg', '/00000000002/product.jpg', '202202241527370.jpg', '2022/02/24/2/', '2022-02-24 15:27:37', 'jalley'),
	(3, 'image/jpeg', 1118635, 'jpg', '/00000000003/product.jpg', '202202241529120.jpg', '2022/02/24/3/', '2022-02-24 15:29:12', 'jalley'),
	(4, 'image/jpeg', 1118635, 'jpg', '/00000000004/product.jpg', '202202241530170.jpg', '2022/02/24/4/', '2022-02-24 15:30:17', 'jalley'),
	(5, 'image/jpeg', 115517, 'jpg', '/00000000005/product.jpg', '202202241551520.jpg', '2022/02/24/5/', '2022-02-24 15:51:52', 'jalley'),
	(6, 'image/jpeg', 115517, 'jpg', '/00000000006/product.jpg', '202202241553020.jpg', '2022/02/24/6/', '2022-02-24 15:53:02', 'jalley'),
	(7, 'image/jpeg', 1018249, 'jpg', '/00000000007/product.jpg', '202202241554340.jpg', '2022/02/24/7/', '2022-02-24 15:54:34', 'jalley'),
	(8, 'image/jpeg', 1018249, 'jpg', '/00000000008/product.jpg', '202202241555500.jpg', '2022/02/24/8/', '2022-02-24 15:55:50', 'jalley'),
	(9, 'image/jpeg', 115517, 'jpg', '/00000000009/product.jpg', '202202241556390.jpg', '2022/02/24/9/', '2022-02-24 15:56:39', 'jalley'),
	(10, 'image/jpeg', 1018249, 'jpg', '/00000000010/product.jpg', '202202241557380.jpg', '2022/02/24/10/', '2022-02-24 15:57:38', 'jalley'),
	(11, 'image/jpeg', 1063607, 'jpg', '/00000000011/product.jpg', '202202241602240.jpg', '2022/02/24/11/', '2022-02-24 16:02:24', 'jalley'),
	(12, 'image/jpeg', 1063607, 'jpg', '/00000000012/product.jpg', '202202241604540.jpg', '2022/02/24/12/', '2022-02-24 16:04:54', 'jalley'),
	(13, 'image/jpeg', 1132527, 'jpg', '/00000000013/product.jpg', '202202241605540.jpg', '2022/02/24/13/', '2022-02-24 16:05:54', 'jalley'),
	(14, 'image/jpeg', 1132527, 'jpg', '/00000000014/product.jpg', '202202241607020.jpg', '2022/02/24/14/', '2022-02-24 16:07:02', 'jalley'),
	(15, 'image/jpeg', 1063607, 'jpg', '/00000000015/product.jpg', '202202241609430.jpg', '2022/02/24/15/', '2022-02-24 16:09:43', 'jalley'),
	(16, 'image/jpeg', 1996170, 'jpg', '/00000000016/product.jpg', '202202241631250.jpg', '2022/02/24/16/', '2022-02-24 16:31:25', 'jalley'),
	(17, 'image/jpeg', 1996170, 'jpg', '/00000000017/product.jpg', '202202241632130.jpg', '2022/02/24/17/', '2022-02-24 16:32:13', 'jalley'),
	(18, 'image/jpeg', 1996170, 'jpg', '/00000000018/product.jpg', '202202241632560.jpg', '2022/02/24/18/', '2022-02-24 16:32:56', 'jalley'),
	(19, 'image/jpeg', 1018249, 'jpg', '/00000000019/product.jpg', '202202241634030.jpg', '2022/02/24/19/', '2022-02-24 16:34:03', 'jalley'),
	(20, 'image/jpeg', 1018249, 'jpg', '/00000000020/product.jpg', '202202241634520.jpg', '2022/02/24/20/', '2022-02-24 16:34:52', 'jalley'),
	(21, 'image/jpeg', 1118635, 'jpg', '/00000000021/product.jpg', '202202241635400.jpg', '2022/02/24/21/', '2022-02-24 16:35:40', 'jalley'),
	(22, 'image/jpeg', 1118635, 'jpg', '/00000000022/product.jpg', '202202241636310.jpg', '2022/02/24/22/', '2022-02-24 16:36:31', 'jalley'),
	(23, 'image/jpeg', 115517, 'jpg', '/00000000023/product.jpg', '202202241637370.jpg', '2022/02/24/23/', '2022-02-24 16:37:37', 'jalley'),
	(24, 'image/jpeg', 115517, 'jpg', '/00000000024/product.jpg', '202202241638290.jpg', '2022/02/24/24/', '2022-02-24 16:38:29', 'jalley'),
	(25, 'image/jpeg', 1018249, 'jpg', '/00000000025/product.jpg', '202202241639270.jpg', '2022/02/24/25/', '2022-02-24 16:39:27', 'jalley'),
	(26, 'image/jpeg', 1018249, 'jpg', '/00000000026/product.jpg', '202202241639270.jpg', '2022/02/24/26/', '2022-02-24 16:39:27', 'jalley'),
	(27, 'image/jpeg', 1118635, 'jpg', '/00000000027/product.jpg', '202202241640080.jpg', '2022/02/24/27/', '2022-02-24 16:40:08', 'jalley'),
	(28, 'image/jpeg', 115517, 'jpg', '/00000000028/product.jpg', '202202241641030.jpg', '2022/02/24/28/', '2022-02-24 16:41:03', 'jalley'),
	(29, 'image/jpeg', 1018249, 'jpg', '/00000000029/product.jpg', '202202241641430.jpg', '2022/02/24/29/', '2022-02-24 16:41:43', 'jalley'),
	(30, 'image/jpeg', 2166390, 'jpg', '/00000000030/product.jpg', '202202241645510.jpg', '2022/02/24/30/', '2022-02-24 16:45:51', 'jalley'),
	(31, 'image/jpeg', 2166390, 'jpg', '/00000000031/product.jpg', '202202241648190.jpg', '2022/02/24/31/', '2022-02-24 16:48:19', 'jalley'),
	(32, 'image/jpeg', 2166390, 'jpg', '/00000000032/product.jpg', '202202241650090.jpg', '2022/02/24/32/', '2022-02-24 16:50:09', 'jalley'),
	(33, 'image/jpeg', 2369432, 'jpg', '/00000000033/product.jpg', '202202241650570.jpg', '2022/02/24/33/', '2022-02-24 16:50:57', 'jalley'),
	(34, 'image/jpeg', 2369432, 'jpg', '/00000000034/product.jpg', '202202241651410.jpg', '2022/02/24/34/', '2022-02-24 16:51:41', 'jalley'),
	(35, 'image/jpeg', 1144958, 'jpg', '/00000000035/product.jpg', '202202241655210.jpg', '2022/02/24/35/', '2022-02-24 16:55:21', 'jalley'),
	(36, 'image/jpeg', 1144958, 'jpg', '/00000000036/product.jpg', '202202241656070.jpg', '2022/02/24/36/', '2022-02-24 16:56:07', 'jalley'),
	(37, 'image/jpeg', 1009857, 'jpg', '/00000000037/product.jpg', '202202241656520.jpg', '2022/02/24/37/', '2022-02-24 16:56:52', 'jalley'),
	(38, 'image/jpeg', 1144958, 'jpg', '/00000000038/product.jpg', '202202241658170.jpg', '2022/02/24/38/', '2022-02-24 16:58:17', 'jalley'),
	(39, 'image/jpeg', 1019362, 'jpg', '/00000000039/product.jpg', '202202241659140.jpg', '2022/02/24/39/', '2022-02-24 16:59:14', 'jalley'),
	(40, 'image/jpeg', 1019362, 'jpg', '/00000000040/product.jpg', '202202241659520.jpg', '2022/02/24/40/', '2022-02-24 16:59:52', 'jalley'),
	(41, 'image/jpeg', 1026742, 'jpg', '/00000000041/product.jpg', '202202241703340.jpg', '2022/02/24/41/', '2022-02-24 17:03:34', 'jalley'),
	(42, 'image/jpeg', 1026742, 'jpg', '/00000000042/product.jpg', '202202241704440.jpg', '2022/02/24/42/', '2022-02-24 17:04:44', 'jalley'),
	(43, 'image/jpeg', 1144958, 'jpg', '/00000000043/product.jpg', '202202241705560.jpg', '2022/02/24/43/', '2022-02-24 17:05:56', 'jalley'),
	(44, 'image/jpeg', 1144958, 'jpg', '/00000000044/product.jpg', '202202241706580.jpg', '2022/02/24/44/', '2022-02-24 17:06:58', 'jalley'),
	(45, 'image/jpeg', 1019362, 'jpg', '/00000000045/product.jpg', '202202241709280.jpg', '2022/02/24/45/', '2022-02-24 17:09:28', 'jalley'),
	(46, 'image/jpeg', 1019362, 'jpg', '/00000000046/product.jpg', '202202241710410.jpg', '2022/02/24/46/', '2022-02-24 17:10:41', 'jalley'),
	(47, 'image/jpeg', 1144958, 'jpg', '/00000000047/product.jpg', '202202241711250.jpg', '2022/02/24/47/', '2022-02-24 17:11:25', 'jalley'),
	(48, 'image/jpeg', 1019362, 'jpg', '/00000000048/product.jpg', '202202241712100.jpg', '2022/02/24/48/', '2022-02-24 17:12:10', 'jalley'),
	(49, 'image/jpeg', 1026742, 'jpg', '/00000000049/product.jpg', '202202241713040.jpg', '2022/02/24/49/', '2022-02-24 17:13:04', 'jalley'),
	(50, 'image/jpeg', 1026742, 'jpg', '/00000000050/product.jpg', '202202241713460.jpg', '2022/02/24/50/', '2022-02-24 17:13:46', 'jalley'),
	(51, 'image/jpeg', 1130098, 'jpg', '/00000000051/product.jpg', '202202241715000.jpg', '2022/02/24/51/', '2022-02-24 17:15:00', 'jalley'),
	(52, 'image/jpeg', 1130098, 'jpg', '/00000000052/product.jpg', '202202241715550.jpg', '2022/02/24/52/', '2022-02-24 17:15:55', 'jalley'),
	(53, 'image/jpeg', 1011425, 'jpg', '/00000000053/product.jpg', '202202241716500.jpg', '2022/02/24/53/', '2022-02-24 17:16:50', 'jalley'),
	(54, 'image/jpeg', 1011425, 'jpg', '/00000000054/product.jpg', '202202241717340.jpg', '2022/02/24/54/', '2022-02-24 17:17:34', 'jalley'),
	(55, 'image/jpeg', 1130098, 'jpg', '/00000000055/product.jpg', '202202241718360.jpg', '2022/02/24/55/', '2022-02-24 17:18:36', 'jalley'),
	(56, 'image/jpeg', 1009857, 'jpg', '/00000000056/product.jpg', '202202241719240.jpg', '2022/02/24/56/', '2022-02-24 17:19:24', 'jalley'),
	(57, 'image/jpeg', 1009857, 'jpg', '/00000000057/product.jpg', '202202241720040.jpg', '2022/02/24/57/', '2022-02-24 17:20:04', 'jalley'),
	(58, 'image/jpeg', 1150200, 'jpg', '/00000000058/product.jpg', '202202241726450.jpg', '2022/02/24/58/', '2022-02-24 17:26:45', 'jalley'),
	(59, 'image/jpeg', 1387762, 'jpg', '/00000000059/product.jpg', '202202241727400.jpg', '2022/02/24/59/', '2022-02-24 17:27:40', 'jalley'),
	(60, 'image/jpeg', 1111678, 'jpg', '/00000000060/product.jpg', '202202241728350.jpg', '2022/02/24/60/', '2022-02-24 17:28:35', 'jalley'),
	(61, 'image/jpeg', 119101, 'jpg', '/00000000061/product.jpg', '202202241730420.jpg', '2022/02/24/61/', '2022-02-24 17:30:42', 'jalley'),
	(62, 'image/jpeg', 1074372, 'jpg', '/00000000062/product.jpg', '202202241731370.jpg', '2022/02/24/62/', '2022-02-24 17:31:37', 'jalley'),
	(63, 'image/jpeg', 119101, 'jpg', '/00000000063/product.jpg', '202202241732270.jpg', '2022/02/24/63/', '2022-02-24 17:32:27', 'jalley'),
	(64, 'image/jpeg', 119101, 'jpg', '/00000000064/product.jpg', '202202241733290.jpg', '2022/02/24/64/', '2022-02-24 17:33:29', 'jalley'),
	(65, 'image/jpeg', 1074372, 'jpg', '/00000000065/product.jpg', '202202241734260.jpg', '2022/02/24/65/', '2022-02-24 17:34:26', 'jalley'),
	(66, 'image/jpeg', 119101, 'jpg', '/66/product.jpg', '202202251649530.jpg', '2022/02/25/66/', '2022-02-25 16:49:53', 'jalley'),
	(67, 'image/jpeg', 1120160, 'jpg', '/67/product.jpg', '202202281439070.jpg', '2022/02/28/67/', '2022-02-28 14:39:07', 'jalley'),
	(68, 'image/jpeg', 1025232, 'jpg', '/68/product.jpg', '202202281440280.jpg', '2022/02/28/68/', '2022-02-28 14:40:28', 'jalley'),
	(69, 'image/jpeg', 1118059, 'jpg', '/69/product.jpg', '202202281441200.jpg', '2022/02/28/69/', '2022-02-28 14:41:20', 'jalley'),
	(70, 'image/jpeg', 1118059, 'jpg', '/70/product.jpg', '202202281442100.jpg', '2022/02/28/70/', '2022-02-28 14:42:10', 'jalley'),
	(71, 'image/jpeg', 1025232, 'jpg', '/71/product.jpg', '202202281443270.jpg', '2022/02/28/71/', '2022-02-28 14:43:27', 'jalley'),
	(72, 'image/jpeg', 1118059, 'jpg', '/72/product.jpg', '202202281444200.jpg', '2022/02/28/72/', '2022-02-28 14:44:20', 'jalley'),
	(73, 'image/jpeg', 1025232, 'jpg', '/73/product.jpg', '202202281445030.jpg', '2022/02/28/73/', '2022-02-28 14:45:03', 'jalley'),
	(74, 'image/jpeg', 1025232, 'jpg', '/74/product.jpg', '202202281445510.jpg', '2022/02/28/74/', '2022-02-28 14:45:51', 'jalley'),
	(75, 'image/jpeg', 1118059, 'jpg', '/75/product.jpg', '202202281446400.jpg', '2022/02/28/75/', '2022-02-28 14:46:40', 'jalley'),
	(76, 'image/jpeg', 1118059, 'jpg', '/76/product.jpg', '202202281447230.jpg', '2022/02/28/76/', '2022-02-28 14:47:23', 'jalley'),
	(77, 'image/jpeg', 1111477, 'jpg', '/77/product.jpg', '202202281449020.jpg', '2022/02/28/77/', '2022-02-28 14:49:02', 'jalley'),
	(78, 'image/jpeg', 1111477, 'jpg', '/78/product.jpg', '202202281449560.jpg', '2022/02/28/78/', '2022-02-28 14:49:56', 'jalley'),
	(79, 'image/jpeg', 1029602, 'jpg', '/79/product.jpg', '202202281451130.jpg', '2022/02/28/79/', '2022-02-28 14:51:13', 'jalley'),
	(80, 'image/jpeg', 1029602, 'jpg', '/80/product.jpg', '202202281452070.jpg', '2022/02/28/80/', '2022-02-28 14:52:07', 'jalley'),
	(81, 'image/jpeg', 1111477, 'jpg', '/00000000081/product.jpg', '202202281453050.jpg', '2022/02/28/81/', '2022-02-28 14:53:05', 'jalley'),
	(82, 'image/jpeg', 1113102, 'jpg', '/82/product.jpg', '202202281454200.jpg', '2022/02/28/82/', '2022-02-28 14:54:20', 'jalley'),
	(83, 'image/jpeg', 1113102, 'jpg', '/83/product.jpg', '202202281456240.jpg', '2022/02/28/83/', '2022-02-28 14:56:24', 'jalley'),
	(84, 'image/jpeg', 1021696, 'jpg', '/84/product.jpg', '202202281457380.jpg', '2022/02/28/84/', '2022-02-28 14:57:38', 'jalley'),
	(85, 'image/jpeg', 1113102, 'jpg', '/85/product.jpg', '202202281459270.jpg', '2022/02/28/85/', '2022-02-28 14:59:27', 'jalley'),
	(86, 'image/jpeg', 1126581, 'jpg', '/00000000086/product.jpg', '202202281505490.jpg', '2022/02/28/86/', '2022-02-28 15:05:49', 'jalley'),
	(87, 'image/jpeg', 1126581, 'jpg', '/87/product.jpg', '202202281508470.jpg', '2022/02/28/87/', '2022-02-28 15:08:47', 'jalley'),
	(88, 'image/jpeg', 1009991, 'jpg', '/88/product.jpg', '202202281509420.jpg', '2022/02/28/88/', '2022-02-28 15:09:42', 'jalley'),
	(89, 'image/jpeg', 1009991, 'jpg', '/00000000089/product.jpg', '202202281511000.jpg', '2022/02/28/89/', '2022-02-28 15:11:00', 'jalley'),
	(90, 'image/jpeg', 1126581, 'jpg', '/90/product.jpg', '202202281514280.jpg', '2022/02/28/90/', '2022-02-28 15:14:28', 'jalley'),
	(91, 'image/jpeg', 1009991, 'jpg', '/91/product.jpg', '202202281515160.jpg', '2022/02/28/91/', '2022-02-28 15:15:16', 'jalley'),
	(92, 'image/jpeg', 1102329, 'jpg', '/92/product.jpg', '202202281523390.jpg', '2022/02/28/92/', '2022-02-28 15:23:39', 'jalley'),
	(93, 'image/jpeg', 1102329, 'jpg', '/93/product.jpg', '202202281524220.jpg', '2022/02/28/93/', '2022-02-28 15:24:22', 'jalley'),
	(94, 'image/jpeg', 1102329, 'jpg', '/94/product.jpg', '202202281526070.jpg', '2022/02/28/94/', '2022-02-28 15:26:07', 'jalley'),
	(95, 'image/jpeg', 1102329, 'jpg', '/95/product.jpg', '202202281526450.jpg', '2022/02/28/95/', '2022-02-28 15:26:45', 'jalley'),
	(96, 'image/jpeg', 1312311, 'jpg', '/96/product.jpg', '202202281549380.jpg', '2022/02/28/96/', '2022-02-28 15:49:38', 'jalley'),
	(97, 'image/jpeg', 1312311, 'jpg', '/97/product.jpg', '202202281551390.jpg', '2022/02/28/97/', '2022-02-28 15:51:39', 'jalley'),
	(98, 'image/jpeg', 41450, 'jpg', '/00000000098/product.jpg', '202202281553310.jpg', '2022/02/28/98/', '2022-02-28 15:53:31', 'jalley'),
	(99, 'image/jpeg', 56590, 'jpg', '/00000000099/product.jpg', '202202281554200.jpg', '2022/02/28/99/', '2022-02-28 15:54:20', 'jalley'),
	(100, 'image/jpeg', 1165260, 'jpg', '/100/product.jpg', '202202281556430.jpg', '2022/02/28/100/', '2022-02-28 15:56:43', 'jalley'),
	(101, 'image/jpeg', 1207986, 'jpg', '/101/product.jpg', '202202281559420.jpg', '2022/02/28/101/', '2022-02-28 15:59:42', 'jalley'),
	(102, 'image/jpeg', 1207986, 'jpg', '/102/product.jpg', '202202281600410.jpg', '2022/02/28/102/', '2022-02-28 16:00:41', 'jalley'),
	(103, 'image/jpeg', 1207986, 'jpg', '/103/product.jpg', '202202281601290.jpg', '2022/02/28/103/', '2022-02-28 16:01:29', 'jalley'),
	(104, 'image/jpeg', 1097384, 'jpg', '/104/product.jpg', '202202281602230.jpg', '2022/02/28/104/', '2022-02-28 16:02:23', 'jalley'),
	(105, 'image/jpeg', 1151919, 'jpg', '/105/product.jpg', '202202281603230.jpg', '2022/02/28/105/', '2022-02-28 16:03:23', 'jalley'),
	(106, 'image/jpeg', 1097384, 'jpg', '/106/product.jpg', '202202281604160.jpg', '2022/02/28/106/', '2022-02-28 16:04:16', 'jalley'),
	(107, 'image/jpeg', 1151919, 'jpg', '/107/product.jpg', '202202281605040.jpg', '2022/02/28/107/', '2022-02-28 16:05:04', 'jalley'),
	(108, 'image/jpeg', 1097384, 'jpg', '/108/product.jpg', '202202281605390.jpg', '2022/02/28/108/', '2022-02-28 16:05:39', 'jalley'),
	(109, 'image/jpeg', 1151919, 'jpg', '/109/product.jpg', '202202281606090.jpg', '2022/02/28/109/', '2022-02-28 16:06:09', 'jalley'),
	(110, 'image/jpeg', 1245252, 'jpg', '/110/product.jpg', '202202281607510.jpg', '2022/02/28/110/', '2022-02-28 16:07:51', 'jalley'),
	(111, 'image/jpeg', 1245252, 'jpg', '/111/product.jpg', '202202281608340.jpg', '2022/02/28/111/', '2022-02-28 16:08:34', 'jalley'),
	(112, 'image/jpeg', 149570, 'jpg', '/00000000112/product.jpg', '202202281610470.jpg', '2022/02/28/112/', '2022-02-28 16:10:47', 'jalley'),
	(113, 'image/jpeg', 1038503, 'jpg', '/113/product.jpg', '202202281611480.jpg', '2022/02/28/113/', '2022-02-28 16:11:48', 'jalley'),
	(114, 'image/jpeg', 1038503, 'jpg', '/114/product.jpg', '202202281612490.jpg', '2022/02/28/114/', '2022-02-28 16:12:49', 'jalley'),
	(115, 'image/jpeg', 1213206, 'jpg', '/115/product.jpg', '202202281613500.jpg', '2022/02/28/115/', '2022-02-28 16:13:50', 'jalley'),
	(116, 'image/jpeg', 1309225, 'jpg', '/116/product.jpg', '202202281614380.jpg', '2022/02/28/116/', '2022-02-28 16:14:38', 'jalley'),
	(117, 'image/jpeg', 1074453, 'jpg', '/117/product.jpg', '202202281615580.jpg', '2022/02/28/117/', '2022-02-28 16:15:58', 'jalley'),
	(118, 'image/jpeg', 1041805, 'jpg', '/118/product.jpg', '202202281617130.jpg', '2022/02/28/118/', '2022-02-28 16:17:13', 'jalley'),
	(119, 'image/jpeg', 1066441, 'jpg', '/119/product.jpg', '202202281618160.jpg', '2022/02/28/119/', '2022-02-28 16:18:16', 'jalley'),
	(120, 'image/jpeg', 1028985, 'jpg', '/120/product.jpg', '202202281619200.jpg', '2022/02/28/120/', '2022-02-28 16:19:20', 'jalley'),
	(121, 'image/jpeg', 1045559, 'jpg', '/121/product.jpg', '202202281620470.jpg', '2022/02/28/121/', '2022-02-28 16:20:47', 'jalley'),
	(122, 'image/jpeg', 971936, 'jpg', '/122/product.jpg', '202202281622050.jpg', '2022/02/28/122/', '2022-02-28 16:22:05', 'jalley'),
	(123, 'image/jpeg', 1045559, 'jpg', '/123/product.jpg', '202202281622570.jpg', '2022/02/28/123/', '2022-02-28 16:22:57', 'jalley'),
	(124, 'image/jpeg', 1138929, 'jpg', '/124/product.jpg', '202202281625270.jpg', '2022/02/28/124/', '2022-02-28 16:25:27', 'jalley'),
	(125, 'image/jpeg', 1196634, 'jpg', '/00000000125/product.jpg', '202202281626110.jpg', '2022/02/28/125/', '2022-02-28 16:26:11', 'jalley'),
	(126, 'image/jpeg', 1198387, 'jpg', '/126/product.jpg', '202202281627300.jpg', '2022/02/28/126/', '2022-02-28 16:27:30', 'jalley'),
	(127, 'image/jpeg', 1198387, 'jpg', '/127/product.jpg', '202202281628050.jpg', '2022/02/28/127/', '2022-02-28 16:28:05', 'jalley'),
	(128, 'image/jpeg', 1110109, 'jpg', '/128/product.jpg', '202202281632130.jpg', '2022/02/28/128/', '2022-02-28 16:32:13', 'jalley'),
	(129, 'image/jpeg', 1090454, 'jpg', '/129/product.jpg', '202202281633170.jpg', '2022/02/28/129/', '2022-02-28 16:33:17', 'jalley'),
	(130, 'image/jpeg', 1090454, 'jpg', '/130/product.jpg', '202202281634010.jpg', '2022/02/28/130/', '2022-02-28 16:34:01', 'jalley'),
	(131, 'image/jpeg', 1095440, 'jpg', '/131/product.jpg', '202202281634550.jpg', '2022/02/28/131/', '2022-02-28 16:34:55', 'jalley'),
	(132, 'image/jpeg', 1095440, 'jpg', '/132/product.jpg', '202202281635530.jpg', '2022/02/28/132/', '2022-02-28 16:35:53', 'jalley'),
	(133, 'image/jpeg', 506049, 'jpg', '/133/product.jpg', '202202281637570.jpg', '2022/02/28/133/', '2022-02-28 16:37:57', 'jalley'),
	(134, 'image/jpeg', 506049, 'jpg', '/134/product.jpg', '202203021622140.jpg', '2022/03/02/134/', '2022-03-02 16:22:14', 'jalley'),
	(135, 'image/jpeg', 477163, 'jpg', '/135/product.jpg', '202203021644290.jpg', '2022/03/02/135/', '2022-03-02 16:44:29', 'jalley'),
	(136, 'image/jpeg', 477163, 'jpg', '/136/product.jpg', '202203021645390.jpg', '2022/03/02/136/', '2022-03-02 16:45:39', 'jalley'),
	(137, 'image/jpeg', 472280, 'jpg', '/137/product.jpg', '202203021657350.jpg', '2022/03/02/137/', '2022-03-02 16:57:35', 'jalley'),
	(138, 'image/jpeg', 472280, 'jpg', '/138/product.jpg', '202203021658330.jpg', '2022/03/02/138/', '2022-03-02 16:58:33', 'jalley'),
	(139, 'image/jpeg', 462677, 'jpg', '/139/product.jpg', '202203021707260.jpg', '2022/03/02/139/', '2022-03-02 17:07:26', 'jalley'),
	(140, 'image/jpeg', 428869, 'jpg', '/140/product.jpg', '202203021720270.jpg', '2022/03/02/140/', '2022-03-02 17:20:27', 'jalley'),
	(141, 'image/jpeg', 2369432, 'jpg', '/141/product.jpg', '202203031419250.jpg', '2022/03/03/141/', '2022-03-03 14:19:25', 'jalley'),
	(142, 'image/jpeg', 428869, 'jpg', '/142/product.jpg', '202203031547470.jpg', '2022/03/03/142/', '2022-03-03 15:47:47', 'jalley'),
	(143, 'image/jpeg', 464061, 'jpg', '/143/product.jpg', '202203031549160.jpg', '2022/03/03/143/', '2022-03-03 15:49:16', 'jalley'),
	(144, 'image/jpeg', 594897, 'jpg', '/144/product.jpg', '202203031551060.jpg', '2022/03/03/144/', '2022-03-03 15:51:06', 'jalley'),
	(145, 'image/jpeg', 481429, 'jpg', '/145/product.jpg', '202203031552200.jpg', '2022/03/03/145/', '2022-03-03 15:52:20', 'jalley'),
	(146, 'image/jpeg', 487991, 'jpg', '/146/product.jpg', '202203031554030.jpg', '2022/03/03/146/', '2022-03-03 15:54:03', 'jalley'),
	(147, 'image/jpeg', 412214, 'jpg', '/147/product.jpg', '202203031557580.jpg', '2022/03/03/147/', '2022-03-03 15:57:58', 'jalley'),
	(148, 'image/jpeg', 369424, 'jpg', '/00000000148/product.jpg', '202203031611280.jpg', '2022/03/03/148/', '2022-03-03 16:11:28', 'jalley'),
	(149, 'image/jpeg', 369424, 'jpg', '/149/product.jpg', '202203071539120.jpg', '2022/03/07/149/', '2022-03-07 15:39:12', 'jalley'),
	(150, 'image/jpeg', 437786, 'jpg', '/150/product.jpg', '202203071540160.jpg', '2022/03/07/150/', '2022-03-07 15:40:16', 'jalley'),
	(151, 'image/jpeg', 437786, 'jpg', '/151/product.jpg', '202203071541010.jpg', '2022/03/07/151/', '2022-03-07 15:41:01', 'jalley'),
	(152, 'image/jpeg', 369424, 'jpg', '/152/product.jpg', '202203071541590.jpg', '2022/03/07/152/', '2022-03-07 15:41:59', 'jalley'),
	(153, 'image/jpeg', 466227, 'jpg', '/153/product.jpg', '202203071542460.jpg', '2022/03/07/153/', '2022-03-07 15:42:46', 'jalley'),
	(154, 'image/jpeg', 597646, 'jpg', '/00000000154/product.jpg', '202203071544320.jpg', '2022/03/07/154/', '2022-03-07 15:44:32', 'jalley'),
	(155, 'image/jpeg', 533637, 'jpg', '/155/product.jpg', '202203071550480.jpg', '2022/03/07/155/', '2022-03-07 15:50:48', 'jalley'),
	(156, 'image/jpeg', 597646, 'jpg', '/156/product.jpg', '202203071551360.jpg', '2022/03/07/156/', '2022-03-07 15:51:36', 'jalley'),
	(157, 'image/jpeg', 466227, 'jpg', '/157/product.jpg', '202203071552470.jpg', '2022/03/07/157/', '2022-03-07 15:52:47', 'jalley'),
	(158, 'image/jpeg', 533637, 'jpg', '/158/product.jpg', '202203071553560.jpg', '2022/03/07/158/', '2022-03-07 15:53:56', 'jalley'),
	(159, 'image/jpeg', 597646, 'jpg', '/159/product.jpg', '202203071555520.jpg', '2022/03/07/159/', '2022-03-07 15:55:52', 'jalley'),
	(160, 'image/jpeg', 499028, 'jpg', '/160/product.jpg', '202203071557570.jpg', '2022/03/07/160/', '2022-03-07 15:57:57', 'jalley'),
	(161, 'image/jpeg', 415362, 'jpg', '/161/product.jpg', '202203071558400.jpg', '2022/03/07/161/', '2022-03-07 15:58:40', 'jalley'),
	(162, 'image/jpeg', 462007, 'jpg', '/162/product.jpg', '202203071559580.jpg', '2022/03/07/162/', '2022-03-07 15:59:58', 'jalley'),
	(163, 'image/jpeg', 462007, 'jpg', '/163/product.jpg', '202203071601240.jpg', '2022/03/07/163/', '2022-03-07 16:01:24', 'jalley'),
	(164, 'image/jpeg', 462007, 'jpg', '/164/product.jpg', '202203071602300.jpg', '2022/03/07/164/', '2022-03-07 16:02:30', 'jalley'),
	(165, 'image/jpeg', 499028, 'jpg', '/165/product.jpg', '202203071603180.jpg', '2022/03/07/165/', '2022-03-07 16:03:18', 'jalley'),
	(166, 'image/jpeg', 415362, 'jpg', '/166/product.jpg', '202203071604020.jpg', '2022/03/07/166/', '2022-03-07 16:04:02', 'jalley'),
	(167, 'image/jpeg', 422688, 'jpg', '/00000000167/product.jpg', '202203071608500.jpg', '2022/03/07/167/', '2022-03-07 16:08:50', 'jalley'),
	(168, 'image/jpeg', 493729, 'jpg', '/00000000168/product.jpg', '202203071610150.jpg', '2022/03/07/168/', '2022-03-07 16:10:15', 'jalley'),
	(169, 'image/jpeg', 422688, 'jpg', '/00000000169/product.jpg', '202203071611170.jpg', '2022/03/07/169/', '2022-03-07 16:11:17', 'jalley'),
	(170, 'image/jpeg', 493729, 'jpg', '/00000000170/product.jpg', '202203071612090.jpg', '2022/03/07/170/', '2022-03-07 16:12:09', 'jalley'),
	(171, 'image/jpeg', 493729, 'jpg', '/00000000171/product.jpg', '202203071612490.jpg', '2022/03/07/171/', '2022-03-07 16:12:49', 'jalley'),
	(172, 'image/jpeg', 422688, 'jpg', '/00000000172/product.jpg', '202203071613340.jpg', '2022/03/07/172/', '2022-03-07 16:13:34', 'jalley'),
	(173, 'image/jpeg', 183520, 'jpg', '/00000000173/product.jpg', '202203101546530.jpg', '2022/03/10/173/', '2022-03-10 15:46:53', 'jalley'),
	(174, 'image/jpeg', 183520, 'jpg', '/174/product.jpg', '202203101550510.jpg', '2022/03/10/174/', '2022-03-10 15:50:51', 'jalley'),
	(175, 'image/jpeg', 186960, 'jpg', '/175/product.jpg', '202203101552180.jpg', '2022/03/10/175/', '2022-03-10 15:52:18', 'jalley'),
	(176, 'image/jpeg', 186960, 'jpg', '/176/product.jpg', '202203101553210.jpg', '2022/03/10/176/', '2022-03-10 15:53:21', 'jalley'),
	(177, 'image/jpeg', 196126, 'jpg', '/00000000177/product.jpg', '202203101554110.jpg', '2022/03/10/177/', '2022-03-10 15:54:11', 'jalley'),
	(178, 'image/jpeg', 196126, 'jpg', '/178/product.jpg', '202203101555250.jpg', '2022/03/10/178/', '2022-03-10 15:55:25', 'jalley'),
	(179, 'image/jpeg', 182895, 'jpg', '/179/product.jpg', '202203101557210.jpg', '2022/03/10/179/', '2022-03-10 15:57:21', 'jalley'),
	(180, 'image/jpeg', 182895, 'jpg', '/180/product.jpg', '202203101558220.jpg', '2022/03/10/180/', '2022-03-10 15:58:22', 'jalley'),
	(181, 'image/jpeg', 182895, 'jpg', '/181/product.jpg', '202203101559530.jpg', '2022/03/10/181/', '2022-03-10 15:59:53', 'jalley'),
	(182, 'image/jpeg', 182895, 'jpg', '/182/product.jpg', '202203101600290.jpg', '2022/03/10/182/', '2022-03-10 16:00:29', 'jalley'),
	(183, 'image/jpeg', 499028, 'jpg', '/183/product.jpg', '202203311720000.jpg', '2022/03/31/183/', '2022-03-31 17:20:00', 'jalley'),
	(184, 'image/jpeg', 499028, 'jpg', '/184/product.jpg', '202203311721000.jpg', '2022/03/31/184/', '2022-03-31 17:21:00', 'jalley'),
	(185, 'image/jpeg', 473731, 'jpg', '/185/product.jpg', '202203311724040.jpg', '2022/03/31/185/', '2022-03-31 17:24:04', 'jalley'),
	(186, 'image/jpeg', 473731, 'jpg', '/186/product.jpg', '202203311725060.jpg', '2022/03/31/186/', '2022-03-31 17:25:06', 'jalley'),
	(187, 'image/jpeg', 473731, 'jpg', '/187/product.jpg', '202203311725480.jpg', '2022/03/31/187/', '2022-03-31 17:25:48', 'jalley'),
	(188, 'image/jpeg', 222700, 'jpg', '/188/product.jpg', '202203311726420.jpg', '2022/03/31/188/', '2022-03-31 17:26:42', 'jalley'),
	(189, 'image/jpeg', 222700, 'jpg', '/189/product.jpg', '202203311727250.jpg', '2022/03/31/189/', '2022-03-31 17:27:25', 'jalley'),
	(190, 'image/jpeg', 222700, 'jpg', '/190/product.jpg', '202203311728080.jpg', '2022/03/31/190/', '2022-03-31 17:28:08', 'jalley'),
	(191, 'image/jpeg', 452741, 'jpg', '/00000000191/product.jpg', '202203311728460.jpg', '2022/03/31/191/', '2022-03-31 17:28:46', 'jalley'),
	(192, 'image/jpeg', 452741, 'jpg', '/192/product.jpg', '202203311730040.jpg', '2022/03/31/192/', '2022-03-31 17:30:04', 'jalley'),
	(193, 'image/jpeg', 452741, 'jpg', '/193/product.jpg', '202203311731050.jpg', '2022/03/31/193/', '2022-03-31 17:31:05', 'jalley'),
	(194, 'image/jpeg', 550273, 'jpg', '/194/product.jpg', '202204041445510.jpg', '2022/04/04/194/', '2022-04-04 14:45:51', 'jalley'),
	(195, 'image/jpeg', 550273, 'jpg', '/195/product.jpg', '202204041446320.jpg', '2022/04/04/195/', '2022-04-04 14:46:32', 'jalley'),
	(196, 'image/jpeg', 550273, 'jpg', '/196/product.jpg', '202204041447230.jpg', '2022/04/04/196/', '2022-04-04 14:47:23', 'jalley'),
	(197, 'image/jpeg', 628967, 'jpg', '/197/product.jpg', '202204041448090.jpg', '2022/04/04/197/', '2022-04-04 14:48:09', 'jalley'),
	(198, 'image/jpeg', 628967, 'jpg', '/198/product.jpg', '202204041448450.jpg', '2022/04/04/198/', '2022-04-04 14:48:45', 'jalley'),
	(199, 'image/jpeg', 1961492, 'jpg', '/00000000199/product.jpg', '202204041450170.jpg', '2022/04/04/199/', '2022-04-04 14:50:17', 'jalley'),
	(200, 'image/jpeg', 1961492, 'jpg', '/00000000200/product.jpg', '202204041451140.jpg', '2022/04/04/200/', '2022-04-04 14:51:14', 'jalley'),
	(201, 'image/jpeg', 2168111, 'jpg', '/00000000201/product.jpg', '202204041452040.jpg', '2022/04/04/201/', '2022-04-04 14:52:04', 'jalley'),
	(202, 'image/jpeg', 2168111, 'jpg', '/00000000202/product.jpg', '202204041452500.jpg', '2022/04/04/202/', '2022-04-04 14:52:50', 'jalley'),
	(203, 'image/jpeg', 1961492, 'jpg', '/00000000203/product.jpg', '202204041457180.jpg', '2022/04/04/203/', '2022-04-04 14:57:18', 'jalley');
/*!40000 ALTER TABLE `file` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.lang
DROP TABLE IF EXISTS `lang`;
CREATE TABLE IF NOT EXISTS `lang` (
  `lang_cd` varchar(3) NOT NULL COMMENT '언어코드',
  `lang_name` varchar(45) NOT NULL COMMENT '언어명',
  `use_yn` varchar(1) DEFAULT NULL COMMENT '사용유무',
  PRIMARY KEY (`lang_cd`),
  UNIQUE KEY `UQ_lang_1` (`lang_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='언어테이블';

-- Dumping data for table jalley_auction.lang: ~0 rows (approximately)
DELETE FROM `lang`;
/*!40000 ALTER TABLE `lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `lang` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '주문아이디',
  `order_type` varchar(45) NOT NULL COMMENT '주문서구분',
  `order_kind` varchar(45) NOT NULL COMMENT '주문서종류',
  `pay_status` varchar(45) NOT NULL,
  `user_id` varchar(45) NOT NULL COMMENT '사용자아이디',
  `total_price` bigint(20) NOT NULL COMMENT '종합가격',
  `total_discount_price` bigint(20) NOT NULL COMMENT '종합할인',
  `delivery_id` bigint(20) DEFAULT NULL COMMENT '배송지아이디',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '생성일',
  `create_id` varchar(32) NOT NULL COMMENT '사용자아이디',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='판매';

-- Dumping data for table jalley_auction.orders: ~2 rows (approximately)
DELETE FROM `orders`;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`order_id`, `order_type`, `order_kind`, `pay_status`, `user_id`, `total_price`, `total_discount_price`, `delivery_id`, `create_date`, `create_id`) VALUES
	(1, '2', '3', '4', 'emp04', 940000, 0, NULL, '2022-06-09 08:42:34', 'admin'),
	(2, '2', '3', '1', 'test', 1200000, 20000, NULL, '2022-06-09 11:08:10', 'admin');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.order_items
DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '주문아이템아이디',
  `order_id` bigint(20) NOT NULL COMMENT '주문아이디',
  `ref_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) NOT NULL COMMENT '상품아이디',
  `store_id` varchar(32) NOT NULL COMMENT '상점아이디',
  `product_type` varchar(45) NOT NULL COMMENT '상품종류',
  `cnt` int(11) NOT NULL COMMENT '수량',
  `product_name` varchar(200) NOT NULL COMMENT '상품명',
  `serial_num` varchar(100) NOT NULL COMMENT '시리얼넘버',
  `price` bigint(20) NOT NULL COMMENT '가격',
  `discount_price` bigint(20) NOT NULL COMMENT '할인료',
  `rent_price` bigint(20) NOT NULL COMMENT '대여료',
  `memo` varchar(45) DEFAULT NULL COMMENT '메모',
  `display_yn` varchar(1) NOT NULL COMMENT '상품노출여부',
  `product_status` varchar(45) NOT NULL COMMENT '상품상태',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '생성일',
  `create_id` varchar(32) NOT NULL COMMENT '생성자',
  PRIMARY KEY (`order_item_id`),
  KEY `FK_order_items_order_id_orders_order_id` (`order_id`),
  CONSTRAINT `FK_order_items_order_id_orders_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COMMENT='상품';

-- Dumping data for table jalley_auction.order_items: ~5 rows (approximately)
DELETE FROM `order_items`;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` (`order_item_id`, `order_id`, `ref_id`, `product_id`, `store_id`, `product_type`, `cnt`, `product_name`, `serial_num`, `price`, `discount_price`, `rent_price`, `memo`, `display_yn`, `product_status`, `use_yn`, `create_date`, `create_id`) VALUES
	(1, 1, 1, 200, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-2', 1440000, 0, 720000, NULL, 'Y', '4', 'Y', '2022-06-09 08:42:34', 'admin'),
	(2, 1, 2, 187, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-3', 440000, 0, 220000, NULL, 'Y', '4', 'Y', '2022-06-09 08:42:34', 'admin'),
	(3, 2, 3, 181, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-1', 220000, 20000, 200000, NULL, 'N', '1', 'N', '2022-06-09 11:08:10', 'admin'),
	(4, 2, 4, 171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 20000, 200000, NULL, 'Y', '4', 'Y', '2022-06-09 11:08:10', 'admin'),
	(5, 2, 5, 197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 1000000, 0, 1000000, NULL, 'Y', '4', 'Y', '2022-06-09 16:34:52', 'admin');
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.payment
DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '결제아이디',
  `pay_status` varchar(45) DEFAULT NULL COMMENT '결제상태',
  `order_item_id` bigint(20) DEFAULT NULL COMMENT '주문아이템아이디',
  `tx_id` varchar(45) DEFAULT NULL COMMENT '결제아이디',
  PRIMARY KEY (`payment_id`),
  KEY `FK_payment_order_item_id_order_items_order_item_id` (`order_item_id`),
  CONSTRAINT `FK_payment_order_item_id_order_items_order_item_id` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`order_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COMMENT='결제';

-- Dumping data for table jalley_auction.payment: ~5 rows (approximately)
DELETE FROM `payment`;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` (`payment_id`, `pay_status`, `order_item_id`, `tx_id`) VALUES
	(1, '5', 1, NULL),
	(2, '1', 2, NULL),
	(3, '1', 3, NULL),
	(4, '1', 4, NULL),
	(5, '1', 5, NULL);
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `product_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '상품아이디',
  `store_id` varchar(32) NOT NULL COMMENT '상점아이디',
  `product_type` varchar(45) NOT NULL COMMENT '상품종류',
  `cnt` int(11) NOT NULL COMMENT '수량',
  `product_name` varchar(200) NOT NULL COMMENT '상품명',
  `serial_num` varchar(100) NOT NULL COMMENT '시리얼넘버',
  `price` bigint(20) NOT NULL COMMENT '가격',
  `discount_price` bigint(20) NOT NULL COMMENT '할인료',
  `rent_price` bigint(20) NOT NULL COMMENT '대여료',
  `memo` varchar(1000) DEFAULT NULL COMMENT '메모',
  `img_path` varchar(1000) NOT NULL COMMENT '이미지경로',
  `display_yn` varchar(1) NOT NULL COMMENT '상품노출여부',
  `product_status` varchar(45) NOT NULL COMMENT '상품상태',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '생성일',
  `create_id` varchar(32) NOT NULL COMMENT '생성자',
  `warranty_start` datetime DEFAULT NULL COMMENT '보증시작기간',
  `warranty_end` datetime DEFAULT NULL COMMENT '보증종료기간',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8mb3 COMMENT='상품';

-- Dumping data for table jalley_auction.product: ~203 rows (approximately)
DELETE FROM `product`;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`product_id`, `store_id`, `product_type`, `cnt`, `product_name`, `serial_num`, `price`, `discount_price`, `rent_price`, `memo`, `img_path`, `display_yn`, `product_status`, `use_yn`, `create_date`, `create_id`, `warranty_start`, `warranty_end`) VALUES
	(1, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-1S', 880000, 880000, 440000, 'System backup', '2022/02/24/1/202202241516140.jpg', 'Y', '1', 'Y', '2022-02-24 15:16:14', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(2, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-5M', 880000, 880000, 440000, 'System backup', '2022/02/24/2/202202241527370.jpg', 'Y', '1', 'Y', '2022-02-24 15:27:37', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(3, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-PK-2S', 880000, 880000, 440000, 'System backup', '2022/02/24/3/202202241529120.jpg', 'Y', '1', 'Y', '2022-02-24 15:29:12', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(4, 'jalley', 'signature', 1, '스타자켓', 'JA21-jk011-PK-3M', 880000, 880000, 440000, 'System backup', '2022/02/24/4/202202241530170.jpg', 'Y', '1', 'Y', '2022-02-24 15:30:17', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(5, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-1S', 880000, 880000, 440000, 'System backup', '2022/02/24/5/202202241551520.jpg', 'Y', '1', 'Y', '2022-02-24 15:51:52', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(6, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-4M', 880000, 880000, 440000, 'System backup', '2022/02/24/6/202202241553020.jpg', 'Y', '1', 'Y', '2022-02-24 15:53:02', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(7, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-2S', 880000, 880000, 440000, 'System backup', '2022/02/24/7/202202241554340.jpg', 'Y', '1', 'Y', '2022-02-24 15:54:34', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(8, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-4S', 880000, 880000, 440000, 'System backup', '2022/02/24/8/202202241555500.jpg', 'Y', '1', 'Y', '2022-02-24 15:55:50', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(9, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-2S', 880000, 880000, 440000, 'System backup', '2022/02/24/9/202202241556390.jpg', 'Y', '1', 'Y', '2022-02-24 15:56:39', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(10, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-3M', 880000, 880000, 440000, 'System backup', '2022/02/24/10/202202241557380.jpg', 'Y', '1', 'Y', '2022-02-24 15:57:38', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(11, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-1S', 440000, 440000, 220000, 'System backup', '2022/02/24/11/202202241602240.jpg', 'Y', '1', 'Y', '2022-02-24 16:02:24', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(12, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-2M', 440000, 440000, 220000, 'System backup', '2022/02/24/12/202202241604540.jpg', 'Y', '1', 'Y', '2022-02-24 16:04:54', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 440000, 220000, 'System backup', '2022/02/24/13/202202241605540.jpg', 'Y', '1', 'Y', '2022-02-24 16:05:54', 'jalley', '2022-05-14 09:44:55', '2022-05-14 09:44:55'),
	(14, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-4M', 440000, 440000, 220000, 'System backup', '2022/02/24/14/202202241607020.jpg', 'Y', '1', 'Y', '2022-02-24 16:07:02', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(15, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-3M', 440000, 440000, 220000, 'System backup', '2022/02/24/15/202202241609430.jpg', 'Y', '1', 'Y', '2022-02-24 16:09:43', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(16, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-1S', 440000, 440000, 220000, 'System backup', '2022/02/24/16/202202241631250.jpg', 'Y', '1', 'Y', '2022-02-24 16:31:25', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(17, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-4M', 440000, 440000, 220000, 'System backup', '2022/02/24/17/202202241632130.jpg', 'Y', '1', 'Y', '2022-02-24 16:32:13', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(18, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-3S', 440000, 440000, 220000, 'System backup', '2022/02/24/18/202202241632560.jpg', 'Y', '1', 'Y', '2022-02-24 16:32:56', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(19, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-2S', 280000, 280000, 140000, 'System backup', '2022/02/24/19/202202241634030.jpg', 'Y', '1', 'Y', '2022-02-24 16:34:03', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(20, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-3M', 280000, 280000, 140000, 'System backup', '2022/02/24/20/202202241634520.jpg', 'Y', '1', 'Y', '2022-02-24 16:34:52', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(21, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-1S', 280000, 280000, 140000, 'System backup', '2022/02/24/21/202202241635400.jpg', 'Y', '1', 'Y', '2022-02-24 16:35:40', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(22, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-4M', 280000, 280000, 140000, 'System backup', '2022/02/24/22/202202241636310.jpg', 'Y', '1', 'Y', '2022-02-24 16:36:31', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 280000, 140000, 'System backup', '2022/02/24/23/202202241637370.jpg', 'Y', '1', 'Y', '2022-02-24 16:37:37', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(24, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-4M', 280000, 280000, 140000, 'System backup', '2022/02/24/24/202202241638290.jpg', 'Y', '1', 'Y', '2022-02-24 16:38:29', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(25, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-3S', 280000, 280000, 140000, 'System backup', '2022/02/24/25/202202241639270.jpg', 'Y', '1', 'Y', '2022-02-24 16:39:27', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(26, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-3S', 280000, 280000, 140000, 'System backup', '2022/02/24/26/202202241639270.jpg', 'Y', '1', 'Y', '2022-02-24 16:39:27', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(27, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-1S', 280000, 280000, 140000, 'System backup', '2022/02/24/27/202202241640080.jpg', 'Y', '1', 'Y', '2022-02-24 16:40:08', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 280000, 140000, 'System backup', '2022/02/24/28/202202241641030.jpg', 'Y', '1', 'Y', '2022-02-24 16:41:03', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(29, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK0054-WH-1M', 280000, 280000, 140000, 'System backup', '2022/02/24/29/202202241641430.jpg', 'Y', '1', 'Y', '2022-02-24 16:41:43', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(30, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-4M', 440000, 440000, 220000, 'System backup', '2022/02/24/30/202202241645510.jpg', 'Y', '1', 'Y', '2022-02-24 16:45:51', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(31, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-1S', 440000, 440000, 220000, 'System backup', '2022/02/24/31/202202241648190.jpg', 'Y', '1', 'Y', '2022-02-24 16:48:19', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(32, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-3M', 440000, 440000, 220000, 'System backup', '2022/02/24/32/202202241650090.jpg', 'Y', '1', 'Y', '2022-02-24 16:50:09', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(33, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-1S', 440000, 440000, 220000, 'System backup', '2022/02/24/33/202202241650570.jpg', 'Y', '1', 'Y', '2022-02-24 16:50:57', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(34, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-3M', 440000, 440000, 220000, 'System backup', '2022/02/24/34/202202241651410.jpg', 'Y', '1', 'Y', '2022-02-24 16:51:41', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(35, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-1S', 880000, 880000, 440000, 'System backup', '2022/02/24/35/202202241655210.jpg', 'Y', '1', 'Y', '2022-02-24 16:55:21', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(36, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-2M', 880000, 880000, 440000, 'System backup', '2022/02/24/36/202202241656070.jpg', 'Y', '1', 'Y', '2022-02-24 16:56:07', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(37, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-WH-1S', 880000, 880000, 440000, 'System backup', '2022/02/24/37/202202241656520.jpg', 'Y', '1', 'Y', '2022-02-24 16:56:52', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(38, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-3S', 880000, 880000, 440000, 'System backup', '2022/02/24/38/202202241658170.jpg', 'Y', '1', 'Y', '2022-02-24 16:58:17', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(39, 'jalley', 'signature', 1, '인어 자켓', 'JA21-JK008-WH-2S', 880000, 880000, 440000, 'System backup', '2022/02/24/39/202202241659140.jpg', 'Y', '1', 'Y', '2022-02-24 16:59:14', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(40, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-WH-3S', 880000, 880000, 440000, 'System backup', '2022/02/24/40/202202241659520.jpg', 'Y', '1', 'Y', '2022-02-24 16:59:52', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(41, 'jalley', 'signature', 1, '인어 케이프', 'JA21-JK006-WH-1', 720000, 720000, 360000, 'System backup', '2022/02/24/41/202202241703340.jpg', 'Y', '1', 'Y', '2022-02-24 17:03:34', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(42, 'jalley', 'signature', 1, '인어 케이프', 'JA21-JK006-WH-2', 720000, 720000, 360000, 'System backup', '2022/02/24/42/202202241704440.jpg', 'Y', '1', 'Y', '2022-02-24 17:04:44', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(43, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-BK-2S', 440000, 440000, 220000, 'System backup', '2022/02/24/43/202202241705560.jpg', 'Y', '1', 'Y', '2022-02-24 17:05:56', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(44, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-BK-2M', 440000, 440000, 220000, 'System backup', '2022/02/24/44/202202241706580.jpg', 'Y', '1', 'Y', '2022-02-24 17:06:58', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(45, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-WH-1S', 440000, 440000, 220000, 'System backup', '2022/02/24/45/202202241709280.jpg', 'Y', '1', 'Y', '2022-02-24 17:09:28', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(46, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-WH-2M', 440000, 440000, 220000, 'System backup', '2022/02/24/46/202202241710410.jpg', 'Y', '1', 'Y', '2022-02-24 17:10:41', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(47, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-BK-1S', 440000, 440000, 220000, 'System backup', '2022/02/24/47/202202241711250.jpg', 'Y', '1', 'Y', '2022-02-24 17:11:25', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(48, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-WH-3S', 440000, 440000, 220000, 'System backup', '2022/02/24/48/202202241712100.jpg', 'Y', '1', 'Y', '2022-02-24 17:12:10', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(49, 'jalley', 'signature', 1, '인어 와이드 팬츠', 'JA21-PT007-WH-1', 600000, 600000, 300000, 'System backup', '2022/02/24/49/202202241713040.jpg', 'Y', '1', 'Y', '2022-02-24 17:13:04', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(50, 'jalley', 'signature', 1, '인어 와이드 팬츠', 'JA21-PT007-WH-2', 600000, 600000, 300000, 'System backup', '2022/02/24/50/202202241713460.jpg', 'Y', '1', 'Y', '2022-02-24 17:13:46', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(51, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-BK-1S', 440000, 440000, 220000, 'System backup', '2022/02/24/51/202202241715000.jpg', 'Y', '1', 'Y', '2022-02-24 17:15:00', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(52, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-BK-3M', 440000, 440000, 220000, 'System backup', '2022/02/24/52/202202241715550.jpg', 'Y', '1', 'Y', '2022-02-24 17:15:55', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(53, 'jalley', 'signature', 1, '인어팬츠', 'JA211-PT001-WH-4S', 440000, 440000, 220000, 'System backup', '2022/02/24/53/202202241716500.jpg', 'Y', '1', 'Y', '2022-02-24 17:16:50', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(54, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-WH-3M', 440000, 440000, 220000, 'System backup', '2022/02/24/54/202202241717340.jpg', 'Y', '1', 'Y', '2022-02-24 17:17:34', 'jalley', '2022-05-14 09:44:56', '2022-05-14 09:44:56'),
	(55, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-BK-2S', 440000, 440000, 220000, 'System backup', '2022/02/24/55/202202241718360.jpg', 'Y', '1', 'Y', '2022-02-24 17:18:36', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(56, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-WH-3S', 440000, 440000, 220000, 'System backup', '2022/02/24/56/202202241719240.jpg', 'Y', '1', 'Y', '2022-02-24 17:19:24', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(57, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-WH-2S', 440000, 440000, 220000, 'System backup', '2022/02/24/57/202202241720040.jpg', 'Y', '1', 'Y', '2022-02-24 17:20:04', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(58, 'jalley', 'signature', 1, '가든 드레스', 'JA21-OPS007-IV-1', 2600000, 2600000, 1300000, 'System backup', '2022/02/24/58/202202241726450.jpg', 'Y', '1', 'Y', '2022-02-24 17:26:45', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(59, 'jalley', 'signature', 1, '가든 드레스', 'JA21-OPS007-BK-1', 2600000, 2600000, 1300000, 'System backup', '2022/02/24/59/202202241727400.jpg', 'Y', '1', 'Y', '2022-02-24 17:27:40', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(60, 'jalley', 'signature', 1, '가든 슬립', 'JA21-OPS012-IV-1', 320000, 320000, 1600000, 'System backup', '2022/02/24/60/202202241728350.jpg', 'Y', '1', 'Y', '2022-02-24 17:28:35', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(61, 'jalley', 'signature', 1, '발레 블라우스', 'JA21-JK007-BK-1', 440000, 440000, 220000, 'System backup', '2022/02/24/61/202202241730420.jpg', 'Y', '1', 'Y', '2022-02-24 17:30:42', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(62, 'jalley', 'signature', 1, '발레 블라우스', 'JA21-JK007-WH-1', 440000, 440000, 220000, 'System backup', '2022/02/24/62/202202241731370.jpg', 'Y', '1', 'Y', '2022-02-24 17:31:37', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(63, 'jalley', 'signature', 1, '발레 블라우스', 'JA21-JK007-BK-2', 440000, 440000, 220000, 'System backup', '2022/02/24/63/202202241732270.jpg', 'Y', '1', 'Y', '2022-02-24 17:32:27', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(64, 'jalley', 'signature', 1, '발레 스커트', 'JA21-SK004-BK-1', 640000, 640000, 320000, 'System backup', '2022/02/24/64/202202241733290.jpg', 'Y', '1', 'Y', '2022-02-24 17:33:29', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(65, 'jalley', 'signature', 1, '발레 스커트', 'JA21-SK004-WH-1', 640000, 640000, 320000, 'System backup', '2022/02/24/65/202202241734260.jpg', 'Y', '1', 'Y', '2022-02-24 17:34:26', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(66, 'jalley', 'signature', 1, 'tret', 'dgd', 111111, 111111, 111111, 'System backup', '2022/02/25/66/202202251649530.jpg', 'Y', '1', 'Y', '2022-02-25 16:49:53', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(67, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-BK-1', 400000, 400000, 200000, 'System backup', '2022/02/28/67/202202281439070.jpg', 'Y', '1', 'Y', '2022-02-28 14:39:07', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(68, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-WH-1', 400000, 400000, 200000, 'System backup', '2022/02/28/68/202202281440280.jpg', 'Y', '1', 'Y', '2022-02-28 14:40:28', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(69, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-BK-3', 400000, 400000, 200000, 'System backup', '2022/02/28/69/202202281441200.jpg', 'Y', '1', 'Y', '2022-02-28 14:41:20', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(70, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-BK-2', 400000, 400000, 200000, 'System backup', '2022/02/28/70/202202281442100.jpg', 'Y', '1', 'Y', '2022-02-28 14:42:10', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(71, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-IV-3', 400000, 400000, 200000, 'System backup', '2022/02/28/71/202202281443270.jpg', 'Y', '1', 'Y', '2022-02-28 14:43:27', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(72, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-BK-1', 400000, 400000, 200000, 'System backup', '2022/02/28/72/202202281444200.jpg', 'Y', '1', 'Y', '2022-02-28 14:44:20', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(73, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-WH-1', 400000, 400000, 200000, 'System backup', '2022/02/28/73/202202281445030.jpg', 'Y', '1', 'Y', '2022-02-28 14:45:03', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(74, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-IV-3', 400000, 400000, 200000, 'System backup', '2022/02/28/74/202202281445510.jpg', 'Y', '1', 'Y', '2022-02-28 14:45:51', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(75, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-BK-3', 400000, 400000, 200000, 'System backup', '2022/02/28/75/202202281446400.jpg', 'Y', '1', 'Y', '2022-02-28 14:46:40', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(76, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-BK-2', 400000, 400000, 200000, 'System backup', '2022/02/28/76/202202281447230.jpg', 'Y', '1', 'Y', '2022-02-28 14:47:23', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(77, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-BK-1', 480000, 480000, 240000, 'System backup', '2022/02/28/77/202202281449020.jpg', 'Y', '1', 'Y', '2022-02-28 14:49:02', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(78, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-BK-2', 480000, 480000, 240000, 'System backup', '2022/02/28/78/202202281449560.jpg', 'Y', '1', 'Y', '2022-02-28 14:49:56', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(79, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-IV-4', 480000, 480000, 240000, 'System backup', '2022/02/28/79/202202281451130.jpg', 'Y', '1', 'Y', '2022-02-28 14:51:13', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(80, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-WH-1', 480000, 480000, 240000, 'System backup', '2022/02/28/80/202202281452070.jpg', 'Y', '1', 'Y', '2022-02-28 14:52:07', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(81, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-BK-3', 480000, 480000, 240000, 'System backup', '2022/02/28/81/202202281453050.jpg', 'Y', '1', 'Y', '2022-02-28 14:53:05', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(82, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK01-A-BK-1', 480000, 480000, 240000, 'System backup', '2022/02/28/82/202202281454200.jpg', 'Y', '1', 'Y', '2022-02-28 14:54:20', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(83, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-BK-2', 480000, 480000, 240000, 'System backup', '2022/02/28/83/202202281456240.jpg', 'Y', '1', 'Y', '2022-02-28 14:56:24', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(84, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-WH-1', 480000, 480000, 240000, 'System backup', '2022/02/28/84/202202281457380.jpg', 'Y', '1', 'Y', '2022-02-28 14:57:38', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(85, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-BK-3', 480000, 480000, 240000, 'System backup', '2022/02/28/85/202202281459270.jpg', 'Y', '1', 'Y', '2022-02-28 14:59:27', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(86, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-BK-1', 760000, 760000, 380000, 'System backup', '2022/02/28/86/202202281505490.jpg', 'Y', '1', 'Y', '2022-02-28 15:05:49', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(87, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-BK-2', 760000, 760000, 380000, 'System backup', '2022/02/28/87/202202281508470.jpg', 'Y', '1', 'Y', '2022-02-28 15:08:47', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(88, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-WH-1', 760000, 760000, 380000, 'System backup', '2022/02/28/88/202202281509420.jpg', 'Y', '1', 'Y', '2022-02-28 15:09:42', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(89, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-WH-2', 760000, 760000, 380000, 'System backup', '2022/02/28/89/202202281511000.jpg', 'Y', '1', 'Y', '2022-02-28 15:11:00', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(90, 'jalley', 'signature', 1, '베이직 팬츠 (투 버튼)', 'JA21-PT004-BK-1', 440000, 440000, 220000, 'System backup', '2022/02/28/90/202202281514280.jpg', 'Y', '1', 'Y', '2022-02-28 15:14:28', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(91, 'jalley', 'signature', 1, '베이직 팬츠 (투 버튼)', 'JA21-PT005-WH-1', 440000, 440000, 220000, 'System backup', '2022/02/28/91/202202281515160.jpg', 'Y', '1', 'Y', '2022-02-28 15:15:16', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(92, 'jalley', 'signature', 1, '스피치 자켓', 'JA21-JK013-IV-1', 840000, 840000, 420000, 'System backup', '2022/02/28/92/202202281523390.jpg', 'Y', '1', 'Y', '2022-02-28 15:23:39', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(93, 'jalley', 'signature', 1, '스피치 자켓', 'JA21-JK013-IV-2', 840000, 840000, 420000, 'System backup', '2022/02/28/93/202202281524220.jpg', 'Y', '1', 'Y', '2022-02-28 15:24:22', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(94, 'jalley', 'signature', 1, '스피치 팬츠', 'JA21-PT003-IV-1', 640000, 640000, 320000, 'System backup', '2022/02/28/94/202202281526070.jpg', 'Y', '1', 'Y', '2022-02-28 15:26:07', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(95, 'jalley', 'signature', 1, '스피치 팬츠', 'JA21-PT003-IV-2', 640000, 640000, 320000, 'System backup', '2022/02/28/95/202202281526450.jpg', 'Y', '1', 'Y', '2022-02-28 15:26:45', 'jalley', '2022-05-14 09:44:57', '2022-05-14 09:44:57'),
	(96, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-2', 920000, 920000, 460000, 'System backup', '2022/02/28/96/202202281549380.jpg', 'Y', '1', 'Y', '2022-02-28 15:49:38', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(97, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-1', 920000, 920000, 460000, 'System backup', '2022/02/28/97/202202281551390.jpg', 'Y', '1', 'Y', '2022-02-28 15:51:39', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(98, 'jalley', 'signature', 1, '엘리 케이프 롱 코트', 'JA21-JK010-IV-1', 720000, 720000, 360000, 'System backup', '2022/02/28/98/202202281553310.jpg', 'Y', '1', 'Y', '2022-02-28 15:53:31', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(99, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-IV-1', 920000, 920000, 460000, 'System backup', '2022/02/28/99/202202281554200.jpg', 'Y', '1', 'Y', '2022-02-28 15:54:20', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(100, 'jalley', 'signature', 1, '플라워 드레스', 'JA21-OPS001-BK-1', 1160000, 1160000, 580000, 'System backup', '2022/02/28/100/202202281556430.jpg', 'Y', '1', 'Y', '2022-02-28 15:56:43', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(101, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS04-BK-1', 1200000, 1200000, 600000, 'System backup', '2022/02/28/101/202202281559420.jpg', 'Y', '1', 'Y', '2022-02-28 15:59:42', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(102, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS004-BK-4', 1200000, 1200000, 600000, 'System backup', '2022/02/28/102/202202281600410.jpg', 'Y', '1', 'Y', '2022-02-28 16:00:41', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(103, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS004-BK-3', 1200000, 1200000, 1200000, 'System backup', '2022/02/28/103/202202281601290.jpg', 'Y', '1', 'Y', '2022-02-28 16:01:29', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(104, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-BK-3', 920000, 920000, 460000, 'System backup', '2022/02/28/104/202202281602230.jpg', 'Y', '1', 'Y', '2022-02-28 16:02:23', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(105, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS0005-RD-4', 920000, 920000, 460000, 'System backup', '2022/02/28/105/202202281603230.jpg', 'Y', '1', 'Y', '2022-02-28 16:03:23', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(106, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-BK-4', 920000, 920000, 460000, 'System backup', '2022/02/28/106/202202281604160.jpg', 'Y', '1', 'Y', '2022-02-28 16:04:16', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(107, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-RD-3', 920000, 920000, 460000, 'System backup', '2022/02/28/107/202202281605040.jpg', 'Y', '1', 'Y', '2022-02-28 16:05:04', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(108, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-BK-2', 920000, 920000, 460000, 'System backup', '2022/02/28/108/202202281605390.jpg', 'Y', '1', 'Y', '2022-02-28 16:05:39', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(109, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-RD-2', 920000, 920000, 460000, 'System backup', '2022/02/28/109/202202281606090.jpg', 'Y', '1', 'Y', '2022-02-28 16:06:09', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(110, 'jalley', 'signature', 1, '벨 드레스', 'JA21-OPS009-BC-1', 1160000, 1160000, 580000, 'System backup', '2022/02/28/110/202202281607510.jpg', 'Y', '1', 'Y', '2022-02-28 16:07:51', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(111, 'jalley', 'signature', 1, '벨 드레스', 'JA21-OPS009-BC-2', 1160000, 1160000, 580000, 'System backup', '2022/02/28/111/202202281608340.jpg', 'Y', '1', 'Y', '2022-02-28 16:08:34', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(112, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WINE-2', 1400000, 1400000, 700000, 'System backup', '2022/02/28/112/202202281610470.jpg', 'Y', '1', 'Y', '2022-02-28 16:10:47', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(113, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WH-2', 1400000, 1400000, 700000, 'System backup', '2022/02/28/113/202202281611480.jpg', 'Y', '1', 'Y', '2022-02-28 16:11:48', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(114, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WH-1', 1400000, 1400000, 700000, 'System backup', '2022/02/28/114/202202281612490.jpg', 'Y', '1', 'Y', '2022-02-28 16:12:49', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(115, 'jalley', 'signature', 1, '메릴 드레스', 'JA21-OPS013-IV-1', 960000, 960000, 480000, 'System backup', '2022/02/28/115/202202281613500.jpg', 'Y', '1', 'Y', '2022-02-28 16:13:50', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(116, 'jalley', 'signature', 1, '메릴 드레스', 'JA21-OPS013-PU-1', 960000, 960000, 480000, 'System backup', '2022/02/28/116/202202281614380.jpg', 'Y', '1', 'Y', '2022-02-28 16:14:38', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(117, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-BK-1', 1400000, 1400000, 700000, 'System backup', '2022/02/28/117/202202281615580.jpg', 'Y', '1', 'Y', '2022-02-28 16:15:58', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 1400000, 700000, 'System backup', '2022/02/28/118/202202281617130.jpg', 'Y', '1', 'Y', '2022-02-28 16:17:13', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(119, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-BK-2', 1320000, 1320000, 660000, 'System backup', '2022/02/28/119/202202281618160.jpg', 'Y', '1', 'Y', '2022-02-28 16:18:16', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(120, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-PK-1', 1320000, 1320000, 660000, 'System backup', '2022/02/28/120/202202281619200.jpg', 'Y', '1', 'Y', '2022-02-28 16:19:20', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(121, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-WH-1', 1320000, 1320000, 660000, 'System backup', '2022/02/28/121/202202281620470.jpg', 'Y', '1', 'Y', '2022-02-28 16:20:47', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(122, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-BC-2', 1320000, 1320000, 660000, 'System backup', '2022/02/28/122/202202281622050.jpg', 'Y', '1', 'Y', '2022-02-28 16:22:05', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(123, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-WH-2', 1320000, 1320000, 660000, 'System backup', '2022/02/28/123/202202281622570.jpg', 'Y', '1', 'Y', '2022-02-28 16:22:57', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(124, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-IV-2', 1080000, 1080000, 540000, 'System backup', '2022/02/28/124/202202281625270.jpg', 'Y', '1', 'Y', '2022-02-28 16:25:27', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(125, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-3', 1080000, 1080000, 540000, 'System backup', '2022/02/28/125/202202281626110.jpg', 'Y', '1', 'Y', '2022-02-28 16:26:11', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(126, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-1', 1080000, 1080000, 540000, 'System backup', '2022/02/28/126/202202281627300.jpg', 'Y', '1', 'Y', '2022-02-28 16:27:30', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(127, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-4', 1080000, 1080000, 540000, 'System backup', '2022/02/28/127/202202281628050.jpg', 'Y', '1', 'Y', '2022-02-28 16:28:05', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(128, 'jalley', 'signature', 1, '담리 드레스(2)', 'JA21-OPS002-A-IV-1', 1080000, 1080000, 540000, 'System backup', '2022/02/28/128/202202281632130.jpg', 'Y', '1', 'Y', '2022-02-28 16:32:13', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(129, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-2', 1360000, 1360000, 680000, 'System backup', '2022/02/28/129/202202281633170.jpg', 'Y', '1', 'Y', '2022-02-28 16:33:17', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(130, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-3', 1360000, 1360000, 680000, 'System backup', '2022/02/28/130/202202281634010.jpg', 'Y', '1', 'Y', '2022-02-28 16:34:01', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(131, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-1', 1360000, 1360000, 680000, 'System backup', '2022/02/28/131/202202281634550.jpg', 'Y', '1', 'Y', '2022-02-28 16:34:55', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 1360000, 680000, 'System backup', '2022/02/28/132/202202281635530.jpg', 'Y', '1', 'Y', '2022-02-28 16:35:53', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(133, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-1', 720000, 720000, 360000, 'System backup', '2022/02/28/133/202202281637570.jpg', 'Y', '1', 'Y', '2022-02-28 16:37:57', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(134, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-3', 760000, 760000, 380000, 'System backup', '2022/03/02/134/202203021622140.jpg', 'Y', '1', 'Y', '2022-03-02 16:22:14', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(135, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-1', 760000, 760000, 380000, 'System backup', '2022/03/02/135/202203021644290.jpg', 'Y', '1', 'Y', '2022-03-02 16:44:29', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(136, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-3', 760000, 760000, 380000, 'System backup', '2022/03/02/136/202203021645390.jpg', 'Y', '1', 'Y', '2022-03-02 16:45:39', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(137, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-2', 760000, 760000, 380000, 'System backup', '2022/03/02/137/202203021657350.jpg', 'Y', '1', 'Y', '2022-03-02 16:57:35', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(138, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-1', 760000, 760000, 380000, 'System backup', '2022/03/02/138/202203021658330.jpg', 'Y', '1', 'Y', '2022-03-02 16:58:33', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(139, 'jalley', 'signature', 1, 'BM자켓 드레스(폴리)', 'JA22-OPS014-WH-1', 760000, 760000, 380000, 'System backup', '2022/03/02/139/202203021707260.jpg', 'Y', '1', 'Y', '2022-03-02 17:07:26', 'jalley', '2022-05-14 09:44:58', '2022-05-14 09:44:58'),
	(140, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-1', 760000, 760000, 380000, 'System backup', '2022/03/02/140/202203021720270.jpg', 'Y', '1', 'Y', '2022-03-02 17:20:27', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 440000, 220000, 'System backup', '2022/03/03/141/202203031419250.jpg', 'Y', '1', 'Y', '2022-03-03 14:19:25', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(142, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-4', 760000, 760000, 380000, 'System backup', '2022/03/03/142/202203031547470.jpg', 'Y', '1', 'Y', '2022-03-03 15:47:47', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(143, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA222-OPS014-WH-2', 760000, 760000, 380000, 'System backup', '2022/03/03/143/202203031549160.jpg', 'Y', '1', 'Y', '2022-03-03 15:49:16', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(144, 'jalley', 'signature', 1, 'BM 자켓 드레스(트위드)', 'JA22-OPS014-A-GOLD-1', 760000, 760000, 380000, 'System backup', '2022/03/03/144/202203031551060.jpg', 'Y', '1', 'Y', '2022-03-03 15:51:06', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(145, 'jalley', 'signature', 1, 'BM 자켓 드레스(트위드)', 'JA22-OPS014-A-BK-1', 760000, 760000, 380000, 'System backup', '2022/03/03/145/202203031552200.jpg', 'Y', '1', 'Y', '2022-03-03 15:52:20', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(146, 'jalley', 'signature', 1, '와인 자켓', 'JA22-JK015-BK-1', 800000, 800000, 40000, 'System backup', '2022/03/03/146/202203031554030.jpg', 'Y', '1', 'Y', '2022-03-03 15:54:03', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(147, 'jalley', 'signature', 1, '와인 팬츠', 'JA22-PT007-BK-1', 440000, 440000, 220000, 'System backup', '2022/03/03/147/202203031557580.jpg', 'Y', '1', 'Y', '2022-03-03 15:57:58', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(148, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-1', 760000, 760000, 380000, 'System backup', '2022/03/03/148/202203031611280.jpg', 'Y', '1', 'Y', '2022-03-03 16:11:28', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(149, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-2', 760000, 760000, 380000, 'System backup', '2022/03/07/149/202203071539120.jpg', 'Y', '1', 'Y', '2022-03-07 15:39:12', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(150, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-BK-1', 760000, 760000, 380000, 'System backup', '2022/03/07/150/202203071540160.jpg', 'Y', '1', 'Y', '2022-03-07 15:40:16', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(151, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA222-JK015-BK-2', 760000, 760000, 380000, 'System backup', '2022/03/07/151/202203071541010.jpg', 'Y', '1', 'Y', '2022-03-07 15:41:01', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(152, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-3', 760000, 760000, 380000, 'System backup', '2022/03/07/152/202203071541590.jpg', 'Y', '1', 'Y', '2022-03-07 15:41:59', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(153, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-2', 760000, 760000, 380000, 'System backup', '2022/03/07/153/202203071542460.jpg', 'Y', '1', 'Y', '2022-03-07 15:42:46', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(154, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-2', 760000, 760000, 380000, 'System backup', '2022/03/07/154/202203071544320.jpg', 'Y', '1', 'Y', '2022-03-07 15:44:32', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 760000, 380000, 'System backup', '2022/03/07/155/202203071550480.jpg', 'Y', '1', 'Y', '2022-03-07 15:50:48', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(156, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-1', 760000, 760000, 380000, 'System backup', '2022/03/07/156/202203071551360.jpg', 'Y', '1', 'Y', '2022-03-07 15:51:36', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(157, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-1', 760000, 760000, 380000, 'System backup', '2022/03/07/157/202203071552470.jpg', 'Y', '1', 'Y', '2022-03-07 15:52:47', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 760000, 380000, 'System backup', '2022/03/07/158/202203071553560.jpg', 'Y', '1', 'Y', '2022-03-07 15:53:56', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(159, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-3', 760000, 760000, 380000, 'System backup', '2022/03/07/159/202203071555520.jpg', 'Y', '1', 'Y', '2022-03-07 15:55:52', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 680000, 340000, 'System backup', '2022/03/07/160/202203071557570.jpg', 'Y', '1', 'Y', '2022-03-07 15:57:57', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(161, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-PK-1', 640000, 640000, 340000, 'System backup', '2022/03/07/161/202203071558400.jpg', 'Y', '1', 'Y', '2022-03-07 15:58:40', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(162, 'jalley', 'signature', 1, '허니 레이스 자켓', 'JA22-JK016-A-GD-1S', 840000, 840000, 420000, 'System backup', '2022/03/07/162/202203071559580.jpg', 'Y', '1', 'Y', '2022-03-07 15:59:58', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 480000, 240000, 'System backup', '2022/03/07/163/202203071601240.jpg', 'Y', '1', 'Y', '2022-03-07 16:01:24', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 480000, 240000, 'System backup', '2022/03/07/164/202203071602300.jpg', 'Y', '1', 'Y', '2022-03-07 16:02:30', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 480000, 240000, 'System backup', '2022/03/07/165/202203071603180.jpg', 'Y', '1', 'Y', '2022-03-07 16:03:18', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(166, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-PK-1', 480000, 480000, 240000, 'System backup', '2022/03/07/166/202203071604020.jpg', 'Y', '1', 'Y', '2022-03-07 16:04:02', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(167, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-BK-1', 760000, 760000, 380000, 'System backup', '2022/03/07/167/202203071608500.jpg', 'Y', '1', 'Y', '2022-03-07 16:08:50', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(168, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-WH-1', 760000, 760000, 380000, 'System backup', '2022/03/07/168/202203071610150.jpg', 'Y', '1', 'Y', '2022-03-07 16:10:15', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(169, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-BK-1', 440000, 440000, 220000, 'System backup', '2022/03/07/169/202203071611170.jpg', 'Y', '1', 'Y', '2022-03-07 16:11:17', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 440000, 220000, 'System backup', '2022/03/07/170/202203071612090.jpg', 'Y', '1', 'Y', '2022-03-07 16:12:09', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 440000, 220000, 'System backup', '2022/03/07/171/202203071612490.jpg', 'Y', '4', 'Y', '2022-03-07 16:12:49', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(172, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-BK-2', 440000, 440000, 220000, 'System backup', '2022/03/07/172/202203071613340.jpg', 'Y', '1', 'Y', '2022-03-07 16:13:34', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(173, 'jalley', 'signature', 1, 'BM-P 드레스', 'JA22-OPS016-BK-1', 760000, 760000, 380000, 'System backup', '2022/03/10/173/202203101546530.jpg', 'Y', '1', 'Y', '2022-03-10 15:46:53', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(174, 'jalley', 'signature', 1, 'BM-P드레스', 'JA22-OPS016', 760000, 760000, 380000, 'System backup', '2022/03/10/174/202203101550510.jpg', 'Y', '1', 'Y', '2022-03-10 15:50:51', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(175, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-WH-1', 360000, 360000, 180000, 'System backup', '2022/03/10/175/202203101552180.jpg', 'Y', '1', 'Y', '2022-03-10 15:52:18', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(176, 'jalley', 'signature', 1, 'BM 스커트', 'JA22-SK007-WH-2', 360000, 360000, 180000, 'System backup', '2022/03/10/176/202203101553210.jpg', 'Y', '1', 'Y', '2022-03-10 15:53:21', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(177, 'jalley', 'signature', 1, 'BM 스커트', 'JA22-SK007-BK-1', 360000, 360000, 180000, 'System backup', '2022/03/10/177/202203101554110.jpg', 'Y', '1', 'Y', '2022-03-10 15:54:11', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(178, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-BK-2', 360000, 360000, 180000, 'System backup', '2022/03/10/178/202203101555250.jpg', 'Y', '1', 'Y', '2022-03-10 15:55:25', 'jalley', '2022-05-14 09:44:59', '2022-05-14 09:44:59'),
	(179, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-IK014-GR-1', 760000, 760000, 380000, 'System backup', '2022/03/10/179/202203101557210.jpg', 'Y', '1', 'Y', '2022-03-10 15:57:21', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(180, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-GR-2', 760000, 760000, 380000, 'System backup', '2022/03/10/180/202203101558220.jpg', 'Y', '1', 'Y', '2022-03-10 15:58:22', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(181, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-1', 440000, 440000, 220000, 'System backup', '2022/03/10/181/202203101559530.jpg', 'Y', '1', 'Y', '2022-03-10 15:59:53', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 440000, 220000, 'System backup', '2022/03/10/182/202203101600290.jpg', 'Y', '1', 'Y', '2022-03-10 16:00:29', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(183, 'jalley', 'signature', 1, '허니 자켓', 'ja22-jk016-wh-3s', 680000, 680000, 340000, 'System backup', '2022/03/31/183/202203311720000.jpg', 'Y', '1', 'Y', '2022-03-31 17:20:00', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(184, 'jalley', 'signature', 1, '허니자켓', 'ja22-jk016-wh-m4', 680000, 680000, 420000, 'System backup', '2022/03/31/184/202203311721000.jpg', 'Y', '1', 'Y', '2022-03-31 17:21:00', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(185, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-1', 440000, 440000, 220000, 'System backup', '2022/03/31/185/202203311724040.jpg', 'Y', '1', 'Y', '2022-03-31 17:24:04', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(186, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-2', 440000, 440000, 220000, 'System backup', '2022/03/31/186/202203311725060.jpg', 'Y', '1', 'Y', '2022-03-31 17:25:06', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(187, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-3', 440000, 440000, 220000, 'System backup', '2022/03/31/187/202203311725480.jpg', 'Y', '1', 'Y', '2022-03-31 17:25:48', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(188, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-1', 440000, 440000, 220000, 'System backup', '2022/03/31/188/202203311726420.jpg', 'Y', '1', 'Y', '2022-03-31 17:26:42', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(189, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-2', 440000, 440000, 220000, 'System backup', '2022/03/31/189/202203311727250.jpg', 'Y', '1', 'Y', '2022-03-31 17:27:25', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(190, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-3', 440000, 440000, 220000, 'System backup', '2022/03/31/190/202203311728080.jpg', 'Y', '1', 'Y', '2022-03-31 17:28:08', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(191, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-1', 440000, 440000, 220000, 'System backup', '2022/03/31/191/202203311728460.jpg', 'Y', '1', 'Y', '2022-03-31 17:28:46', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 440000, 220000, 'System backup', '2022/03/31/192/202203311730040.jpg', 'Y', '1', 'Y', '2022-03-31 17:30:04', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(193, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-3', 440000, 440000, 220000, 'System backup', '2022/03/31/193/202203311731050.jpg', 'Y', '1', 'Y', '2022-03-31 17:31:05', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(194, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-1', 2000000, 2000000, 1000000, 'System backup', '2022/04/04/194/202204041445510.jpg', 'Y', '1', 'Y', '2022-04-04 14:45:51', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(195, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-2', 2000000, 2000000, 1000000, 'System backup', '2022/04/04/195/202204041446320.jpg', 'Y', '1', 'Y', '2022-04-04 14:46:32', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 2000000, 1000000, 'System backup', '2022/04/04/196/202204041447230.jpg', 'Y', '1', 'Y', '2022-04-04 14:47:23', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 2000000, 2000000, 1000000, 'System backup', '2022/04/04/197/202204041448090.jpg', 'Y', '4', 'Y', '2022-04-04 14:48:09', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(198, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-2', 2000000, 2000000, 1000000, 'System backup', '2022/04/04/198/202204041448450.jpg', 'Y', '1', 'Y', '2022-04-04 14:48:45', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(199, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-1', 1440000, 1440000, 720000, 'System backup', '2022/04/04/199/202204041450170.jpg', 'Y', '1', 'Y', '2022-04-04 14:50:17', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(200, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-2', 1440000, 1440000, 720000, 'System backup', '2022/04/04/200/202204041451140.jpg', 'Y', '1', 'Y', '2022-04-04 14:51:14', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 1440000, 720000, 'System backup', '2022/04/04/201/202204041452040.jpg', 'Y', '1', 'Y', '2022-04-04 14:52:04', 'jalley', '2022-06-06 00:00:00', '2022-06-06 00:00:00'),
	(202, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-2', 1440000, 1440000, 720000, 'System backup 1', '2022/04/04/202/202204041452500.jpg', 'Y', '2', 'Y', '2022-04-04 14:52:50', 'jalley', '2022-05-14 09:45:00', '2022-05-14 09:45:00'),
	(203, 'emp10', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 1440000, 720000, 'System backup', '2022/04/04/203/202204041457180.jpg', 'Y', '2', 'Y', '2022-04-04 14:57:18', 'jalley', '2022-06-09 00:00:00', '2022-06-09 00:00:00');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.product_backup
DROP TABLE IF EXISTS `product_backup`;
CREATE TABLE IF NOT EXISTS `product_backup` (
  `product_backup_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '상품백업아이디',
  `work_type` varchar(45) NOT NULL COMMENT '작업구분',
  `product_id` bigint(20) NOT NULL COMMENT '상품아이디',
  `store_id` varchar(45) NOT NULL COMMENT '상점아이디',
  `product_type` varchar(45) NOT NULL COMMENT '상품종류',
  `cnt` int(11) NOT NULL COMMENT '수량',
  `product_name` varchar(200) NOT NULL COMMENT '상품명',
  `serial_num` varchar(100) NOT NULL COMMENT '시리얼넘버',
  `price` bigint(20) NOT NULL COMMENT '가격',
  `memo` varchar(45) DEFAULT NULL COMMENT '메모',
  `display_yn` varchar(1) NOT NULL COMMENT '상품노출여부',
  `use_yn` varchar(1) NOT NULL COMMENT '사용여부',
  `change_date` datetime NOT NULL COMMENT '변경일',
  `change_id` varchar(32) NOT NULL COMMENT '변경아이디',
  PRIMARY KEY (`product_backup_id`),
  KEY `FK_product_backup_product_id_product_product_id` (`product_id`),
  CONSTRAINT `FK_product_backup_product_id_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=777 DEFAULT CHARSET=utf8mb3 COMMENT='상품변경로그';

-- Dumping data for table jalley_auction.product_backup: ~724 rows (approximately)
DELETE FROM `product_backup`;
/*!40000 ALTER TABLE `product_backup` DISABLE KEYS */;
INSERT INTO `product_backup` (`product_backup_id`, `work_type`, `product_id`, `store_id`, `product_type`, `cnt`, `product_name`, `serial_num`, `price`, `memo`, `display_yn`, `use_yn`, `change_date`, `change_id`) VALUES
	(1, 'insert', 1, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-1S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:16:14', 'jalley'),
	(2, 'insert', 2, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-5M', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:27:37', 'jalley'),
	(3, 'insert', 3, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-PK-2S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:29:12', 'jalley'),
	(4, 'insert', 4, 'jalley', 'signature', 1, '스타자켓', 'JA21-jk011-PK-3M', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:30:17', 'jalley'),
	(5, 'insert', 5, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-1S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:51:52', 'jalley'),
	(6, 'insert', 6, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-4M', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:53:02', 'jalley'),
	(7, 'insert', 7, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-2S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:54:34', 'jalley'),
	(8, 'insert', 8, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-4S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:55:50', 'jalley'),
	(9, 'insert', 9, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-2S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:56:39', 'jalley'),
	(10, 'insert', 10, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-3M', 880000, 'System backup', 'Y', 'Y', '2022-02-24 15:57:38', 'jalley'),
	(11, 'insert', 11, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-1S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:02:24', 'jalley'),
	(12, 'insert', 12, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-2M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:04:54', 'jalley'),
	(13, 'insert', 13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:05:54', 'jalley'),
	(14, 'insert', 14, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-4M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:07:02', 'jalley'),
	(15, 'insert', 15, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-3M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:09:43', 'jalley'),
	(16, 'insert', 16, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-1S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:31:25', 'jalley'),
	(17, 'insert', 17, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:32:13', 'jalley'),
	(18, 'insert', 18, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-3S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:32:56', 'jalley'),
	(19, 'insert', 19, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-2S', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:34:03', 'jalley'),
	(20, 'insert', 20, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-3M', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:34:52', 'jalley'),
	(21, 'insert', 21, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-1S', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:35:40', 'jalley'),
	(22, 'insert', 22, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-4M', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:36:31', 'jalley'),
	(23, 'insert', 23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:37:37', 'jalley'),
	(24, 'insert', 24, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-4M', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:38:29', 'jalley'),
	(25, 'insert', 25, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-3S', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:39:27', 'jalley'),
	(26, 'insert', 26, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-3S', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:39:27', 'jalley'),
	(27, 'insert', 27, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-1S', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:40:08', 'jalley'),
	(28, 'insert', 28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:41:03', 'jalley'),
	(29, 'insert', 29, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK0054-WH-1M', 280000, 'System backup', 'Y', 'Y', '2022-02-24 16:41:43', 'jalley'),
	(30, 'insert', 30, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-4M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:45:51', 'jalley'),
	(31, 'insert', 31, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-1S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:48:19', 'jalley'),
	(32, 'insert', 32, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-3M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:50:09', 'jalley'),
	(33, 'insert', 33, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-1S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:50:57', 'jalley'),
	(34, 'insert', 34, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-3M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 16:51:41', 'jalley'),
	(35, 'insert', 35, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-1S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 16:55:21', 'jalley'),
	(36, 'insert', 36, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-2M', 880000, 'System backup', 'Y', 'Y', '2022-02-24 16:56:07', 'jalley'),
	(37, 'insert', 37, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-WH-1S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 16:56:52', 'jalley'),
	(38, 'insert', 38, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-3S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 16:58:17', 'jalley'),
	(39, 'insert', 39, 'jalley', 'signature', 1, '인어 자켓', 'JA21-JK008-WH-2S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 16:59:14', 'jalley'),
	(40, 'insert', 40, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-WH-3S', 880000, 'System backup', 'Y', 'Y', '2022-02-24 16:59:52', 'jalley'),
	(41, 'insert', 41, 'jalley', 'signature', 1, '인어 케이프', 'JA21-JK006-WH-1', 720000, 'System backup', 'Y', 'Y', '2022-02-24 17:03:34', 'jalley'),
	(42, 'insert', 42, 'jalley', 'signature', 1, '인어 케이프', 'JA21-JK006-WH-2', 720000, 'System backup', 'Y', 'Y', '2022-02-24 17:04:44', 'jalley'),
	(43, 'insert', 43, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-BK-2S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:05:56', 'jalley'),
	(44, 'insert', 44, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-BK-2M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:06:58', 'jalley'),
	(45, 'insert', 45, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-WH-1S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:09:28', 'jalley'),
	(46, 'insert', 46, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-WH-2M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:10:41', 'jalley'),
	(47, 'insert', 47, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-BK-1S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:11:25', 'jalley'),
	(48, 'insert', 48, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-WH-3S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:12:10', 'jalley'),
	(49, 'insert', 49, 'jalley', 'signature', 1, '인어 와이드 팬츠', 'JA21-PT007-WH-1', 600000, 'System backup', 'Y', 'Y', '2022-02-24 17:13:04', 'jalley'),
	(50, 'insert', 50, 'jalley', 'signature', 1, '인어 와이드 팬츠', 'JA21-PT007-WH-2', 600000, 'System backup', 'Y', 'Y', '2022-02-24 17:13:46', 'jalley'),
	(51, 'insert', 51, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-BK-1S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:15:00', 'jalley'),
	(52, 'insert', 52, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-BK-3M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:15:55', 'jalley'),
	(53, 'insert', 53, 'jalley', 'signature', 1, '인어팬츠', 'JA211-PT001-WH-4S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:16:50', 'jalley'),
	(54, 'insert', 54, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-WH-3M', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:17:34', 'jalley'),
	(55, 'insert', 55, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-BK-2S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:18:36', 'jalley'),
	(56, 'insert', 56, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-WH-3S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:19:24', 'jalley'),
	(57, 'insert', 57, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-WH-2S', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:20:04', 'jalley'),
	(58, 'insert', 58, 'jalley', 'signature', 1, '가든 드레스', 'JA21-OPS007-IV-1', 2600000, 'System backup', 'Y', 'Y', '2022-02-24 17:26:45', 'jalley'),
	(59, 'insert', 59, 'jalley', 'signature', 1, '가든 드레스', 'JA21-OPS007-BK-1', 2600000, 'System backup', 'Y', 'Y', '2022-02-24 17:27:40', 'jalley'),
	(60, 'insert', 60, 'jalley', 'signature', 1, '가든 슬립', 'JA21-OPS012-IV-1', 320000, 'System backup', 'Y', 'Y', '2022-02-24 17:28:35', 'jalley'),
	(61, 'insert', 61, 'jalley', 'signature', 1, '발레 블라우스', 'JA21-JK007-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:30:42', 'jalley'),
	(62, 'insert', 62, 'jalley', 'signature', 1, '발레 블라우스', 'JA21-JK007-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:31:37', 'jalley'),
	(63, 'insert', 63, 'jalley', 'signature', 1, '발레 블라우스', 'JA21-JK007-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-02-24 17:32:27', 'jalley'),
	(64, 'insert', 64, 'jalley', 'signature', 1, '발레 스커트', 'JA21-SK004-BK-1', 640000, 'System backup', 'Y', 'Y', '2022-02-24 17:33:29', 'jalley'),
	(65, 'insert', 65, 'jalley', 'signature', 1, '발레 스커트', 'JA21-SK004-WH-1', 640000, 'System backup', 'Y', 'Y', '2022-02-24 17:34:26', 'jalley'),
	(66, 'insert', 66, 'jalley', 'signature', 1, 'tret', 'dgd', 111111, 'System backup', 'Y', 'Y', '2022-02-25 16:49:53', 'jalley'),
	(67, 'insert', 67, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-BK-1', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:39:07', 'jalley'),
	(68, 'insert', 68, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-WH-1', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:40:28', 'jalley'),
	(69, 'insert', 69, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-BK-3', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:41:20', 'jalley'),
	(70, 'insert', 70, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-BK-2', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:42:10', 'jalley'),
	(71, 'insert', 71, 'jalley', 'signature', 1, '샤잉 블라우스', 'JA21-JK001-IV-3', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:43:27', 'jalley'),
	(72, 'insert', 72, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-BK-1', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:44:20', 'jalley'),
	(73, 'insert', 73, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-WH-1', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:45:03', 'jalley'),
	(74, 'insert', 74, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-IV-3', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:45:51', 'jalley'),
	(75, 'insert', 75, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-BK-3', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:46:40', 'jalley'),
	(76, 'insert', 76, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-BK-2', 400000, 'System backup', 'Y', 'Y', '2022-02-28 14:47:23', 'jalley'),
	(77, 'insert', 77, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-BK-1', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:49:02', 'jalley'),
	(78, 'insert', 78, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-BK-2', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:49:56', 'jalley'),
	(79, 'insert', 79, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-IV-4', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:51:13', 'jalley'),
	(80, 'insert', 80, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:52:07', 'jalley'),
	(81, 'insert', 81, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-BK-3', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:53:05', 'jalley'),
	(82, 'insert', 82, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK01-A-BK-1', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:54:20', 'jalley'),
	(83, 'insert', 83, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-BK-2', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:56:24', 'jalley'),
	(84, 'insert', 84, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:57:38', 'jalley'),
	(85, 'insert', 85, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-BK-3', 480000, 'System backup', 'Y', 'Y', '2022-02-28 14:59:27', 'jalley'),
	(86, 'insert', 86, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-02-28 15:05:49', 'jalley'),
	(87, 'insert', 87, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-02-28 15:08:47', 'jalley'),
	(88, 'insert', 88, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-02-28 15:09:42', 'jalley'),
	(89, 'insert', 89, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-02-28 15:11:00', 'jalley'),
	(90, 'insert', 90, 'jalley', 'signature', 1, '베이직 팬츠 (투 버튼)', 'JA21-PT004-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-02-28 15:14:28', 'jalley'),
	(91, 'insert', 91, 'jalley', 'signature', 1, '베이직 팬츠 (투 버튼)', 'JA21-PT005-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-02-28 15:15:16', 'jalley'),
	(92, 'insert', 92, 'jalley', 'signature', 1, '스피치 자켓', 'JA21-JK013-IV-1', 840000, 'System backup', 'Y', 'Y', '2022-02-28 15:23:39', 'jalley'),
	(93, 'insert', 93, 'jalley', 'signature', 1, '스피치 자켓', 'JA21-JK013-IV-2', 840000, 'System backup', 'Y', 'Y', '2022-02-28 15:24:22', 'jalley'),
	(94, 'insert', 94, 'jalley', 'signature', 1, '스피치 팬츠', 'JA21-PT003-IV-1', 640000, 'System backup', 'Y', 'Y', '2022-02-28 15:26:07', 'jalley'),
	(95, 'insert', 95, 'jalley', 'signature', 1, '스피치 팬츠', 'JA21-PT003-IV-2', 640000, 'System backup', 'Y', 'Y', '2022-02-28 15:26:45', 'jalley'),
	(96, 'insert', 96, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-2', 920000, 'System backup', 'Y', 'Y', '2022-02-28 15:49:38', 'jalley'),
	(97, 'insert', 97, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-1', 920000, 'System backup', 'Y', 'Y', '2022-02-28 15:51:39', 'jalley'),
	(98, 'insert', 98, 'jalley', 'signature', 1, '엘리 케이프 롱 코트', 'JA21-JK010-IV-1', 720000, 'System backup', 'Y', 'Y', '2022-02-28 15:53:31', 'jalley'),
	(99, 'insert', 99, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-IV-1', 920000, 'System backup', 'Y', 'Y', '2022-02-28 15:54:20', 'jalley'),
	(100, 'insert', 100, 'jalley', 'signature', 1, '플라워 드레스', 'JA21-OPS001-BK-1', 1160000, 'System backup', 'Y', 'Y', '2022-02-28 15:56:43', 'jalley'),
	(101, 'insert', 101, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS04-BK-1', 1200000, 'System backup', 'Y', 'Y', '2022-02-28 15:59:42', 'jalley'),
	(102, 'insert', 102, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS004-BK-4', 1200000, 'System backup', 'Y', 'Y', '2022-02-28 16:00:41', 'jalley'),
	(103, 'insert', 103, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS004-BK-3', 1200000, 'System backup', 'Y', 'Y', '2022-02-28 16:01:29', 'jalley'),
	(104, 'insert', 104, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-BK-3', 920000, 'System backup', 'Y', 'Y', '2022-02-28 16:02:23', 'jalley'),
	(105, 'insert', 105, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS0005-RD-4', 920000, 'System backup', 'Y', 'Y', '2022-02-28 16:03:23', 'jalley'),
	(106, 'insert', 106, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-BK-4', 920000, 'System backup', 'Y', 'Y', '2022-02-28 16:04:16', 'jalley'),
	(107, 'insert', 107, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-RD-3', 920000, 'System backup', 'Y', 'Y', '2022-02-28 16:05:04', 'jalley'),
	(108, 'insert', 108, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-BK-2', 920000, 'System backup', 'Y', 'Y', '2022-02-28 16:05:39', 'jalley'),
	(109, 'insert', 109, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-RD-2', 920000, 'System backup', 'Y', 'Y', '2022-02-28 16:06:09', 'jalley'),
	(110, 'insert', 110, 'jalley', 'signature', 1, '벨 드레스', 'JA21-OPS009-BC-1', 1160000, 'System backup', 'Y', 'Y', '2022-02-28 16:07:51', 'jalley'),
	(111, 'insert', 111, 'jalley', 'signature', 1, '벨 드레스', 'JA21-OPS009-BC-2', 1160000, 'System backup', 'Y', 'Y', '2022-02-28 16:08:34', 'jalley'),
	(112, 'insert', 112, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WINE-2', 1400000, 'System backup', 'Y', 'Y', '2022-02-28 16:10:47', 'jalley'),
	(113, 'insert', 113, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WH-2', 1400000, 'System backup', 'Y', 'Y', '2022-02-28 16:11:48', 'jalley'),
	(114, 'insert', 114, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-02-28 16:12:49', 'jalley'),
	(115, 'insert', 115, 'jalley', 'signature', 1, '메릴 드레스', 'JA21-OPS013-IV-1', 960000, 'System backup', 'Y', 'Y', '2022-02-28 16:13:50', 'jalley'),
	(116, 'insert', 116, 'jalley', 'signature', 1, '메릴 드레스', 'JA21-OPS013-PU-1', 960000, 'System backup', 'Y', 'Y', '2022-02-28 16:14:38', 'jalley'),
	(117, 'insert', 117, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-BK-1', 1400000, 'System backup', 'Y', 'Y', '2022-02-28 16:15:58', 'jalley'),
	(118, 'insert', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-02-28 16:17:13', 'jalley'),
	(119, 'insert', 119, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-BK-2', 1320000, 'System backup', 'Y', 'Y', '2022-02-28 16:18:16', 'jalley'),
	(120, 'insert', 120, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-PK-1', 1320000, 'System backup', 'Y', 'Y', '2022-02-28 16:19:20', 'jalley'),
	(121, 'insert', 121, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-WH-1', 1320000, 'System backup', 'Y', 'Y', '2022-02-28 16:20:47', 'jalley'),
	(122, 'insert', 122, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-BC-2', 1320000, 'System backup', 'Y', 'Y', '2022-02-28 16:22:05', 'jalley'),
	(123, 'insert', 123, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-WH-2', 1320000, 'System backup', 'Y', 'Y', '2022-02-28 16:22:57', 'jalley'),
	(124, 'insert', 124, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-IV-2', 1080000, 'System backup', 'Y', 'Y', '2022-02-28 16:25:27', 'jalley'),
	(125, 'insert', 125, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-3', 1080000, 'System backup', 'Y', 'Y', '2022-02-28 16:26:11', 'jalley'),
	(126, 'insert', 126, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-1', 1080000, 'System backup', 'Y', 'Y', '2022-02-28 16:27:30', 'jalley'),
	(127, 'insert', 127, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-4', 1080000, 'System backup', 'Y', 'Y', '2022-02-28 16:28:05', 'jalley'),
	(128, 'insert', 128, 'jalley', 'signature', 1, '담리 드레스(2)', 'JA21-OPS002-A-IV-1', 1080000, 'System backup', 'Y', 'Y', '2022-02-28 16:32:13', 'jalley'),
	(129, 'insert', 129, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-2', 1360000, 'System backup', 'Y', 'Y', '2022-02-28 16:33:17', 'jalley'),
	(130, 'insert', 130, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-3', 1360000, 'System backup', 'Y', 'Y', '2022-02-28 16:34:01', 'jalley'),
	(131, 'insert', 131, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-1', 1360000, 'System backup', 'Y', 'Y', '2022-02-28 16:34:55', 'jalley'),
	(132, 'insert', 132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 'System backup', 'Y', 'Y', '2022-02-28 16:35:53', 'jalley'),
	(133, 'insert', 133, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-1', 720000, 'System backup', 'Y', 'Y', '2022-02-28 16:37:57', 'jalley'),
	(134, 'insert', 134, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-3', 760000, 'System backup', 'Y', 'Y', '2022-03-02 16:22:14', 'jalley'),
	(135, 'insert', 135, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-03-02 16:44:29', 'jalley'),
	(136, 'insert', 136, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-3', 760000, 'System backup', 'Y', 'Y', '2022-03-02 16:45:39', 'jalley'),
	(137, 'insert', 137, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-2', 760000, 'System backup', 'Y', 'Y', '2022-03-02 16:57:35', 'jalley'),
	(138, 'insert', 138, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-03-02 16:58:33', 'jalley'),
	(139, 'insert', 139, 'jalley', 'signature', 1, 'BM자켓 드레스(폴리)', 'JA22-OPS014-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-03-02 17:07:26', 'jalley'),
	(140, 'insert', 140, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-03-02 17:20:27', 'jalley'),
	(141, 'insert', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-03-03 14:19:25', 'jalley'),
	(142, 'insert', 142, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-4', 760000, 'System backup', 'Y', 'Y', '2022-03-03 15:47:47', 'jalley'),
	(143, 'insert', 143, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA222-OPS014-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-03-03 15:49:16', 'jalley'),
	(144, 'insert', 144, 'jalley', 'signature', 1, 'BM 자켓 드레스(트위드)', 'JA22-OPS014-A-GOLD-1', 760000, 'System backup', 'Y', 'Y', '2022-03-03 15:51:06', 'jalley'),
	(145, 'insert', 145, 'jalley', 'signature', 1, 'BM 자켓 드레스(트위드)', 'JA22-OPS014-A-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-03-03 15:52:20', 'jalley'),
	(146, 'insert', 146, 'jalley', 'signature', 1, '와인 자켓', 'JA22-JK015-BK-1', 800000, 'System backup', 'Y', 'Y', '2022-03-03 15:54:03', 'jalley'),
	(147, 'insert', 147, 'jalley', 'signature', 1, '와인 팬츠', 'JA22-PT007-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-03-03 15:57:58', 'jalley'),
	(148, 'insert', 148, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-03-03 16:11:28', 'jalley'),
	(149, 'insert', 149, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:39:12', 'jalley'),
	(150, 'insert', 150, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:40:16', 'jalley'),
	(151, 'insert', 151, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA222-JK015-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:41:01', 'jalley'),
	(152, 'insert', 152, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-3', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:41:59', 'jalley'),
	(153, 'insert', 153, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:42:46', 'jalley'),
	(154, 'insert', 154, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-2', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:44:32', 'jalley'),
	(155, 'insert', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:50:48', 'jalley'),
	(156, 'insert', 156, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-1', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:51:36', 'jalley'),
	(157, 'insert', 157, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:52:47', 'jalley'),
	(158, 'insert', 158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:53:56', 'jalley'),
	(159, 'insert', 159, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-3', 760000, 'System backup', 'Y', 'Y', '2022-03-07 15:55:52', 'jalley'),
	(160, 'insert', 160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 'System backup', 'Y', 'Y', '2022-03-07 15:57:57', 'jalley'),
	(161, 'insert', 161, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-PK-1', 640000, 'System backup', 'Y', 'Y', '2022-03-07 15:58:40', 'jalley'),
	(162, 'insert', 162, 'jalley', 'signature', 1, '허니 레이스 자켓', 'JA22-JK016-A-GD-1S', 840000, 'System backup', 'Y', 'Y', '2022-03-07 15:59:58', 'jalley'),
	(163, 'insert', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-03-07 16:01:24', 'jalley'),
	(164, 'insert', 164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 'System backup', 'Y', 'Y', '2022-03-07 16:02:30', 'jalley'),
	(165, 'insert', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-03-07 16:03:18', 'jalley'),
	(166, 'insert', 166, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-PK-1', 480000, 'System backup', 'Y', 'Y', '2022-03-07 16:04:02', 'jalley'),
	(167, 'insert', 167, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-03-07 16:08:50', 'jalley'),
	(168, 'insert', 168, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-03-07 16:10:15', 'jalley'),
	(169, 'insert', 169, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-03-07 16:11:17', 'jalley'),
	(170, 'insert', 170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-03-07 16:12:09', 'jalley'),
	(171, 'insert', 171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 'System backup', 'Y', 'Y', '2022-03-07 16:12:49', 'jalley'),
	(172, 'insert', 172, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-03-07 16:13:34', 'jalley'),
	(173, 'insert', 173, 'jalley', 'signature', 1, 'BM-P 드레스', 'JA22-OPS016-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-03-10 15:46:53', 'jalley'),
	(174, 'insert', 174, 'jalley', 'signature', 1, 'BM-P드레스', 'JA22-OPS016', 760000, 'System backup', 'Y', 'Y', '2022-03-10 15:50:51', 'jalley'),
	(175, 'insert', 175, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-WH-1', 360000, 'System backup', 'Y', 'Y', '2022-03-10 15:52:18', 'jalley'),
	(176, 'insert', 176, 'jalley', 'signature', 1, 'BM 스커트', 'JA22-SK007-WH-2', 360000, 'System backup', 'Y', 'Y', '2022-03-10 15:53:21', 'jalley'),
	(177, 'insert', 177, 'jalley', 'signature', 1, 'BM 스커트', 'JA22-SK007-BK-1', 360000, 'System backup', 'Y', 'Y', '2022-03-10 15:54:11', 'jalley'),
	(178, 'insert', 178, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-BK-2', 360000, 'System backup', 'Y', 'Y', '2022-03-10 15:55:25', 'jalley'),
	(179, 'insert', 179, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-IK014-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-03-10 15:57:21', 'jalley'),
	(180, 'insert', 180, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-GR-2', 760000, 'System backup', 'Y', 'Y', '2022-03-10 15:58:22', 'jalley'),
	(181, 'insert', 181, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-1', 440000, 'System backup', 'Y', 'Y', '2022-03-10 15:59:53', 'jalley'),
	(182, 'insert', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-03-10 16:00:29', 'jalley'),
	(183, 'insert', 183, 'jalley', 'signature', 1, '허니 자켓', 'ja22-jk016-wh-3s', 680000, 'System backup', 'Y', 'Y', '2022-03-31 17:20:00', 'jalley'),
	(184, 'insert', 184, 'jalley', 'signature', 1, '허니자켓', 'ja22-jk016-wh-m4', 680000, 'System backup', 'Y', 'Y', '2022-03-31 17:21:00', 'jalley'),
	(185, 'insert', 185, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-1', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:24:04', 'jalley'),
	(186, 'insert', 186, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-2', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:25:06', 'jalley'),
	(187, 'insert', 187, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-3', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:25:48', 'jalley'),
	(188, 'insert', 188, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-1', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:26:42', 'jalley'),
	(189, 'insert', 189, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-2', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:27:25', 'jalley'),
	(190, 'insert', 190, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-3', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:28:08', 'jalley'),
	(191, 'insert', 191, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:28:46', 'jalley'),
	(192, 'insert', 192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:30:04', 'jalley'),
	(193, 'insert', 193, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-3', 440000, 'System backup', 'Y', 'Y', '2022-03-31 17:31:05', 'jalley'),
	(194, 'insert', 194, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-1', 2000000, 'System backup', 'Y', 'Y', '2022-04-04 14:45:51', 'jalley'),
	(195, 'insert', 195, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-2', 2000000, 'System backup', 'Y', 'Y', '2022-04-04 14:46:32', 'jalley'),
	(196, 'insert', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-04-04 14:47:23', 'jalley'),
	(197, 'insert', 197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 2000000, 'System backup', 'Y', 'Y', '2022-04-04 14:48:09', 'jalley'),
	(198, 'insert', 198, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-2', 2000000, 'System backup', 'Y', 'Y', '2022-04-04 14:48:45', 'jalley'),
	(199, 'insert', 199, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-1', 1440000, 'System backup', 'Y', 'Y', '2022-04-04 14:50:17', 'jalley'),
	(200, 'insert', 200, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-2', 1440000, 'System backup', 'Y', 'Y', '2022-04-04 14:51:14', 'jalley'),
	(201, 'insert', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-04-04 14:52:04', 'jalley'),
	(202, 'insert', 202, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-2', 1440000, 'System backup', 'Y', 'Y', '2022-04-04 14:52:50', 'jalley'),
	(203, 'insert', 203, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-04-04 14:57:18', 'jalley'),
	(256, 'update', 199, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-15 17:09:55', 'jalley'),
	(257, 'update', 184, 'jalley', 'signature', 1, '허니자켓', 'ja22-jk016-wh-m4', 680000, 'System backup', 'Y', 'Y', '2022-05-15 17:11:18', 'jalley'),
	(258, 'update', 185, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-1', 440000, 'System backup', 'Y', 'Y', '2022-05-15 17:11:21', 'jalley'),
	(259, 'update', 191, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-16 12:03:18', 'jalley'),
	(260, 'update', 190, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-16 12:03:18', 'jalley'),
	(261, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-16 14:38:06', 'jalley'),
	(262, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-16 14:38:06', 'jalley'),
	(263, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-16 14:38:06', 'jalley'),
	(264, 'update', 202, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-2', 1440000, 'System backup 1', 'Y', 'Y', '2022-05-16 17:04:06', 'emp07'),
	(265, 'update', 202, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-2', 1440000, 'System backup 1', 'Y', 'Y', '2022-05-16 17:07:21', 'emp07'),
	(266, 'update', 203, 'emp07', 'signature', 2, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-16 17:09:57', 'emp07'),
	(267, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-16 17:11:27', 'emp07'),
	(268, 'update', 203, 'emp07', 'signature', 2, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-16 17:12:15', 'emp07'),
	(269, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-16 17:13:47', 'emp07'),
	(270, 'update', 194, 'emp07', 'signature', 2, '라임 드레스', 'JA22-OPS017-WH-1', 2000000, 'System backup', 'Y', 'Y', '2022-05-16 17:14:10', 'emp07'),
	(271, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 10:50:44', 'emp07'),
	(272, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 10:50:53', 'emp07'),
	(273, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 10:54:27', 'emp07'),
	(274, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 10:54:35', 'emp07'),
	(275, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 10:56:12', 'emp07'),
	(276, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 10:58:21', 'emp07'),
	(277, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 10:59:21', 'emp07'),
	(278, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 10:59:47', 'emp07'),
	(279, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 11:02:29', 'emp07'),
	(280, 'update', 203, 'emp07', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 11:02:36', 'emp07'),
	(281, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:33:59', 'jalley'),
	(282, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:38:52', 'jalley'),
	(283, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:41:39', 'jalley'),
	(284, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:42:11', 'jalley'),
	(285, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:42:55', 'jalley'),
	(286, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:44:19', 'jalley'),
	(287, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:45:42', 'jalley'),
	(288, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:46:22', 'jalley'),
	(289, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:47:18', 'jalley'),
	(290, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:48:33', 'jalley'),
	(291, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:48:34', 'jalley'),
	(292, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:48:35', 'jalley'),
	(293, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:49:30', 'jalley'),
	(294, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:50:04', 'jalley'),
	(295, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:52:36', 'jalley'),
	(296, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:52:49', 'jalley'),
	(297, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 12:52:54', 'jalley'),
	(298, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:57:08', 'jalley'),
	(299, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 12:57:40', 'jalley'),
	(300, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 12:58:54', 'jalley'),
	(301, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 13:01:06', 'jalley'),
	(302, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 13:05:12', 'jalley'),
	(303, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 13:05:38', 'jalley'),
	(304, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 13:06:24', 'jalley'),
	(305, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 13:07:01', 'jalley'),
	(306, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 13:09:45', 'jalley'),
	(307, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 13:10:53', 'jalley'),
	(308, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 13:14:26', 'jalley'),
	(309, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 13:15:22', 'jalley'),
	(310, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 13:23:27', 'jalley'),
	(311, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 13:23:27', 'jalley'),
	(312, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 15:35:02', 'jalley'),
	(313, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 15:35:02', 'jalley'),
	(314, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 15:35:02', 'jalley'),
	(315, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 15:36:51', 'jalley'),
	(316, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 15:37:15', 'jalley'),
	(317, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 15:37:15', 'jalley'),
	(318, 'update', 163, 'jalley', 'signature', 1, '허니 자켓', 'JA22-PT007-GD-1S', 480000, 'System backup', 'Y', 'Y', '2022-05-17 15:37:15', 'jalley'),
	(319, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 15:37:56', 'jalley'),
	(320, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 15:37:56', 'jalley'),
	(321, 'update', 181, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-1', 440000, 'System backup', 'Y', 'Y', '2022-05-17 15:37:56', 'jalley'),
	(322, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 15:42:11', 'jalley'),
	(323, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 15:42:38', 'jalley'),
	(324, 'update', 183, 'jalley', 'signature', 1, '허니 자켓', 'ja22-jk016-wh-3s', 680000, 'System backup', 'Y', 'Y', '2022-05-17 15:42:48', 'jalley'),
	(325, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 15:45:39', 'jalley'),
	(326, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 15:57:11', 'jalley'),
	(327, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 15:57:23', 'jalley'),
	(328, 'update', 15, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-3M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 15:59:09', 'jalley'),
	(329, 'update', 190, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:02:04', 'jalley'),
	(330, 'update', 191, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:02:29', 'jalley'),
	(331, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:08:28', 'jalley'),
	(332, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 16:08:33', 'jalley'),
	(333, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:10:30', 'jalley'),
	(334, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 16:10:30', 'jalley'),
	(335, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:12:00', 'jalley'),
	(336, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 16:12:00', 'jalley'),
	(337, 'update', 158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:12:32', 'jalley'),
	(338, 'update', 190, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:16:37', 'jalley'),
	(339, 'update', 191, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:16:37', 'jalley'),
	(340, 'update', 175, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-WH-1', 360000, 'System backup', 'Y', 'Y', '2022-05-17 16:16:37', 'jalley'),
	(341, 'update', 151, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA222-JK015-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:16:37', 'jalley'),
	(342, 'update', 153, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:16:37', 'jalley'),
	(343, 'update', 153, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:20:24', 'jalley'),
	(344, 'update', 153, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:20:38', 'jalley'),
	(345, 'update', 151, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA222-JK015-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:20:38', 'jalley'),
	(346, 'update', 175, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-WH-1', 360000, 'System backup', 'Y', 'Y', '2022-05-17 16:20:38', 'jalley'),
	(347, 'update', 190, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:20:38', 'jalley'),
	(348, 'update', 191, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:20:38', 'jalley'),
	(349, 'update', 151, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA222-JK015-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:38:25', 'jalley'),
	(350, 'update', 175, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-WH-1', 360000, 'System backup', 'Y', 'Y', '2022-05-17 16:38:29', 'jalley'),
	(351, 'update', 190, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:38:34', 'jalley'),
	(352, 'update', 191, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:38:38', 'jalley'),
	(353, 'update', 153, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:39:01', 'jalley'),
	(354, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 16:39:58', 'jalley'),
	(355, 'update', 158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:44:22', 'jalley'),
	(356, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:44:22', 'jalley'),
	(357, 'update', 165, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-17 16:44:22', 'jalley'),
	(358, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:48:58', 'jalley'),
	(359, 'update', 158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:03', 'jalley'),
	(360, 'update', 158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:30', 'jalley'),
	(361, 'update', 200, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-2', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:30', 'jalley'),
	(362, 'update', 140, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:30', 'jalley'),
	(363, 'update', 142, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-4', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:30', 'jalley'),
	(364, 'update', 142, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-4', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:56', 'jalley'),
	(365, 'update', 140, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:56', 'jalley'),
	(366, 'update', 158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:56', 'jalley'),
	(367, 'update', 178, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-BK-2', 360000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:56', 'jalley'),
	(368, 'update', 177, 'jalley', 'signature', 1, 'BM 스커트', 'JA22-SK007-BK-1', 360000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:56', 'jalley'),
	(369, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-17 16:49:56', 'jalley'),
	(370, 'update', 158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:15', 'jalley'),
	(371, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:25', 'jalley'),
	(372, 'update', 178, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-BK-2', 360000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:25', 'jalley'),
	(373, 'update', 142, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-4', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:25', 'jalley'),
	(374, 'update', 140, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:25', 'jalley'),
	(375, 'update', 140, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:32', 'jalley'),
	(376, 'update', 142, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-4', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:33', 'jalley'),
	(377, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:36', 'jalley'),
	(378, 'update', 178, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-BK-2', 360000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:36', 'jalley'),
	(379, 'update', 203, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 16:50:36', 'jalley'),
	(380, 'update', 178, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-BK-2', 360000, 'System backup', 'Y', 'Y', '2022-05-17 16:52:54', 'jalley'),
	(381, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-17 16:53:22', 'jalley'),
	(382, 'update', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:53:22', 'jalley'),
	(383, 'update', 149, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:53:22', 'jalley'),
	(384, 'update', 149, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:53:35', 'jalley'),
	(385, 'update', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:53:38', 'jalley'),
	(386, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-17 16:53:38', 'jalley'),
	(387, 'update', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:54:09', 'jalley'),
	(388, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-17 16:54:09', 'jalley'),
	(389, 'update', 158, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:54:09', 'jalley'),
	(390, 'update', 159, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-3', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:54:09', 'jalley'),
	(391, 'update', 160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 'System backup', 'Y', 'Y', '2022-05-17 16:54:09', 'jalley'),
	(392, 'update', 140, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:54:09', 'jalley'),
	(393, 'update', 141, 'jalley', 'signature', 1, '스타 팬츠', 'JA21-PT005-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-17 16:54:09', 'jalley'),
	(394, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-17 16:55:09', 'jalley'),
	(395, 'update', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 16:56:53', 'jalley'),
	(396, 'update', 160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 'System backup', 'Y', 'Y', '2022-05-17 16:57:41', 'jalley'),
	(397, 'update', 159, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-3', 760000, 'System backup', 'Y', 'Y', '2022-05-17 17:08:12', 'jalley'),
	(398, 'update', 131, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-1', 1360000, 'System backup', 'Y', 'Y', '2022-05-17 17:08:12', 'jalley'),
	(399, 'update', 128, 'jalley', 'signature', 1, '담리 드레스(2)', 'JA21-OPS002-A-IV-1', 1080000, 'System backup', 'Y', 'Y', '2022-05-17 17:08:12', 'jalley'),
	(400, 'update', 131, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-1', 1360000, 'System backup', 'Y', 'Y', '2022-05-17 17:08:45', 'jalley'),
	(401, 'update', 128, 'jalley', 'signature', 1, '담리 드레스(2)', 'JA21-OPS002-A-IV-1', 1080000, 'System backup', 'Y', 'Y', '2022-05-17 17:08:46', 'jalley'),
	(402, 'update', 159, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-3', 760000, 'System backup', 'Y', 'Y', '2022-05-17 17:08:46', 'jalley'),
	(403, 'update', 128, 'jalley', 'signature', 1, '담리 드레스(2)', 'JA21-OPS002-A-IV-1', 1080000, 'System backup', 'Y', 'Y', '2022-05-17 17:09:15', 'jalley'),
	(404, 'update', 159, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-3', 760000, 'System backup', 'Y', 'Y', '2022-05-17 17:09:18', 'jalley'),
	(405, 'update', 138, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 17:09:18', 'jalley'),
	(406, 'update', 136, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-3', 760000, 'System backup', 'Y', 'Y', '2022-05-17 17:09:18', 'jalley'),
	(407, 'update', 135, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 17:09:18', 'jalley'),
	(408, 'update', 151, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA222-JK015-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 17:10:07', 'jalley'),
	(409, 'update', 175, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-WH-1', 360000, 'System backup', 'Y', 'Y', '2022-05-17 17:10:22', 'jalley'),
	(410, 'update', 190, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-17 17:10:22', 'jalley'),
	(411, 'update', 191, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-17 17:10:22', 'jalley'),
	(412, 'update', 151, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA222-JK015-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 17:10:22', 'jalley'),
	(413, 'update', 147, 'jalley', 'signature', 1, '와인 팬츠', 'JA22-PT007-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-17 19:13:28', 'jalley'),
	(414, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-05-17 19:19:53', 'jalley'),
	(415, 'update', 186, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 19:21:32', 'jalley'),
	(416, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-05-17 19:24:26', 'jalley'),
	(417, 'update', 193, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-17 19:42:31', 'jalley'),
	(418, 'update', 193, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-17 19:50:15', 'jalley'),
	(419, 'update', 159, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-3', 760000, 'System backup', 'Y', 'Y', '2022-05-17 19:51:41', 'jalley'),
	(420, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-05-17 19:52:48', 'jalley'),
	(421, 'update', 186, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 19:53:11', 'jalley'),
	(422, 'update', 133, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-1', 720000, 'System backup', 'Y', 'Y', '2022-05-17 19:53:33', 'jalley'),
	(423, 'update', 133, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-1', 720000, 'System backup', 'Y', 'Y', '2022-05-17 19:53:48', 'jalley'),
	(424, 'update', 138, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 19:54:11', 'jalley'),
	(425, 'update', 135, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 19:54:23', 'jalley'),
	(426, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 20:03:31', 'jalley'),
	(427, 'update', 186, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 20:03:36', 'jalley'),
	(428, 'update', 171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 20:03:50', 'jalley'),
	(429, 'update', 136, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-3', 760000, 'System backup', 'Y', 'Y', '2022-05-17 20:05:56', 'jalley'),
	(430, 'update', 171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 20:06:00', 'jalley'),
	(431, 'update', 202, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-2', 1440000, 'System backup 1', 'Y', 'Y', '2022-05-17 20:06:02', 'jalley'),
	(432, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-17 20:06:04', 'jalley'),
	(433, 'update', 189, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 20:06:07', 'jalley'),
	(434, 'update', 202, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-2', 1440000, 'System backup 1', 'Y', 'Y', '2022-05-17 20:06:18', 'jalley'),
	(435, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 20:06:30', 'jalley'),
	(436, 'update', 197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 2000000, 'System backup', 'Y', 'Y', '2022-05-17 20:10:25', 'jalley'),
	(437, 'update', 124, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-IV-2', 1080000, 'System backup', 'Y', 'Y', '2022-05-17 20:15:07', 'jalley'),
	(438, 'update', 124, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-IV-2', 1080000, 'System backup', 'Y', 'Y', '2022-05-17 20:15:33', 'jalley'),
	(439, 'update', 120, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-PK-1', 1320000, 'System backup', 'Y', 'Y', '2022-05-17 20:15:40', 'jalley'),
	(440, 'update', 120, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-PK-1', 1320000, 'System backup', 'Y', 'Y', '2022-05-17 20:15:49', 'jalley'),
	(441, 'update', 197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 2000000, 'System backup', 'Y', 'Y', '2022-05-17 20:15:54', 'jalley'),
	(442, 'update', 23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 'System backup', 'Y', 'Y', '2022-05-17 20:16:15', 'jalley'),
	(443, 'update', 22, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-4M', 280000, 'System backup', 'Y', 'Y', '2022-05-17 20:16:16', 'jalley'),
	(444, 'update', 21, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-1S', 280000, 'System backup', 'Y', 'Y', '2022-05-17 20:16:17', 'jalley'),
	(445, 'update', 189, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 20:16:32', 'jalley'),
	(446, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 20:18:25', 'jalley'),
	(447, 'update', 180, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-GR-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 20:18:25', 'jalley'),
	(448, 'update', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 20:18:25', 'jalley'),
	(449, 'update', 150, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 20:18:25', 'jalley'),
	(450, 'update', 139, 'jalley', 'signature', 1, 'BM자켓 드레스(폴리)', 'JA22-OPS014-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 20:18:25', 'jalley'),
	(451, 'update', 124, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-IV-2', 1080000, 'System backup', 'Y', 'Y', '2022-05-17 20:18:25', 'jalley'),
	(452, 'update', 123, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-WH-2', 1320000, 'System backup', 'Y', 'Y', '2022-05-17 20:18:25', 'jalley'),
	(453, 'update', 123, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-WH-2', 1320000, 'System backup', 'Y', 'Y', '2022-05-17 20:18:48', 'jalley'),
	(454, 'update', 124, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-IV-2', 1080000, 'System backup', 'Y', 'Y', '2022-05-17 21:03:50', 'jalley'),
	(455, 'update', 139, 'jalley', 'signature', 1, 'BM자켓 드레스(폴리)', 'JA22-OPS014-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 21:04:28', 'jalley'),
	(456, 'update', 150, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 21:05:54', 'jalley'),
	(457, 'update', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-17 21:22:01', 'jalley'),
	(458, 'update', 180, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-GR-2', 760000, 'System backup', 'Y', 'Y', '2022-05-17 21:22:02', 'jalley'),
	(459, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 21:22:02', 'jalley'),
	(460, 'update', 195, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-2', 2000000, 'System backup', 'Y', 'Y', '2022-05-17 21:22:13', 'jalley'),
	(461, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-05-17 21:22:14', 'jalley'),
	(462, 'update', 192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-05-17 21:22:16', 'jalley'),
	(463, 'update', 179, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-IK014-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-05-18 05:59:01', 'jalley'),
	(464, 'update', 179, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-IK014-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-05-18 07:46:04', 'jalley'),
	(465, 'update', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-18 07:46:08', 'jalley'),
	(466, 'update', 29, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK0054-WH-1M', 280000, 'System backup', 'Y', 'Y', '2022-05-18 07:46:28', 'jalley'),
	(467, 'update', 36, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-2M', 880000, 'System backup', 'Y', 'Y', '2022-05-18 07:46:31', 'jalley'),
	(468, 'update', 131, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-1', 1360000, 'System backup', 'Y', 'Y', '2022-05-18 07:47:30', 'jalley'),
	(469, 'update', 115, 'jalley', 'signature', 1, '메릴 드레스', 'JA21-OPS013-IV-1', 960000, 'System backup', 'Y', 'Y', '2022-05-18 07:47:33', 'jalley'),
	(470, 'update', 29, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK0054-WH-1M', 280000, 'System backup', 'Y', 'Y', '2022-05-18 07:47:54', 'jalley'),
	(471, 'update', 38, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-3S', 880000, 'System backup', 'Y', 'Y', '2022-05-18 07:47:58', 'jalley'),
	(472, 'update', 195, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-2', 2000000, 'System backup', 'Y', 'Y', '2022-05-18 10:28:45', 'jalley'),
	(473, 'update', 99, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-IV-1', 920000, 'System backup', 'Y', 'Y', '2022-05-18 10:28:53', 'jalley'),
	(474, 'update', 97, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-1', 920000, 'System backup', 'Y', 'Y', '2022-05-18 10:28:54', 'jalley'),
	(475, 'update', 166, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-PK-1', 480000, 'System backup', 'Y', 'Y', '2022-05-18 17:09:18', 'jalley'),
	(476, 'update', 167, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-18 17:09:19', 'jalley'),
	(477, 'update', 166, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-PK-1', 480000, 'System backup', 'Y', 'Y', '2022-05-18 17:10:32', 'jalley'),
	(478, 'update', 167, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-18 17:10:33', 'jalley'),
	(479, 'update', 188, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-20 15:57:52', 'jalley'),
	(480, 'update', 202, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-2', 1440000, 'System backup 1', 'Y', 'Y', '2022-05-20 16:18:58', 'jalley'),
	(481, 'update', 142, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA22-OPS014-BK-4', 760000, 'System backup', 'Y', 'Y', '2022-05-20 16:27:55', 'jalley'),
	(482, 'update', 157, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-20 16:31:10', 'jalley'),
	(483, 'update', 123, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-WH-2', 1320000, 'System backup', 'Y', 'Y', '2022-05-20 16:38:49', 'jalley'),
	(484, 'update', 120, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-PK-1', 1320000, 'System backup', 'Y', 'Y', '2022-05-20 16:41:46', 'jalley'),
	(485, 'update', 21, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-1S', 280000, 'System backup', 'Y', 'Y', '2022-05-20 16:44:32', 'jalley'),
	(486, 'update', 18, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-3S', 440000, 'System backup', 'Y', 'Y', '2022-05-20 16:44:49', 'jalley'),
	(487, 'update', 45, 'jalley', 'signature', 1, '인어 롱 스커트', 'JA21-SK004-WH-1S', 440000, 'System backup', 'Y', 'Y', '2022-05-21 13:22:38', 'jalley'),
	(488, 'update', 52, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-BK-3M', 440000, 'System backup', 'Y', 'Y', '2022-05-21 13:22:38', 'jalley'),
	(489, 'update', 53, 'jalley', 'signature', 1, '인어팬츠', 'JA211-PT001-WH-4S', 440000, 'System backup', 'Y', 'Y', '2022-05-21 13:29:22', 'jalley'),
	(490, 'update', 60, 'jalley', 'signature', 1, '가든 슬립', 'JA21-OPS012-IV-1', 320000, 'System backup', 'Y', 'Y', '2022-05-21 13:29:22', 'jalley'),
	(491, 'update', 119, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-BK-2', 1320000, 'System backup', 'Y', 'Y', '2022-05-22 09:16:09', 'jalley'),
	(492, 'update', 106, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-BK-4', 920000, 'System backup', 'Y', 'Y', '2022-05-22 09:16:09', 'jalley'),
	(493, 'update', 28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 'System backup', 'Y', 'Y', '2022-05-22 17:26:17', 'jalley'),
	(494, 'update', 137, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-2', 760000, 'System backup', 'Y', 'Y', '2022-05-22 21:56:55', 'jalley'),
	(495, 'update', 29, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK0054-WH-1M', 280000, 'System backup', 'Y', 'Y', '2022-05-22 21:59:06', 'jalley'),
	(496, 'update', 24, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-4M', 280000, 'System backup', 'Y', 'Y', '2022-05-22 21:59:06', 'jalley'),
	(497, 'update', 12, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-2M', 440000, 'System backup', 'Y', 'Y', '2022-05-22 22:05:12', 'jalley'),
	(498, 'update', 137, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-2', 760000, 'System backup', 'Y', 'Y', '2022-05-23 06:52:24', 'jalley'),
	(499, 'update', 149, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-23 06:52:49', 'jalley'),
	(500, 'update', 101, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS04-BK-1', 1200000, 'System backup', 'Y', 'Y', '2022-05-23 06:58:32', 'jalley'),
	(501, 'update', 146, 'jalley', 'signature', 1, '와인 자켓', 'JA22-JK015-BK-1', 800000, 'System backup', 'Y', 'Y', '2022-05-23 07:19:34', 'jalley'),
	(502, 'update', 137, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-2', 760000, 'System backup', 'Y', 'Y', '2022-05-23 07:19:53', 'jalley'),
	(503, 'update', 92, 'jalley', 'signature', 1, '스피치 자켓', 'JA21-JK013-IV-1', 840000, 'System backup', 'Y', 'Y', '2022-05-23 08:14:17', 'jalley'),
	(504, 'update', 110, 'jalley', 'signature', 1, '벨 드레스', 'JA21-OPS009-BC-1', 1160000, 'System backup', 'Y', 'Y', '2022-05-23 08:16:42', 'jalley'),
	(505, 'update', 96, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-2', 920000, 'System backup', 'Y', 'Y', '2022-05-23 08:17:17', 'jalley'),
	(506, 'update', 96, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-2', 920000, 'System backup', 'Y', 'Y', '2022-05-23 08:17:40', 'jalley'),
	(507, 'update', 76, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-BK-2', 400000, 'System backup', 'Y', 'Y', '2022-05-23 08:30:39', 'jalley'),
	(508, 'update', 144, 'jalley', 'signature', 1, 'BM 자켓 드레스(트위드)', 'JA22-OPS014-A-GOLD-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 08:32:09', 'jalley'),
	(509, 'update', 76, 'jalley', 'signature', 1, '샤잉 스커트', 'JA21-SK001-BK-2', 400000, 'System backup', 'Y', 'Y', '2022-05-23 08:59:58', 'jalley'),
	(510, 'update', 144, 'jalley', 'signature', 1, 'BM 자켓 드레스(트위드)', 'JA22-OPS014-A-GOLD-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 09:09:42', 'jalley'),
	(511, 'update', 84, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-23 09:19:20', 'jalley'),
	(512, 'update', 84, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-WH-1', 480000, 'System backup', 'Y', 'Y', '2022-05-23 09:21:22', 'jalley'),
	(513, 'update', 56, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-WH-3S', 440000, 'System backup', 'Y', 'Y', '2022-05-23 09:36:52', 'jalley'),
	(514, 'update', 56, 'jalley', 'signature', 1, '인어팬츠', 'JA21-PT001-WH-3S', 440000, 'System backup', 'Y', 'Y', '2022-05-23 09:39:03', 'jalley'),
	(515, 'update', 127, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-4', 1080000, 'System backup', 'Y', 'Y', '2022-05-23 09:39:20', 'jalley'),
	(516, 'update', 127, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-4', 1080000, 'System backup', 'Y', 'Y', '2022-05-23 09:45:10', 'jalley'),
	(517, 'update', 87, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-23 09:48:57', 'jalley'),
	(518, 'update', 87, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-BK-2', 760000, 'System backup', 'Y', 'Y', '2022-05-23 09:49:54', 'jalley'),
	(519, 'update', 88, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 09:50:25', 'jalley'),
	(520, 'update', 88, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 09:53:30', 'jalley'),
	(521, 'update', 129, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 09:55:32', 'jalley'),
	(522, 'update', 28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 'System backup', 'Y', 'Y', '2022-05-23 09:59:14', 'jalley'),
	(523, 'update', 58, 'jalley', 'signature', 1, '가든 드레스', 'JA21-OPS007-IV-1', 2600000, 'System backup', 'Y', 'Y', '2022-05-23 10:03:12', 'jalley'),
	(524, 'update', 129, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 10:04:37', 'jalley'),
	(525, 'update', 160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 'System backup', 'Y', 'Y', '2022-05-23 10:10:11', 'jalley'),
	(526, 'update', 28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 'System backup', 'Y', 'Y', '2022-05-23 10:14:08', 'jalley'),
	(527, 'update', 58, 'jalley', 'signature', 1, '가든 드레스', 'JA21-OPS007-IV-1', 2600000, 'System backup', 'Y', 'Y', '2022-05-23 10:15:04', 'jalley'),
	(528, 'update', 133, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-1', 720000, 'System backup', 'Y', 'Y', '2022-05-23 10:16:53', 'jalley'),
	(529, 'update', 170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-23 10:20:52', 'jalley'),
	(530, 'update', 133, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-1', 720000, 'System backup', 'Y', 'Y', '2022-05-23 10:21:00', 'jalley'),
	(531, 'update', 132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 10:28:38', 'jalley'),
	(532, 'update', 160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 'System backup', 'Y', 'Y', '2022-05-23 10:28:46', 'jalley'),
	(533, 'update', 164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 'System backup', 'Y', 'Y', '2022-05-23 10:29:01', 'jalley'),
	(534, 'update', 170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-23 10:29:12', 'jalley'),
	(535, 'update', 167, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 10:29:19', 'jalley'),
	(536, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-05-23 10:43:20', 'jalley'),
	(537, 'update', 178, 'jalley', 'signature', 1, 'BM스커트', 'JA22-SK007-BK-2', 360000, 'System backup', 'Y', 'Y', '2022-05-23 10:43:29', 'jalley'),
	(538, 'update', 132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 10:50:38', 'jalley'),
	(539, 'update', 127, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-4', 1080000, 'System backup', 'Y', 'Y', '2022-05-23 10:51:14', 'jalley'),
	(540, 'update', 102, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS004-BK-4', 1200000, 'System backup', 'Y', 'Y', '2022-05-23 10:54:21', 'jalley'),
	(541, 'update', 170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-23 10:59:29', 'jalley'),
	(542, 'update', 164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 'System backup', 'Y', 'Y', '2022-05-23 10:59:57', 'jalley'),
	(543, 'update', 167, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 11:01:31', 'jalley'),
	(544, 'update', 132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 11:02:30', 'jalley'),
	(545, 'update', 127, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-4', 1080000, 'System backup', 'Y', 'Y', '2022-05-23 11:05:55', 'jalley'),
	(546, 'update', 81, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-BK-3', 480000, 'System backup', 'Y', 'Y', '2022-05-23 11:06:13', 'jalley'),
	(547, 'update', 81, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-BK-3', 480000, 'System backup', 'Y', 'Y', '2022-05-23 11:06:44', 'jalley'),
	(548, 'update', 164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 'System backup', 'Y', 'Y', '2022-05-23 11:07:16', 'jalley'),
	(549, 'update', 102, 'jalley', 'signature', 1, '엔젤 드레스', 'JA21-OPS004-BK-4', 1200000, 'System backup', 'Y', 'Y', '2022-05-23 11:17:05', 'jalley'),
	(550, 'update', 149, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-23 11:17:15', 'jalley'),
	(551, 'update', 149, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-23 11:17:22', 'jalley'),
	(552, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:18:00', 'jalley'),
	(553, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:18:04', 'jalley'),
	(554, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:18:50', 'jalley'),
	(555, 'update', 170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-23 11:18:58', 'jalley'),
	(556, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:19:17', 'jalley'),
	(557, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:19:54', 'jalley'),
	(558, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:20:17', 'jalley'),
	(559, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:20:18', 'jalley'),
	(560, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:20:42', 'jalley'),
	(561, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:24:42', 'jalley'),
	(562, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:24:57', 'jalley'),
	(563, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:24:58', 'jalley'),
	(564, 'update', 132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 11:25:02', 'jalley'),
	(565, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:25:06', 'jalley'),
	(566, 'update', 172, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-05-23 11:25:09', 'jalley'),
	(567, 'update', 173, 'jalley', 'signature', 1, 'BM-P 드레스', 'JA22-OPS016-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 11:25:11', 'jalley'),
	(568, 'update', 173, 'jalley', 'signature', 1, 'BM-P 드레스', 'JA22-OPS016-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 11:25:13', 'jalley'),
	(569, 'update', 168, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 11:25:14', 'jalley'),
	(570, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:25:21', 'jalley'),
	(571, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 11:25:23', 'jalley'),
	(572, 'update', 149, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-23 11:26:08', 'jalley'),
	(573, 'update', 144, 'jalley', 'signature', 1, 'BM 자켓 드레스(트위드)', 'JA22-OPS014-A-GOLD-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 11:26:17', 'jalley'),
	(574, 'update', 1, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-1S', 880000, 'System backup', 'Y', 'Y', '2022-05-23 11:27:58', 'jalley'),
	(575, 'update', 161, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-PK-1', 640000, 'System backup', 'Y', 'Y', '2022-05-23 11:29:11', 'jalley'),
	(576, 'update', 133, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-1', 720000, 'System backup', 'Y', 'Y', '2022-05-23 13:04:30', 'jalley'),
	(577, 'update', 164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 'System backup', 'Y', 'Y', '2022-05-23 13:05:15', 'jalley'),
	(578, 'update', 97, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-1', 920000, 'System backup', 'Y', 'Y', '2022-05-23 13:08:23', 'jalley'),
	(579, 'update', 192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-05-23 13:17:04', 'jalley'),
	(580, 'update', 179, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-IK014-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-05-23 13:17:12', 'jalley'),
	(581, 'update', 129, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 13:17:25', 'jalley'),
	(582, 'update', 129, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 13:25:06', 'jalley'),
	(583, 'update', 129, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-23 13:26:16', 'jalley'),
	(584, 'update', 114, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-23 13:26:31', 'jalley'),
	(585, 'update', 114, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-05-23 13:26:35', 'jalley'),
	(586, 'update', 92, 'jalley', 'signature', 1, '스피치 자켓', 'JA21-JK013-IV-1', 840000, 'System backup', 'Y', 'Y', '2022-05-23 13:28:02', 'jalley'),
	(587, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-23 14:28:31', 'jalley'),
	(588, 'update', 97, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-1', 920000, 'System backup', 'Y', 'Y', '2022-05-23 14:28:33', 'jalley'),
	(589, 'update', 164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 'System backup', 'Y', 'Y', '2022-05-24 09:38:32', 'jalley'),
	(590, 'update', 164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 'System backup', 'Y', 'Y', '2022-05-24 09:38:33', 'jalley'),
	(591, 'update', 164, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-GD-2M', 480000, 'System backup', 'Y', 'Y', '2022-05-24 09:38:38', 'jalley'),
	(592, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-05-24 15:08:15', 'jalley'),
	(593, 'update', 192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-05-24 15:08:27', 'jalley'),
	(594, 'update', 197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 2000000, 'System backup', 'Y', 'Y', '2022-05-24 15:42:09', 'jalley'),
	(595, 'update', 189, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-PK-2', 440000, 'System backup', 'Y', 'Y', '2022-05-24 15:42:28', 'jalley'),
	(596, 'update', 23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 'System backup', 'Y', 'Y', '2022-05-24 15:43:45', 'jalley'),
	(597, 'update', 14, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-24 15:43:48', 'jalley'),
	(598, 'update', 23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 'System backup', 'Y', 'Y', '2022-05-24 15:59:10', 'jalley'),
	(599, 'update', 170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-24 16:02:37', 'jalley'),
	(600, 'update', 14, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-4M', 440000, 'System backup', 'Y', 'Y', '2022-05-24 16:02:57', 'jalley'),
	(601, 'update', 195, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-2', 2000000, 'System backup', 'Y', 'Y', '2022-05-24 17:15:25', 'jalley'),
	(602, 'update', 195, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-2', 2000000, 'System backup', 'Y', 'Y', '2022-05-24 17:15:29', 'jalley'),
	(603, 'update', 193, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-24 17:15:34', 'jalley'),
	(604, 'update', 132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-25 16:31:29', 'jalley'),
	(605, 'update', 193, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-3', 440000, 'System backup', 'Y', 'Y', '2022-05-26 14:59:34', 'jalley'),
	(606, 'update', 170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-26 14:59:35', 'jalley'),
	(607, 'update', 197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 2000000, 'System backup', 'Y', 'Y', '2022-05-26 15:01:55', 'jalley'),
	(608, 'update', 91, 'jalley', 'signature', 1, '베이직 팬츠 (투 버튼)', 'JA21-PT005-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-26 15:11:23', 'jalley'),
	(609, 'update', 91, 'jalley', 'signature', 1, '베이직 팬츠 (투 버튼)', 'JA21-PT005-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-26 16:25:37', 'jalley'),
	(610, 'update', 132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 'System backup', 'Y', 'Y', '2022-05-26 16:25:37', 'jalley'),
	(611, 'update', 174, 'jalley', 'signature', 1, 'BM-P드레스', 'JA22-OPS016', 760000, 'System backup', 'Y', 'Y', '2022-05-27 09:51:43', 'jalley'),
	(612, 'update', 91, 'jalley', 'signature', 1, '베이직 팬츠 (투 버튼)', 'JA21-PT005-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-27 09:51:47', 'jalley'),
	(613, 'update', 192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-05-27 13:57:02', 'jalley'),
	(614, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-05-27 13:57:02', 'jalley'),
	(615, 'update', 96, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-BK-2', 920000, 'System backup', 'Y', 'Y', '2022-05-27 14:03:22', 'jalley'),
	(616, 'update', 95, 'jalley', 'signature', 1, '스피치 팬츠', 'JA21-PT003-IV-2', 640000, 'System backup', 'Y', 'Y', '2022-05-27 14:03:25', 'jalley'),
	(617, 'update', 34, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-3M', 440000, 'System backup', 'Y', 'Y', '2022-05-27 15:11:16', 'jalley'),
	(618, 'update', 33, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-1S', 440000, 'System backup', 'Y', 'Y', '2022-05-27 15:11:16', 'jalley'),
	(619, 'update', 98, 'jalley', 'signature', 1, '엘리 케이프 롱 코트', 'JA21-JK010-IV-1', 720000, 'System backup', 'Y', 'Y', '2022-05-27 15:11:57', 'jalley'),
	(620, 'update', 34, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-3M', 440000, 'System backup', 'Y', 'Y', '2022-05-27 15:12:02', 'jalley'),
	(621, 'update', 98, 'jalley', 'signature', 1, '엘리 케이프 롱 코트', 'JA21-JK010-IV-1', 720000, 'System backup', 'Y', 'Y', '2022-05-27 15:12:05', 'jalley'),
	(622, 'update', 33, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-1S', 440000, 'System backup', 'Y', 'Y', '2022-05-27 15:12:32', 'jalley'),
	(623, 'update', 99, 'jalley', 'signature', 1, '엘리 드레스', 'JA21-OPS006-IV-1', 920000, 'System backup', 'Y', 'Y', '2022-05-27 15:13:02', 'jalley'),
	(624, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-05-27 15:13:14', 'jalley'),
	(625, 'update', 23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 'System backup', 'Y', 'Y', '2022-05-27 15:13:16', 'jalley'),
	(626, 'update', 197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 2000000, 'System backup', 'Y', 'Y', '2022-05-27 15:30:30', 'jalley'),
	(627, 'update', 35, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-BK-1S', 880000, 'System backup', 'Y', 'Y', '2022-05-27 15:30:33', 'jalley'),
	(628, 'update', 104, 'jalley', 'signature', 1, '렌즈 드레스', 'JA21-OPS005-BK-3', 920000, 'System backup', 'Y', 'Y', '2022-05-27 16:39:27', 'jalley'),
	(629, 'update', 79, 'jalley', 'signature', 1, '하루 블라우스', 'JA21-JK001-A-IV-4', 480000, 'System backup', 'Y', 'Y', '2022-05-27 16:39:27', 'jalley'),
	(630, 'update', 91, 'jalley', 'signature', 1, '베이직 팬츠 (투 버튼)', 'JA21-PT005-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-05-29 18:42:54', 'jalley'),
	(631, 'update', 127, 'jalley', 'signature', 1, '담리 드레스(1)', 'JA21-OPS002-BK-4', 1080000, 'System backup', 'Y', 'Y', '2022-05-29 18:47:38', 'jalley'),
	(632, 'update', 32, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-3M', 440000, 'System backup', 'Y', 'Y', '2022-05-30 09:52:50', 'jalley'),
	(633, 'update', 37, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-WH-1S', 880000, 'System backup', 'Y', 'Y', '2022-05-30 09:53:06', 'jalley'),
	(634, 'update', 37, 'jalley', 'signature', 1, '인어자켓', 'JA21-JK008-WH-1S', 880000, 'System backup', 'Y', 'Y', '2022-05-30 09:55:50', 'jalley'),
	(635, 'update', 32, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-3M', 440000, 'System backup', 'Y', 'Y', '2022-05-30 09:55:50', 'jalley'),
	(636, 'update', 131, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-1', 1360000, 'System backup', 'Y', 'Y', '2022-05-30 09:56:38', 'jalley'),
	(637, 'update', 121, 'jalley', 'signature', 1, '원더 코트 (숏)', 'JA21-OPS010-A-WH-1', 1320000, 'System backup', 'Y', 'Y', '2022-05-30 09:56:38', 'jalley'),
	(638, 'update', 89, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-05-30 13:55:21', 'jalley'),
	(639, 'update', 86, 'jalley', 'signature', 1, '베이직 자켓', 'JA21-JK005-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-05-30 13:55:21', 'jalley'),
	(640, 'update', 169, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-05-31 08:28:37', 'jalley'),
	(641, 'update', 112, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WINE-2', 1400000, 'System backup', 'Y', 'Y', '2022-06-01 14:15:37', 'jalley'),
	(642, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-06-01 14:49:31', 'jalley'),
	(643, 'update', 195, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-2', 2000000, 'System backup', 'Y', 'Y', '2022-06-01 14:56:10', 'jalley'),
	(644, 'update', 167, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-06-01 16:27:33', 'jalley'),
	(645, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-06-01 16:28:11', 'jalley'),
	(646, 'update', 136, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-A-BK-3', 760000, 'System backup', 'Y', 'Y', '2022-06-01 16:29:14', 'jalley'),
	(647, 'update', 114, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-06-01 17:11:13', 'jalley'),
	(648, 'update', 192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-06-01 17:11:42', 'jalley'),
	(649, 'update', 192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-06-01 17:21:10', 'jalley'),
	(650, 'update', 114, 'jalley', 'signature', 1, '제이 드레스', 'JA21-OPS011-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-06-01 17:21:10', 'jalley'),
	(651, 'update', 32, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-WH-3M', 440000, 'System backup', 'Y', 'Y', '2022-06-03 10:35:10', 'jalley'),
	(652, 'update', 7, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-03 11:00:48', 'jalley'),
	(653, 'update', 9, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-03 11:00:48', 'jalley'),
	(654, 'update', 41, 'jalley', 'signature', 1, '인어 케이프', 'JA21-JK006-WH-1', 720000, 'System backup', 'Y', 'Y', '2022-06-03 11:33:54', 'jalley'),
	(655, 'update', 9, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-03 11:57:02', 'jalley'),
	(656, 'update', 7, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-03 11:57:02', 'jalley'),
	(657, 'update', 83, 'jalley', 'signature', 1, '하루 스커트', 'JA21-SK001-A-BK-2', 480000, 'System backup', 'Y', 'Y', '2022-06-03 11:59:15', 'jalley'),
	(658, 'update', 171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 'System backup', 'Y', 'Y', '2022-06-03 14:04:05', 'jalley'),
	(659, 'update', 173, 'jalley', 'signature', 1, 'BM-P 드레스', 'JA22-OPS016-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-06-03 14:04:05', 'jalley'),
	(660, 'update', 173, 'jalley', 'signature', 1, 'BM-P 드레스', 'JA22-OPS016-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-06-03 14:15:02', 'jalley'),
	(661, 'update', 171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 'System backup', 'Y', 'Y', '2022-06-03 14:15:02', 'jalley'),
	(662, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-06-03 14:31:55', 'jalley'),
	(663, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-06-03 14:34:00', 'jalley'),
	(664, 'update', 187, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-3', 440000, 'System backup', 'Y', 'Y', '2022-06-04 10:13:08', 'jalley'),
	(665, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-06-04 10:57:59', 'jalley'),
	(666, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-06-04 10:58:06', 'jalley'),
	(667, 'update', 171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 'System backup', 'Y', 'Y', '2022-06-04 13:08:34', 'jalley'),
	(668, 'update', 201, 'admin', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-06-06 14:47:17', 'admin'),
	(669, 'update', 193, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-3', 440000, 'System backup', 'Y', 'Y', '2022-06-07 13:40:12', 'jalley'),
	(670, 'update', 192, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-BK-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 13:40:30', 'jalley'),
	(671, 'update', 186, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 13:50:10', 'jalley'),
	(672, 'update', 176, 'jalley', 'signature', 1, 'BM 스커트', 'JA22-SK007-WH-2', 360000, 'System backup', 'Y', 'Y', '2022-06-07 13:50:10', 'jalley'),
	(673, 'update', 186, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 14:23:47', 'jalley'),
	(674, 'update', 186, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 14:34:01', 'jalley'),
	(675, 'update', 176, 'jalley', 'signature', 1, 'BM 스커트', 'JA22-SK007-WH-2', 360000, 'System backup', 'Y', 'Y', '2022-06-07 14:41:54', 'jalley'),
	(676, 'update', 196, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-WH-3', 2000000, 'System backup', 'Y', 'Y', '2022-06-07 14:42:47', 'jalley'),
	(677, 'update', 194, 'jalley', 'signature', 2, '라임 드레스', 'JA22-OPS017-WH-1', 2000000, 'System backup', 'Y', 'Y', '2022-06-07 14:42:47', 'jalley'),
	(678, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-06-07 14:53:36', 'jalley'),
	(679, 'update', 117, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-BK-1', 1400000, 'System backup', 'Y', 'Y', '2022-06-07 14:53:36', 'jalley'),
	(680, 'update', 118, 'jalley', 'signature', 1, '원더 코트 (롱)', 'JA21-OPS010-WH-1', 1400000, 'System backup', 'Y', 'Y', '2022-06-07 14:57:06', 'jalley'),
	(681, 'update', 33, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:05:31', 'jalley'),
	(682, 'update', 34, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-3M', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:05:31', 'jalley'),
	(683, 'update', 34, 'jalley', 'signature', 1, '스타팬츠', 'JA21-PT005-BK-3M', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:08:36', 'jalley'),
	(684, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:18:00', 'jalley'),
	(685, 'update', 180, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-JK014-GR-2', 760000, 'System backup', 'Y', 'Y', '2022-06-07 16:18:00', 'jalley'),
	(686, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:19:29', 'jalley'),
	(687, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:24:02', 'jalley'),
	(688, 'update', 176, 'jalley', 'signature', 1, 'BM 스커트', 'JA22-SK007-WH-2', 360000, 'System backup', 'Y', 'Y', '2022-06-07 16:24:02', 'jalley'),
	(689, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:26:06', 'jalley'),
	(690, 'update', 170, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-1', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:34:08', 'jalley'),
	(691, 'update', 147, 'jalley', 'signature', 1, '와인 팬츠', 'JA22-PT007-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:34:08', 'jalley'),
	(692, 'update', 147, 'jalley', 'signature', 1, '와인 팬츠', 'JA22-PT007-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:36:08', 'jalley'),
	(693, 'update', 203, 'admin', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-06-07 16:48:06', 'admin'),
	(694, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:50:29', 'jalley'),
	(695, 'update', 147, 'jalley', 'signature', 1, '와인 팬츠', 'JA22-PT007-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:50:30', 'jalley'),
	(696, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:53:32', 'jalley'),
	(697, 'update', 147, 'jalley', 'signature', 1, '와인 팬츠', 'JA22-PT007-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-06-07 16:55:48', 'jalley'),
	(698, 'update', 201, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-1', 1440000, 'System backup', 'Y', 'Y', '2022-06-07 17:29:25', 'jalley'),
	(699, 'update', 182, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-2', 440000, 'System backup', 'Y', 'Y', '2022-06-07 17:29:33', 'jalley'),
	(700, 'update', 173, 'jalley', 'signature', 1, 'BM-P 드레스', 'JA22-OPS016-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 17:31:52', 'jalley'),
	(701, 'update', 166, 'jalley', 'signature', 1, '허니 팬츠', 'JA22-PT007-PK-1', 480000, 'System backup', 'Y', 'Y', '2022-06-07 17:31:52', 'jalley'),
	(702, 'update', 179, 'jalley', 'signature', 1, '구찌 자켓', 'JA22-IK014-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 17:37:01', 'jalley'),
	(703, 'update', 162, 'jalley', 'signature', 1, '허니 레이스 자켓', 'JA22-JK016-A-GD-1S', 840000, 'System backup', 'Y', 'Y', '2022-06-07 17:37:01', 'jalley'),
	(704, 'update', 2, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-5M', 880000, 'System backup', 'Y', 'Y', '2022-06-07 17:38:24', 'jalley'),
	(705, 'update', 3, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-PK-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-07 17:38:24', 'jalley'),
	(706, 'update', 139, 'jalley', 'signature', 1, 'BM자켓 드레스(폴리)', 'JA22-OPS014-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 17:46:29', 'jalley'),
	(707, 'update', 138, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 17:46:30', 'jalley'),
	(708, 'update', 139, 'jalley', 'signature', 1, 'BM자켓 드레스(폴리)', 'JA22-OPS014-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 18:53:07', 'jalley'),
	(709, 'update', 138, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-GR-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 19:08:53', 'jalley'),
	(710, 'update', 17, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-4M', 440000, 'System backup', 'Y', 'Y', '2022-06-07 19:36:11', 'jalley'),
	(711, 'update', 16, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-BK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-07 19:36:26', 'jalley'),
	(712, 'update', 14, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-4M', 440000, 'System backup', 'Y', 'Y', '2022-06-07 19:38:27', 'jalley'),
	(713, 'update', 13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-07 19:38:27', 'jalley'),
	(714, 'update', 13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-07 19:42:21', 'jalley'),
	(715, 'update', 155, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK0015-A-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 20:11:51', 'jalley'),
	(716, 'update', 154, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-2', 760000, 'System backup', 'Y', 'Y', '2022-06-07 20:17:06', 'jalley'),
	(717, 'update', 23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 20:23:35', 'jalley'),
	(718, 'update', 22, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-4M', 280000, 'System backup', 'Y', 'Y', '2022-06-07 20:23:42', 'jalley'),
	(719, 'update', 23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 20:24:23', 'jalley'),
	(720, 'update', 5, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-1S', 880000, 'System backup', 'Y', 'Y', '2022-06-07 20:27:42', 'jalley'),
	(721, 'update', 22, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-4M', 280000, 'System backup', 'Y', 'Y', '2022-06-07 20:41:46', 'jalley'),
	(722, 'update', 4, 'jalley', 'signature', 1, '스타자켓', 'JA21-jk011-PK-3M', 880000, 'System backup', 'Y', 'Y', '2022-06-07 20:43:49', 'jalley'),
	(723, 'update', 11, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-WH-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-07 20:45:55', 'jalley'),
	(724, 'update', 13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-07 20:53:34', 'jalley'),
	(725, 'update', 28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 20:54:07', 'jalley'),
	(726, 'update', 25, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-3S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 20:54:07', 'jalley'),
	(727, 'update', 28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 20:55:33', 'jalley'),
	(728, 'update', 148, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 20:59:05', 'jalley'),
	(729, 'update', 10, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-3M', 880000, 'System backup', 'Y', 'Y', '2022-06-07 21:03:21', 'jalley'),
	(730, 'update', 147, 'jalley', 'signature', 1, '와인 팬츠', 'JA22-PT007-BK-1', 440000, 'System backup', 'Y', 'Y', '2022-06-07 21:13:39', 'jalley'),
	(731, 'update', 134, 'jalley', 'signature', 1, '던던 자켓 드레스', 'JA22-OPS015-WH-3', 760000, 'System backup', 'Y', 'Y', '2022-06-07 21:15:18', 'jalley'),
	(732, 'update', 25, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-WH-3S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 21:18:45', 'jalley'),
	(733, 'update', 9, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-07 21:23:42', 'jalley'),
	(734, 'update', 8, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-4S', 880000, 'System backup', 'Y', 'Y', '2022-06-07 21:23:42', 'jalley'),
	(735, 'update', 9, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-07 21:29:42', 'jalley'),
	(736, 'update', 6, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-4M', 880000, 'System backup', 'Y', 'Y', '2022-06-07 21:32:31', 'jalley'),
	(737, 'update', 27, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-1S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 21:33:37', 'jalley'),
	(738, 'update', 28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 21:33:37', 'jalley'),
	(739, 'update', 143, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA222-OPS014-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-06-07 21:34:27', 'jalley'),
	(740, 'update', 156, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 21:46:51', 'jalley'),
	(741, 'update', 160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 'System backup', 'Y', 'Y', '2022-06-07 21:48:42', 'jalley'),
	(742, 'update', 152, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-3', 760000, 'System backup', 'Y', 'Y', '2022-06-07 22:04:33', 'jalley'),
	(743, 'update', 145, 'jalley', 'signature', 1, 'BM 자켓 드레스(트위드)', 'JA22-OPS014-A-BK-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 22:04:33', 'jalley'),
	(744, 'update', 159, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-3', 760000, 'System backup', 'Y', 'Y', '2022-06-07 22:05:58', 'jalley'),
	(745, 'update', 130, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-3', 1360000, 'System backup', 'Y', 'Y', '2022-06-07 23:16:36', 'jalley'),
	(746, 'update', 13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-07 23:23:16', 'jalley'),
	(747, 'update', 148, 'jalley', 'signature', 1, 'BM자켓 (폴리)', 'JA22-JK015-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 23:24:09', 'jalley'),
	(748, 'update', 130, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-BK-3', 1360000, 'System backup', 'Y', 'Y', '2022-06-07 23:24:49', 'jalley'),
	(749, 'update', 160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 'System backup', 'Y', 'Y', '2022-06-07 23:24:49', 'jalley'),
	(750, 'update', 156, 'jalley', 'signature', 1, 'BM자켓 (트위드)', 'JA22-JK015-A-PK-1', 760000, 'System backup', 'Y', 'Y', '2022-06-07 23:24:49', 'jalley'),
	(751, 'update', 143, 'jalley', 'signature', 1, 'BM 자켓 드레스(폴리)', 'JA222-OPS014-WH-2', 760000, 'System backup', 'Y', 'Y', '2022-06-07 23:24:49', 'jalley'),
	(752, 'update', 28, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-2S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 23:24:49', 'jalley'),
	(753, 'update', 27, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-1S', 280000, 'System backup', 'Y', 'Y', '2022-06-07 23:24:49', 'jalley'),
	(754, 'update', 7, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-07 23:25:53', 'jalley'),
	(755, 'update', 9, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-07 23:25:53', 'jalley'),
	(756, 'update', 7, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-WH-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-07 23:27:13', 'jalley'),
	(757, 'update', 13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-07 23:28:35', 'jalley'),
	(758, 'update', 23, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-BK-3S', 280000, 'System backup', 'Y', 'Y', '2022-06-08 08:16:02', 'jalley'),
	(759, 'update', 22, 'jalley', 'signature', 1, '스타 숏 스커트', 'JA21-SK005-PK-4M', 280000, 'System backup', 'Y', 'Y', '2022-06-08 08:16:02', 'jalley'),
	(760, 'update', 139, 'jalley', 'signature', 1, 'BM자켓 드레스(폴리)', 'JA22-OPS014-WH-1', 760000, 'System backup', 'Y', 'Y', '2022-06-08 08:19:21', 'jalley'),
	(761, 'update', 160, 'jalley', 'signature', 1, '허니 자켓', 'JA22-JK016-WH-1', 680000, 'System backup', 'Y', 'Y', '2022-06-08 08:20:15', 'jalley'),
	(762, 'update', 132, 'jalley', 'signature', 1, '모티브 드레스', 'JA21-OPS003-IV-2', 1360000, 'System backup', 'Y', 'Y', '2022-06-08 08:22:21', 'jalley'),
	(763, 'update', 13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-08 08:23:13', 'jalley'),
	(764, 'update', 9, 'jalley', 'signature', 1, '스타자켓', 'JA21-JK011-BK-2S', 880000, 'System backup', 'Y', 'Y', '2022-06-08 08:23:13', 'jalley'),
	(765, 'update', 13, 'jalley', 'signature', 1, '스타 롱 스커트', 'JA21-SK006-PK-1S', 440000, 'System backup', 'Y', 'Y', '2022-06-08 08:23:46', 'jalley'),
	(766, 'update', 203, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-06-08 11:07:26', 'jalley'),
	(767, 'update', 202, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-BK-2', 1440000, 'System backup 1', 'Y', 'Y', '2022-06-08 11:07:26', 'jalley'),
	(768, 'update', 200, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-2', 1440000, 'System backup', 'Y', 'Y', '2022-06-09 08:42:34', 'jalley'),
	(769, 'update', 187, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-3', 440000, 'System backup', 'Y', 'Y', '2022-06-09 08:42:34', 'jalley'),
	(770, 'update', 203, 'emp10', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-3', 1440000, 'System backup', 'Y', 'Y', '2022-06-09 10:14:51', 'emp10'),
	(771, 'update', 181, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-1', 440000, 'System backup', 'Y', 'Y', '2022-06-09 11:08:10', 'jalley'),
	(772, 'update', 171, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-WH-2', 440000, 'System backup', 'Y', 'Y', '2022-06-09 11:08:10', 'jalley'),
	(773, 'update', 181, 'jalley', 'signature', 1, '구찌 팬츠', 'JA22-PT006-GR-1', 440000, 'System backup', 'Y', 'Y', '2022-06-09 16:34:24', 'jalley'),
	(774, 'update', 197, 'jalley', 'signature', 1, '라임 드레스', 'JA22-OPS017-GR-1', 2000000, 'System backup', 'Y', 'Y', '2022-06-09 16:34:51', 'jalley'),
	(775, 'update', 187, 'jalley', 'signature', 1, 'BB스커트', 'ja22-sk009-wh-3', 440000, 'System backup', 'Y', 'Y', '2022-06-11 14:33:55', 'jalley'),
	(776, 'update', 200, 'jalley', 'signature', 1, '브리티시 드레스', 'JA22-OPS019-WH-2', 1440000, 'System backup', 'Y', 'Y', '2022-06-11 14:33:55', 'jalley');
/*!40000 ALTER TABLE `product_backup` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.product_file
DROP TABLE IF EXISTS `product_file`;
CREATE TABLE IF NOT EXISTS `product_file` (
  `product_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '상품아이디',
  `file_id` bigint(20) NOT NULL COMMENT '파일아이디',
  PRIMARY KEY (`product_id`),
  KEY `FK_product_file_file_id_file_file_id` (`file_id`),
  CONSTRAINT `FK_product_file_file_id_file_file_id` FOREIGN KEY (`file_id`) REFERENCES `file` (`file_id`),
  CONSTRAINT `FK_product_file_product_id_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8mb3 COMMENT='상품파일';

-- Dumping data for table jalley_auction.product_file: ~203 rows (approximately)
DELETE FROM `product_file`;
/*!40000 ALTER TABLE `product_file` DISABLE KEYS */;
INSERT INTO `product_file` (`product_id`, `file_id`) VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4),
	(5, 5),
	(6, 6),
	(7, 7),
	(8, 8),
	(9, 9),
	(10, 10),
	(11, 11),
	(12, 12),
	(13, 13),
	(14, 14),
	(15, 15),
	(16, 16),
	(17, 17),
	(18, 18),
	(19, 19),
	(20, 20),
	(21, 21),
	(22, 22),
	(23, 23),
	(24, 24),
	(25, 25),
	(26, 26),
	(27, 27),
	(28, 28),
	(29, 29),
	(30, 30),
	(31, 31),
	(32, 32),
	(33, 33),
	(34, 34),
	(35, 35),
	(36, 36),
	(37, 37),
	(38, 38),
	(39, 39),
	(40, 40),
	(41, 41),
	(42, 42),
	(43, 43),
	(44, 44),
	(45, 45),
	(46, 46),
	(47, 47),
	(48, 48),
	(49, 49),
	(50, 50),
	(51, 51),
	(52, 52),
	(53, 53),
	(54, 54),
	(55, 55),
	(56, 56),
	(57, 57),
	(58, 58),
	(59, 59),
	(60, 60),
	(61, 61),
	(62, 62),
	(63, 63),
	(64, 64),
	(65, 65),
	(66, 66),
	(67, 67),
	(68, 68),
	(69, 69),
	(70, 70),
	(71, 71),
	(72, 72),
	(73, 73),
	(74, 74),
	(75, 75),
	(76, 76),
	(77, 77),
	(78, 78),
	(79, 79),
	(80, 80),
	(81, 81),
	(82, 82),
	(83, 83),
	(84, 84),
	(85, 85),
	(86, 86),
	(87, 87),
	(88, 88),
	(89, 89),
	(90, 90),
	(91, 91),
	(92, 92),
	(93, 93),
	(94, 94),
	(95, 95),
	(96, 96),
	(97, 97),
	(98, 98),
	(99, 99),
	(100, 100),
	(101, 101),
	(102, 102),
	(103, 103),
	(104, 104),
	(105, 105),
	(106, 106),
	(107, 107),
	(108, 108),
	(109, 109),
	(110, 110),
	(111, 111),
	(112, 112),
	(113, 113),
	(114, 114),
	(115, 115),
	(116, 116),
	(117, 117),
	(118, 118),
	(119, 119),
	(120, 120),
	(121, 121),
	(122, 122),
	(123, 123),
	(124, 124),
	(125, 125),
	(126, 126),
	(127, 127),
	(128, 128),
	(129, 129),
	(130, 130),
	(131, 131),
	(132, 132),
	(133, 133),
	(134, 134),
	(135, 135),
	(136, 136),
	(137, 137),
	(138, 138),
	(139, 139),
	(140, 140),
	(141, 141),
	(142, 142),
	(143, 143),
	(144, 144),
	(145, 145),
	(146, 146),
	(147, 147),
	(148, 148),
	(149, 149),
	(150, 150),
	(151, 151),
	(152, 152),
	(153, 153),
	(154, 154),
	(155, 155),
	(156, 156),
	(157, 157),
	(158, 158),
	(159, 159),
	(160, 160),
	(161, 161),
	(162, 162),
	(163, 163),
	(164, 164),
	(165, 165),
	(166, 166),
	(167, 167),
	(168, 168),
	(169, 169),
	(170, 170),
	(171, 171),
	(172, 172),
	(173, 173),
	(174, 174),
	(175, 175),
	(176, 176),
	(177, 177),
	(178, 178),
	(179, 179),
	(180, 180),
	(181, 181),
	(182, 182),
	(183, 183),
	(184, 184),
	(185, 185),
	(186, 186),
	(187, 187),
	(188, 188),
	(189, 189),
	(190, 190),
	(191, 191),
	(192, 192),
	(193, 193),
	(194, 194),
	(195, 195),
	(196, 196),
	(197, 197),
	(198, 198),
	(199, 199),
	(200, 200),
	(201, 201),
	(202, 202),
	(203, 203);
/*!40000 ALTER TABLE `product_file` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.product_request
DROP TABLE IF EXISTS `product_request`;
CREATE TABLE IF NOT EXISTS `product_request` (
  `request_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '상품등록요청번호',
  `product_name` varchar(200) NOT NULL COMMENT '상품명',
  `serial_num` varchar(100) NOT NULL COMMENT '시리얼넘버',
  `price` bigint(20) NOT NULL COMMENT '가격',
  `memo` varchar(1000) DEFAULT NULL COMMENT '메모',
  `reject_memo` varchar(1000) DEFAULT NULL,
  `create_id` varchar(32) NOT NULL COMMENT '생성자',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '생성일',
  `request_status` varchar(32) NOT NULL COMMENT '요청상태',
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='상품등록요청';

-- Dumping data for table jalley_auction.product_request: ~2 rows (approximately)
DELETE FROM `product_request`;
/*!40000 ALTER TABLE `product_request` DISABLE KEYS */;
INSERT INTO `product_request` (`request_id`, `product_name`, `serial_num`, `price`, `memo`, `reject_memo`, `create_id`, `create_date`, `request_status`) VALUES
	(1, 'Product Test 2', '1231212323', 10, 'aaa', 'Test', 'emp07', '2022-05-17 09:20:42', 'reject'),
	(2, 'a1', 'a1', 1, 'a1', NULL, 'emp07', '2022-05-17 09:43:44', 'request');
/*!40000 ALTER TABLE `product_request` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.rent
DROP TABLE IF EXISTS `rent`;
CREATE TABLE IF NOT EXISTS `rent` (
  `rent_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '대여아이디',
  `user_id` varchar(32) NOT NULL,
  `rent_start_date` varchar(45) DEFAULT NULL COMMENT '대여시작일',
  `rent_end_date` varchar(45) DEFAULT NULL COMMENT '대여종료일',
  `total_rent_price` bigint(20) NOT NULL,
  `total_discount_price` bigint(20) NOT NULL,
  `total_sum_price` bigint(20) NOT NULL,
  `rent_status` varchar(45) NOT NULL,
  `pay_status` varchar(45) NOT NULL COMMENT '결제상태',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '생성일',
  `create_id` varchar(32) NOT NULL COMMENT '생성자',
  PRIMARY KEY (`rent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='대여';

-- Dumping data for table jalley_auction.rent: ~2 rows (approximately)
DELETE FROM `rent`;
/*!40000 ALTER TABLE `rent` DISABLE KEYS */;
INSERT INTO `rent` (`rent_id`, `user_id`, `rent_start_date`, `rent_end_date`, `total_rent_price`, `total_discount_price`, `total_sum_price`, `rent_status`, `pay_status`, `use_yn`, `create_date`, `create_id`) VALUES
	(1, 'emp04', '2022-06-09', '2022-06-10', 940000, 0, 940000, '4', '4', 'Y', '2022-06-09 08:42:34', 'admin'),
	(2, 'test', '2022-06-09', '2022-06-10', 1220000, 20000, 1200000, '1', '1', 'Y', '2022-06-09 11:08:10', 'admin');
/*!40000 ALTER TABLE `rent` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.rent_backup
DROP TABLE IF EXISTS `rent_backup`;
CREATE TABLE IF NOT EXISTS `rent_backup` (
  `rent_backup_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '대여백업아이디',
  `work_type` varchar(45) NOT NULL COMMENT '작업구분',
  `rent_id` bigint(20) NOT NULL COMMENT '대여아이디',
  `user_id` varchar(32) NOT NULL,
  `rent_start_date` varchar(45) NOT NULL COMMENT '대여시작일',
  `rent_end_date` varchar(45) NOT NULL COMMENT '대여종료일',
  `total_rent_price` bigint(20) NOT NULL,
  `total_discount_price` bigint(20) NOT NULL,
  `total_sum_price` bigint(20) NOT NULL,
  `rent_status` varchar(45) NOT NULL,
  `pay_status` varchar(45) NOT NULL COMMENT '결제상태',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  `change_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '수정일',
  `change_id` varchar(32) NOT NULL COMMENT '수정자',
  PRIMARY KEY (`rent_backup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COMMENT='대여';

-- Dumping data for table jalley_auction.rent_backup: ~6 rows (approximately)
DELETE FROM `rent_backup`;
/*!40000 ALTER TABLE `rent_backup` DISABLE KEYS */;
INSERT INTO `rent_backup` (`rent_backup_id`, `work_type`, `rent_id`, `user_id`, `rent_start_date`, `rent_end_date`, `total_rent_price`, `total_discount_price`, `total_sum_price`, `rent_status`, `pay_status`, `use_yn`, `change_date`, `change_id`) VALUES
	(1, 'insert', 1, 'emp04', '2022-06-09', '2022-06-10', 940000, 0, 940000, '1', '1', 'Y', '2022-06-09 08:42:34', 'admin'),
	(2, 'insert', 2, 'tuyen 123(test)', '2022-06-09', '2022-06-10', 440000, 40000, 400000, '1', '1', 'Y', '2022-06-09 11:08:10', 'admin'),
	(3, 'update', 2, 'test', '2022-06-09', '2022-06-10', 220000, 20000, 200000, '1', '1', 'Y', '2022-06-09 16:34:24', 'admin'),
	(4, 'update', 2, 'test', '2022-06-09', '2022-06-10', 1220000, 20000, 1200000, '1', '1', 'Y', '2022-06-09 16:34:51', 'admin'),
	(5, 'update', 1, 'emp04', '2022-06-09', '2022-06-10', 940000, 0, 940000, '1', '4', 'Y', '2022-06-10 11:19:50', 'admin'),
	(6, 'update', 1, 'emp04', '2022-06-09', '2022-06-10', 940000, 0, 940000, '4', '4', 'Y', '2022-06-11 14:33:55', 'admin');
/*!40000 ALTER TABLE `rent_backup` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.rent_items
DROP TABLE IF EXISTS `rent_items`;
CREATE TABLE IF NOT EXISTS `rent_items` (
  `rent_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '대여아이템아이디',
  `rent_id` bigint(20) NOT NULL COMMENT '대여아이디',
  `product_id` bigint(20) NOT NULL COMMENT '상품아이디',
  `memo` varchar(500) DEFAULT NULL,
  `rent_price` bigint(20) NOT NULL,
  `discount_price` bigint(20) NOT NULL,
  `sum_rent_price` bigint(20) NOT NULL,
  `pay_status` varchar(45) NOT NULL,
  `use_yn` varchar(1) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '생성일',
  `create_id` varchar(32) NOT NULL COMMENT '생성자',
  PRIMARY KEY (`rent_item_id`),
  KEY `FK_rent_items_product_id_product_product_id` (`product_id`),
  KEY `FK_rent_items_rent_id_rent_rent_id` (`rent_id`),
  CONSTRAINT `FK_rent_items_product_id_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `FK_rent_items_rent_id_rent_rent_id` FOREIGN KEY (`rent_id`) REFERENCES `rent` (`rent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COMMENT='대여상품';

-- Dumping data for table jalley_auction.rent_items: ~5 rows (approximately)
DELETE FROM `rent_items`;
/*!40000 ALTER TABLE `rent_items` DISABLE KEYS */;
INSERT INTO `rent_items` (`rent_item_id`, `rent_id`, `product_id`, `memo`, `rent_price`, `discount_price`, `sum_rent_price`, `pay_status`, `use_yn`, `create_date`, `create_id`) VALUES
	(1, 1, 200, NULL, 720000, 0, 720000, '1', 'Y', '2022-06-09 08:42:34', 'admin'),
	(2, 1, 187, NULL, 220000, 0, 220000, '1', 'Y', '2022-06-09 08:42:34', 'admin'),
	(3, 2, 181, NULL, 220000, 20000, 200000, '1', 'N', '2022-06-09 11:08:10', 'admin'),
	(4, 2, 171, NULL, 220000, 20000, 200000, '1', 'Y', '2022-06-09 11:08:10', 'admin'),
	(5, 2, 197, NULL, 1000000, 0, 1000000, '1', 'Y', '2022-06-09 16:34:51', 'admin');
/*!40000 ALTER TABLE `rent_items` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.rent_items_backup
DROP TABLE IF EXISTS `rent_items_backup`;
CREATE TABLE IF NOT EXISTS `rent_items_backup` (
  `rent_item_backup_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '대여상품백업아이디',
  `work_type` varchar(45) NOT NULL,
  `rent_item_id` bigint(20) NOT NULL,
  `rent_id` bigint(20) NOT NULL COMMENT '대여아이디',
  `product_id` bigint(20) NOT NULL COMMENT '상품아이디',
  `memo` varchar(500) DEFAULT NULL,
  `rent_price` bigint(20) NOT NULL,
  `discount_price` bigint(20) DEFAULT NULL,
  `sum_rent_price` bigint(20) NOT NULL,
  `pay_status` varchar(45) NOT NULL,
  `use_yn` varchar(1) NOT NULL,
  `change_date` datetime NOT NULL DEFAULT current_timestamp() COMMENT '수정일',
  `change_id` varchar(32) NOT NULL COMMENT '수정자',
  PRIMARY KEY (`rent_item_backup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COMMENT='대여상품백업';

-- Dumping data for table jalley_auction.rent_items_backup: ~5 rows (approximately)
DELETE FROM `rent_items_backup`;
/*!40000 ALTER TABLE `rent_items_backup` DISABLE KEYS */;
INSERT INTO `rent_items_backup` (`rent_item_backup_id`, `work_type`, `rent_item_id`, `rent_id`, `product_id`, `memo`, `rent_price`, `discount_price`, `sum_rent_price`, `pay_status`, `use_yn`, `change_date`, `change_id`) VALUES
	(1, 'insert', 1, 1, 200, NULL, 720000, 0, 720000, '1', 'Y', '2022-06-09 08:42:34', 'admin'),
	(2, 'insert', 2, 1, 187, NULL, 220000, 0, 220000, '1', 'Y', '2022-06-09 08:42:34', 'admin'),
	(3, 'insert', 3, 2, 181, NULL, 220000, 20000, 200000, '1', 'Y', '2022-06-09 11:08:10', 'admin'),
	(4, 'insert', 4, 2, 171, NULL, 220000, 20000, 200000, '1', 'Y', '2022-06-09 11:08:10', 'admin'),
	(5, 'insert', 5, 2, 197, NULL, 1000000, 0, 1000000, '1', 'Y', '2022-06-09 16:34:51', 'admin');
/*!40000 ALTER TABLE `rent_items_backup` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '권한아이디',
  `role_code` varchar(45) DEFAULT NULL,
  `lang_cd` varchar(45) DEFAULT NULL,
  `role_name` varchar(50) NOT NULL COMMENT '권한명',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3 COMMENT='권한테이블';

-- Dumping data for table jalley_auction.role: ~12 rows (approximately)
DELETE FROM `role`;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`role_id`, `role_code`, `lang_cd`, `role_name`, `use_yn`) VALUES
	(1, '1', 'en', 'Admin', 'Y'),
	(2, '2', 'en', 'Main Company', 'Y'),
	(3, '3', 'en', 'Other Company', 'Y'),
	(4, '4', 'en', 'User', 'Y'),
	(5, '1', 'ko', '관리자', 'Y'),
	(6, '2', 'ko', '본사', 'Y'),
	(7, '3', 'ko', '대리점', 'Y'),
	(8, '4', 'ko', '사용자', 'Y'),
	(12, '1', 'vt', 'Quản trị', 'Y'),
	(13, '2', 'vt', 'Tổng công ty', 'Y'),
	(14, '3', 'vt', 'Công ty khác', 'Y'),
	(15, '4', 'vt', 'Người dùng', 'Y');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.subdomain
DROP TABLE IF EXISTS `subdomain`;
CREATE TABLE IF NOT EXISTS `subdomain` (
  `subdomain_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '서브도메인아이디',
  `user_id` varchar(32) NOT NULL COMMENT '사용자아이디',
  `sub_domain` varchar(50) NOT NULL COMMENT '서브도메인',
  `site_open_yn` varchar(1) NOT NULL COMMENT '사이트오픈유무',
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  `create_date` datetime NOT NULL COMMENT '생성일',
  `create_id` varchar(32) NOT NULL COMMENT '생성자',
  PRIMARY KEY (`subdomain_id`),
  KEY `FK_subdomain_user_id_user_user_id` (`user_id`),
  CONSTRAINT `FK_subdomain_user_id_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='서브도메인';

-- Dumping data for table jalley_auction.subdomain: ~0 rows (approximately)
DELETE FROM `subdomain`;
/*!40000 ALTER TABLE `subdomain` DISABLE KEYS */;
/*!40000 ALTER TABLE `subdomain` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `u_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '유저아이디',
  `user_id` varchar(32) NOT NULL COMMENT '사용자아이디',
  `user_pw` varchar(255) NOT NULL COMMENT '비밀번호',
  `cash` bigint(20) NOT NULL COMMENT '충전금액',
  `name` varchar(50) NOT NULL COMMENT '이름',
  `nickname` varchar(50) NOT NULL COMMENT '별명',
  `country_tel_code` varchar(5) NOT NULL COMMENT '국가코드',
  `tel` varchar(20) NOT NULL COMMENT '전화번호',
  `addr` varchar(200) DEFAULT NULL COMMENT '주소',
  `addr_detail` varchar(200) DEFAULT NULL COMMENT '상세주소',
  `mail` varchar(200) DEFAULT NULL COMMENT '메일',
  `birthday` varchar(11) DEFAULT NULL COMMENT '생일',
  `comp_name` varchar(50) DEFAULT NULL COMMENT '회사명',
  `comp_tel` varchar(20) DEFAULT NULL COMMENT '회사전화',
  `comp_mgt_name` varchar(50) DEFAULT NULL COMMENT '업체담당자명',
  `company_num` varchar(20) NOT NULL COMMENT '사업자번호',
  `comp_mgt_tel` varchar(20) DEFAULT NULL COMMENT '업체담당자전화',
  `push_id` varchar(2000) DEFAULT NULL COMMENT '알림아이디(앱사용시)',
  `blacklist_level` varchar(45) NOT NULL COMMENT '블랙리스트단계',
  `status` varchar(3) NOT NULL COMMENT '사용자상태',
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `use_yn` varchar(1) NOT NULL COMMENT '사용유무',
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `UQ_user_1` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb3 COMMENT='사용자';

-- Dumping data for table jalley_auction.user: ~14 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`u_id`, `user_id`, `user_pw`, `cash`, `name`, `nickname`, `country_tel_code`, `tel`, `addr`, `addr_detail`, `mail`, `birthday`, `comp_name`, `comp_tel`, `comp_mgt_name`, `company_num`, `comp_mgt_tel`, `push_id`, `blacklist_level`, `status`, `create_date`, `use_yn`) VALUES
	(1, 'admin', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 0, 'admin', 'admin', '084', '098584869', 'Bắc Ninh', 'Bắc Ninh', 'admin@vietkosoft.com', '1990-05-15', 'vietkosoft', '8254856666', 'VietKo-Group', '9999999999', '0958422586', '12', '1', '1', '2022-05-05 09:21:34', 'Y'),
	(2, 'emp01', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 0, 'emp01', 'emp01', '084', '096584428', NULL, NULL, 'emp01@vietkosoft.com', NULL, 'EICompany', '825433346', 'VietKo-Group', '678888888', '0958422586', '999999999', '2', '1', '2022-05-05 09:21:34', 'Y'),
	(4, 'emp02', '756bc47cb5215dc3329ca7e1f7be33a2dad68990bb94b76d90aa07f4e44a233a', 0, 'emp02-1', 'emp02', '84858', '845825555', 'bn', 'bn', 'emp02@gmail.com', '1990-05-23', 'VK Soft', '9908778899', 'VietKo', '12399989999', '2345555', '999999999', '2', '1', '2022-05-08 20:12:34', 'Y'),
	(5, 'emp04', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 0, 'emp04', 'emp04', '12133', '1314443333', 'Bắc Ninh', 'Bắc Ninh', 'emp04@gmail&gt;com', '', 'VK Soft', '990877887', 'VietKo', '123455555', '2345555', '999999999', '2', '1', '2022-05-08 20:55:55', 'Y'),
	(6, 'emp03', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 10, 'emp03', 'emp03', '12133', '31312313123', 'Bắc Ninh', 'Bắc Ninh', 'emp03@gmail.com', '2000-06-11', 'VK Soft', '9908778666', 'VietKo', '12399989999', '2345555', '999999999', '3', '1', '2022-05-08 21:02:43', 'Y'),
	(7, 'emp06', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 20, 'emp06', 'emp06', '0844', '548585825', 'Bắc Ninh 1', 'Bắc Ninh', 'emp06@gmail.com', '1990-05-09', 'VK Korean', '9908778645', 'VK Bio', '12399989999', '2345555', '999999999', '1', '1', '2022-05-08 22:39:00', 'Y'),
	(8, 'emp07', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 0, 'emp07', 'emp07', '084', '0895485485', 'Bắc Ninh', 'Bắc Ninh', 'emp07@gmail.com', '1990-05-09', 'VK Soft', '9908778668', 'VietKo', '12399989999', '2345555', '999999999', '1', '2', '2022-05-08 22:54:04', 'Y'),
	(10, 'emp09', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 5, 'em09', 'em09-2', '084', '38388877', 'Hà Nội', 'Mỹ Đình', 'emp09@gmail.com', '1992-07-20', 'VK Soft', '9908778666', 'VietKo', '12399989999', '2345555', '999999999', '2', '1', '2022-05-09 14:01:29', 'Y'),
	(11, 'emp10', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 0, 'emp10', 'emp10', '084', '0254856845', 'Hà Nội', NULL, 'emp10@gmail.com', NULL, NULL, NULL, NULL, '12399989999', NULL, NULL, '1', '1', '2022-05-09 21:58:11', 'Y'),
	(12, 'emp11', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 0, 'emp11', 'emp11', '084', '0895485455', 'Bắc Ninh', 'Bắc Ninh', 'emp11@gmail.com', '1990-08-10', 'VK Soft', '9908778666', 'VietKo', '12399989999', '2345555', '999999999', '1', '1', '2022-05-10 11:08:29', 'Y'),
	(13, 'emp12', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 0, 'emp12', 'emp12', '084', '0685485485', 'Bắc Ninh', 'Bắc Ninh', 'emp12@gmail.com', '1989-07-05', 'VK Soft', '9908778666', 'VietKo', '12399989999', '2345555', '999999999', '1', '1', '2022-05-10 11:17:11', 'Y'),
	(14, 'jalley', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 0, 'jalley', 'jalley', '084', '0999999999', 'Bắc Ninh', 'Bắc Ninh', 'jalley@auction.com', '1900-01-01', 'VK Soft', '9908778666', 'VietKo', '12399989999', '2345555', '999999999', '1', '1', '2022-05-14 11:32:38', 'Y'),
	(15, 'test', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', 0, 'tuyen 123', 'test ad', '010', '355747814', 'Que Vo, Bac Ninh', 'Bắc Ninh', 'tuyen.cntt.k13a@gmail.com', '2021-09-02', NULL, NULL, NULL, '01100110011', NULL, '999999999', '1', '1', '2022-05-26 10:49:16', 'Y'),
	(21, 'dev', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 200000, 'dev', 'dev', '084', '0985485859', 'Bac Ninh', 'Ly nhan tong', 'dev@gmail.com', '1990-01-20', 'VK Soft', '89584854825', 'VietKo Group', '9999999', '888888888', '999999999', '1', '1', '2022-06-10 10:55:12', 'Y');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table jalley_auction.user_role
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
  `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '사용자권한아이디',
  `user_id` varchar(32) NOT NULL COMMENT '사용자아이디',
  `role_code` varchar(45) NOT NULL COMMENT '권한아이디',
  PRIMARY KEY (`user_role_id`),
  KEY `FK_user_role_user_id_user_user_id` (`user_id`),
  FULLTEXT KEY `FK_user_role_role_code_role_role_code` (`role_code`),
  CONSTRAINT `FK_user_role_user_id_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table jalley_auction.user_role: ~14 rows (approximately)
DELETE FROM `user_role`;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`user_role_id`, `user_id`, `role_code`) VALUES
	(2, 'admin', '1'),
	(3, 'emp01', '3'),
	(4, 'emp02', '4'),
	(5, 'emp04', '2'),
	(6, 'emp03', '3'),
	(7, 'emp06', '4'),
	(8, 'emp07', '1'),
	(10, 'emp09', '3'),
	(11, 'emp10', '4'),
	(12, 'emp11', '2'),
	(13, 'emp12', '1'),
	(14, 'jalley', '1'),
	(15, 'test', '4'),
	(16, 'dev', '1');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

-- Dumping structure for function jalley_auction.GET_CODE_NAME
DROP FUNCTION IF EXISTS `GET_CODE_NAME`;
DELIMITER //
CREATE FUNCTION `GET_CODE_NAME`(`var_c_code` VARCHAR(45),
    `var_lang_cd` VARCHAR(3),
    `var_c_code_value` VARCHAR(45)
    ) RETURNS varchar(200) CHARSET utf8mb3
BEGIN

RETURN
    (   
		SELECT 
				c_code_name AS C_CODE_NAME FROM jalley_auction.c_code
			WHERE c_code = var_c_code
			AND lang_cd = var_lang_cd	
            AND c_code_value = var_c_code_value            
	);
END//
DELIMITER ;

-- Dumping structure for function jalley_auction.GET_COUNT_BID
DROP FUNCTION IF EXISTS `GET_COUNT_BID`;
DELIMITER //
CREATE FUNCTION `GET_COUNT_BID`(`auction_id` BIGINT(12)    
    ) RETURNS varchar(200) CHARSET utf8mb3
BEGIN

RETURN
    (   
		SELECT 
				COUNT(B.AUCTION_ID)  BID_NUM FROM BID B
			WHERE B.AUCTION_ID = auction_id			       
	);
END//
DELIMITER ;

-- Dumping structure for function jalley_auction.GET_MAX_BID_PRICE
DROP FUNCTION IF EXISTS `GET_MAX_BID_PRICE`;
DELIMITER //
CREATE FUNCTION `GET_MAX_BID_PRICE`(`auction_id` BIGINT(12)    
    ) RETURNS varchar(200) CHARSET utf8mb3
BEGIN

RETURN
    (   
		SELECT 
				MAX(b.bid_price)  max_bid_price FROM jalley_auction.bid b
			WHERE b.auction_id = auction_id			       
	);
END//
DELIMITER ;

-- Dumping structure for function jalley_auction.GET_PRODUCT_IMG_PATH
DROP FUNCTION IF EXISTS `GET_PRODUCT_IMG_PATH`;
DELIMITER //
CREATE FUNCTION `GET_PRODUCT_IMG_PATH`(`product_id` BIGINT(12)    
    ) RETURNS varchar(200) CHARSET utf8mb3
BEGIN

RETURN
    (   
		SELECT 
				p.img_path AS img_path FROM jalley_auction.product p
			WHERE p.product_id = product_id			       
	);
END//
DELIMITER ;

-- Dumping structure for function jalley_auction.GET_ROLE_NAME
DROP FUNCTION IF EXISTS `GET_ROLE_NAME`;
DELIMITER //
CREATE FUNCTION `GET_ROLE_NAME`(`var_lang_cd` VARCHAR(3),
    `var_user_id` VARCHAR(20)
    ) RETURNS varchar(200) CHARSET utf8mb3
BEGIN

RETURN
    (   
		 SELECT 
			role_name AS ROLE_NAME 
				FROM jalley_auction.user_role ur
				INNER JOIN jalley_auction.role r
					ON 	ur.user_id = var_user_id
					AND  r.lang_cd = var_lang_cd
					AND ur.role_code = r.role_code         
	);
END//
DELIMITER ;

-- Dumping structure for function jalley_auction.GET_STORE_NAME
DROP FUNCTION IF EXISTS `GET_STORE_NAME`;
DELIMITER //
CREATE FUNCTION `GET_STORE_NAME`(`product_id` BIGINT(12)    
    ) RETURNS varchar(200) CHARSET utf8mb3
BEGIN

RETURN
    (   
		SELECT 
				p.store_id AS store_name FROM jalley_auction.product p
			WHERE p.product_id = product_id			       
	);
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
